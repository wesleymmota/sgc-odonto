#!/usr/bin/env bash

find src/ -type f -name "*.php" -print0 | xargs -0 wc -l
