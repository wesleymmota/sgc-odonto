<?php

namespace AdminBundle\Controller;

use AdminBundle\Manager\ClinicManager;
use AdminBundle\Manager\ProcedureManager;
use AdminBundle\Manager\SpecializationManager;
use AdminBundle\Model\ClinicModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/procedure")
 */
class ProcedureController extends BaseController
{
    /**
     * @Route("/", name="admin_procedure_list")
     * @Template("admin/procedure/index.html.twig")
     */
    public function indexAction(Request $request, ProcedureManager $manager)
    {
        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "id"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
                [ 'id' => 'description', 'name' => 'form.description' , 'sorted' => 'true', 'style' => 'width: 300px;'],
                [ 'id' => 'averageTime', 'name' => 'form.averageTime' , 'sorted' => 'false','style' => 'width: 130px;'],
                [ 'id' => 'updatedAt', 'name' => 'form.updatedAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'createdAt', 'name' => 'form.createdAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 100px;'],

            ]
        ];

        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

//
//        $request->getSession()->getFlashBag()->add('success', 'Cartão cadastrado com sucesso.');
//        $request->getSession()->getFlashBag()->add('error', 'Ocorreu um erro ao cadastrar o cartão.');
        $data = array_merge([ 'data' => $pagination ], $filter);


        return $data;
    }

    /**
     * @Route("/new", name="admin_procedure_new")
     * @Template("admin/procedure/new.html.twig")
     */
    public function new(Request $request, ProcedureManager $manager)
    {
        $task = new Procedure();


        $form = $this->createFormBuilder($task)
            ->setAction($this->generateUrl('admin_procedure_new'))
            ->setMethod('POST')
            ->add('description', TextType::class, array('label' => 'form.label.description'))
            ->add('average_time', TextType::class, array('label' => 'form.label.average_time'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $saved = $manager->save($task);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_procedure_list');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_procedure_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.procedure',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'procedure.new' => [
                        'name' =>  'header.procedure.new'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/edit", name="admin_procedure_edit")
     * @ParamConverter(
     *     "procedure",
     *     class="BaseBundle:Procedure",
     *     options={"id": "id"}
     * )
     * @Template("admin/procedure/edit.html.twig")
     */
    public function edit(Request $request, Procedure $procedure, ProcedureManager $manager)
    {
        $form = $this->createFormBuilder($procedure)
            ->setAction($this->generateUrl('admin_procedure_edit', ['id'=> $procedure->getId() ]))
            ->setMethod('POST')
            ->add('description', TextType::class, array('label' => 'form.label.description'))
            ->add('average_time', TextType::class, array('label' => 'form.label.average_time'))

            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $specializationForm = $form->getData();


            $saved = $manager->save($specializationForm);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_procedure_list');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_procedure_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.procedure',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'procedure.new' => [
                        'name' =>  'header.procedure.edit'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", name="admin_procedure_delete")
     * @ParamConverter(
     *     "procedure",
     *     class="BaseBundle:Procedure",
     *     options={"id": "id"}
     * )
     */
    public function delete(Request $request, Procedure $procedure, ProcedureManager $manager)
    {
        if ($procedure instanceof Procedure && $procedure->getId() > 0) {
            $saved = $manager->delete($procedure);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.removed.if.success');

                return $this->redirectToRoute('admin_procedure_list');
            }
        }

        $request->getSession()->getFlashBag()->add('danger', 'register.removed.if.notsuccess');
        return $this->redirectToRoute('admin_procedure_list');
    }
}
