<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ClinicFormType;
use AdminBundle\Manager\ClinicManager;
use AdminBundle\Manager\UserManager;
use AdminBundle\Model\ClinicModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use FOS\UserBundle\Event\FormEvent;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/clinic")
 */
class ClinicController extends BaseController
{
    private $eventDispatcher;
    private $userManager;

    /**
     * ClinicController constructor.
     * @param EventDispatcherInterface $eventDispatcher
     * @param UserManager $userManager
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, UserManager $userManager)
    {
        $this->eventDispatcher = $eventDispatcher;

        $this->userManager = $userManager;
    }

    /**
     * @Route("/", name="admin_clinic")
     * @Template("admin/clinic/index.html.twig")
     */
    public function indexAction(Request $request, ClinicManager $manager)
    {

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "razao"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
//                [ 'id' => 'id', 'name' => 'form.id' , 'sorted' => 'true', 'style' => 'width: 40px;'],
                [ 'id' => 'razao', 'name' => 'form.razao' , 'sorted' => 'true', 'style' => 'width: 240px;'],
                [ 'id' => 'fantasia', 'name' => 'form.fantasia', 'sorted' => 'true', 'style' => 'width: 240px;'],
                [ 'id' => 'cnpj', 'name' => 'form.cpnj', 'sorted' => 'false' , 'style' => 'width: 140px;'],
                [ 'id' => 'enabled', 'name' => 'form.status', 'sorted' => 'true' , 'bool'=> true, 'style' => 'width: 100px;']
            ]
        ];

        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

        $data = array_merge([ 'data' => $pagination ], $filter);


        $routeName = "admin_clinic";

        $breadcumbs = [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  null,
                'items' => [
                ]
            ],
        ];

        $data = array_merge($breadcumbs, $data);
        return  $data;
    }


    /**
     * @Route("/{id}/state",
     *     name="admin_clinic_change_state",
     *       options = { "expose" = true },
     * )
     * @ParamConverter(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     *
     */
    public function changeState(Request $request, Clinic $clinic, ClinicManager $manager)
    {
        if (null === $clinic) {
            return $this->redirectToRoute('admin_clinic');
        }
        $clinic->setEnabled(!$clinic->isEnabled());
        $saved = $manager->merge($clinic);

        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        } else {
            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }


        return new JsonResponse(array('state' => true));
    }

    /**
     * @Route("/new", name="admin_clinic_new")
     * @Template("admin/clinic/new.html.twig")
     */
    public function newAction(Request $request, ClinicManager $clinicManager)
    {
        $userLogged = $this->getUser();


        $profile = new ClinicModel(new Clinic());

        $form = $this->createForm(ClinicFormType::class, $profile, [
            'action' => $this->generateUrl('admin_clinic_new'),
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_clinic');
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $profile = $form->getData();

            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $saved = null;
            $clinic = null;
            if (null === $profile->getSettings()->getLogo()->getImage()) {
                $profile->getPersistClinic()->getSettings()->setLogo(null);
                $saved = $clinicManager->save($profile->getPersistClinic());
            }
            if (null !== $profile->getSettings()->getLogo() && $profile->getSettings()->getLogo()->getImage() instanceof UploadedFile) {

                $asset = $profile->getSettings()->getLogo();
                $profile->getPersistClinic()->getSettings()->setLogo(null);
                $clinic = $clinicManager->save($profile->getPersistClinic());



                $profile_upload_directory = "clinic/" . $clinic->getId(). "/logo";
                $asset->setTargetDir($profile_upload_directory);

                $assetFile = $asset->upload('BaseBundle::Clinic');
                $storage = $this->get('base.google_storage');

                $image100x100 = $profile_upload_directory . "/100x100-" . basename($assetFile);
                Image::open($assetFile)
                    ->cropResize(200, 200)
                    ->zoomCrop(250, 250)
                    ->save($image100x100);

                $options = [
                    'resumable' => true,
                    'name' => $image100x100,
                    'metadata' => [
                        'user_id' => $clinic->getId(),
                        'type' => 'logo'
                    ]
                ];

                $fileName = $storage->uploadFile($image100x100, $options);

                $asset->setUrl($fileName);
                $asset->setUpdatedAt(new \DateTime());

                $clinic->getSettings()->setLogo($asset);

                $saved = $clinicManager->merge($clinic);


            }

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_clinic_show', ['id' => $saved->getId() ]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_clinic";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'form.new',
                        'route' => '',
                    ]

                ]
            ],
            'form' =>  $form->createView(),
        ];
    }

    /**
     * @Route("/edit/{id}", name="admin_clinic_edit")
     * @Template("admin/clinic/edit.html.twig")
     * @ParamConverter(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function edit(Request $request, Clinic $clinic, ClinicManager $clinicManager)
    {
        $userLogged = $this->getUser();
        $profile = new ClinicModel($clinic);

        $profile_upload_directory = "clinic/" . $clinic->getId(). "/logo";

        $form = $this->createForm(ClinicFormType::class, $profile, [
            'action' => $this->generateUrl('admin_clinic_edit', ["id" => $clinic->getId()]),
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_clinic_show', ['id' => $clinic->getId()]);

        }

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);

            $profile = $form->getData();

            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            if (null !== $profile->getSettings()->getLogo() && $profile->getSettings()->getLogo()->getImage() instanceof UploadedFile) {

                $asset = $profile->getSettings()->getLogo();

                if($asset->getImage() instanceof UploadedFile) {


                    $asset->setTargetDir($profile_upload_directory);

                    $assetFile = $asset->upload('BaseBundle::Clinic');
                    $storage = $this->get('base.google_storage');

                    $image100x100 = $profile_upload_directory . "/100x100-" . basename($assetFile);
                    Image::open($assetFile)
                        ->cropResize(200, 200)
                        ->zoomCrop(250, 250)
                        ->save($image100x100);

                    $options = [
                        'resumable' => true,
                        'name' => $image100x100,
                        'metadata' => [
                            'user_id' => $clinic->getId(),
                            'type' => 'logo'
                        ]
                    ];

                    $fileName = $storage->uploadFile($image100x100, $options);

                    $asset->setUrl($fileName);
                    $asset->setUpdatedAt(new \DateTime());

                    $profile->getPersistClinic()->getSettings()->setLogo($asset);
                }else{
                    $profile->getPersistClinic()->getSettings()->setLogo(null);
                }
            }
            $saved = $clinicManager->merge($profile->getPersistClinic());

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_clinic_show', ['id' => $clinic->getId() ]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_clinic";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'form.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' =>  $form->createView(),
            'clinic' => $clinic
        ];
    }

    /**
     * @Route("/{id}/show", name="admin_clinic_show")
     * @Template("admin/clinic/show.html.twig")
     * @Entity(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function show(Request $request, Clinic $clinic)
    {
        $routeName = "admin_clinic";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'information' => [
                        'name' => 'form.information',
                        'route' => null,
                    ]

                ]
            ],
            'clinic' =>  $clinic,
        ];
    }

    /**
     * @Route("/{id}/users", name="admin_clinic_user_list")
     * @Template("admin/clinic/user/user-list.html.twig")
     * @Entity(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function user_list(Request $request, Clinic $clinic, ClinicManager $clinicManager)
    {

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "name"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
                [ 'id' => 'name', 'name' => 'form.name' , 'sorted' => 'true', 'style' => 'width: 250px;', 'card' => true],
                ['id'  => 'roles', 'name' => 'Papel na clinica' , 'sorted' => 'false', 'style' => 'width: 130px;',  'array' => true],
                ['id' => 'phone', 'name' => 'form.phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                ['id'  => 'cell_phone', 'name' => 'form.cell_phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'updatedAt', 'name' => 'form.updatedAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'createdAt', 'name' => 'form.createdAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'enabled', 'name' => 'form.status', 'sorted' => 'true' , 'bool'=> true, 'style' => 'width: 120px;'],
            ]
        ];

        $build = $clinicManager->getRepository()->getClients($clinic, $filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

        $data = array_merge([ 'data' => $pagination ], $filter);


        $routeName = "admin_clinic_show";

        $breadcumbs = [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName,['id' => $clinic->getId()]),
                'items' => [
                    'information' => [
                        'name' => 'breadcumb.clinical.admin_user_list',
                        'route' => null,
                    ]

                ]
            ],

            '_route' => 'admin_clinic_user_list',
            'clinic' =>  $clinic
        ];

        $data = array_merge($breadcumbs, $data);
        return  $data;
    }


    /**
     * @Route("/{id}/add_user", name="admin_clinic_user_add_to_clinic")
     * @Template("admin/clinic/user/user-add.html.twig")
     * @Entity(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function user_add(Request $request, Clinic $clinic, UserManager $userManager, ClinicManager $clinicManager)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_clinic_user_add_to_clinic', ['id' => $clinic->getId()]))
            ->setMethod('POST')
            ->add('profile', ChoiceType::class, [
                'label' => 'user.label.profile',
                'expanded' => true,
                'required' => true,
                'choices' => User::getAdminProfilesRoles(),
                'choice_attr' => function($choiceValue, $key, $value) {
                    return ['class' => 'm-radio m-radio--bold', 'style' => "margin-left: 30px;margin-right: 5px;"];
                },
                'choice_label' => function ($choice) {
                    return User::getStateRole($choice);
                },
            ])
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
            ->add('user', EntityType::class, [
                'label' => 'form.users',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:User',
                'choice_label' => function ($choice) {
                    return  $choice->getName() . " - ". $choice->getEmail();
                },
                'query_builder' => function (EntityRepository $er)  use ($clinic) {
//                    $qb = $er->
//                    ->leftJoin(
//                        '\BaseBundle\Entity\UserClinical',
//                        'user_clinical',
//                        \Doctrine\ORM\Query\Expr\Join::WITH,
//                        'u = user_clinical.clinic'
//                    )
//                    ;

//                        $qb->where($qb->expr()->andX(
//                            $qb->expr()->eq('c.id', ':clinic')
//                        ))
//
//                        ->setParameters(
//                            [
//                                'clinic' => $clinic instanceof Clinic ? $clinic->getId() :  $clinic,
//                            ]
//                        );
//
//

//                    $qb->where($qb->expr()->orX(
//                    //                        $qb->expr()->eq('u.enabled', 1),
//                    //                        $qb->expr()->isNull('u.clinic'),
//                        $qb->expr()->like('u.roles', ':roles1'),
//                        $qb->expr()->like('u.roles', ':roles2')
//                    ))
//                        ->setParameter('roles1', '%ROLE_EMPLOYEE%')
//                       ->setParameter('roles2', '%ROLE_USER%');
//
////                    $qb->andWhere('u.clinic is null');
//                    $qb->andWhere('u.enabled = 1');

                    return $er->findUsersWithAddressesIn($clinic);
                },
            ])
            ->getForm();

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_clinic_user_list', ['id' => $clinic->getId()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();
            $user = $userManager->fetch($task['user']->getId());
            if ($task['profile'] == User::ROLE_ADMIN){

                $saved = $userManager->createOwnerToClinic($user,$clinic);
            }

            if ($task['profile'] == User::ROLE_USER){

                $saved = $userManager->createClientToClinic($user,$clinic);
            }

            if ($task['profile']  == User::ROLE_EMPLOYEE){

                $saved = $userManager->createEmployeeToClinic($user,$clinic);
            }

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_clinic_user_list', ['id' => $clinic->getId()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }


        $routeName = "admin_clinic";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'information' => [
                        'name' => 'form.information',
                        'route' => $this->getUrl('admin_clinic_show', ['id' => $clinic->getId()]),
                    ],
                    'information1' => [
                        'name' => 'form.users',
                        'route' => $this->getUrl('admin_clinic_user_list', ['id' => $clinic->getId()]),
                    ],
                    'information2' => [
                        'name' => 'form.clinic.user.new',
                        'route' => null,
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/user/{userId}/delete", name="admin_clinic_user_delete")
     * @ParamConverter(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"userId": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function user_delete(Request $request, Clinic $clinic, User $user, UserManager $userManager)
    {

        $userClinical = $userManager->getRepository(Clinic::class)->getUserClinic($user,$clinic);
        $saved = false;
        if (!empty($userClinical)){
            $saved = $userManager->delete($userClinical);
            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_clinic_user_list', ['id' => $clinic->getId()]);
            }
            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }else{
            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }


        return $this->redirectToRoute('admin_clinic_user_list', ['id' => $clinic->getId()]);
        $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
    }




}
