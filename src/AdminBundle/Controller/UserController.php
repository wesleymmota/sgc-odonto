<?php

namespace AdminBundle\Controller;

use AdminBundle\Form\ChangePhotoType;
use AdminBundle\Form\UserFormType;
use AdminBundle\Manager\UserManager;
use AdminBundle\Model\UserModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Assets;
use BaseBundle\Entity\User;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use DoctorBundle\Form\ChangePasswordFormType;

/**
 * @Route("/admin/users")
 */
class UserController extends BaseController
{

    private $eventDispatcher;
    private $userManager;

    public function __construct(EventDispatcherInterface $eventDispatcher, UserManager $userManager)
    {
        $this->eventDispatcher = $eventDispatcher;

        $this->userManager = $userManager;
    }
    /**
     * @Route("/", name="admin_user_list")
     * @Template("admin/user/index.html.twig")
     */
    public function index(Request $request)
    {

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "razao"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
                [ 'id' => 'name', 'name' => 'form.name' , 'sorted' => 'true', 'style' => 'width: 250px;', 'card' => true],
                ['id'  => 'roles', 'name' => 'form.type' , 'sorted' => 'false', 'style' => 'width: 130px;',  'array' => true],
                [ 'id' => 'phone', 'name' => 'form.phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                ['id'  => 'cell_phone', 'name' => 'form.cell_phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'enabled', 'name' => 'form.status', 'sorted' => 'true' , 'bool'=> true, 'style' => 'width: 120px;'],
            ]
        ];

        $build = $this->userManager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

        $data = array_merge([ 'data' => $pagination ], $filter);


        $routeName = "admin_user_list";

        $breadcumbs = [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.admin_user_list',
                'route' =>  null,
                'items' => [
                ]
            ],
            '_route' => 'admin_user_list'
        ];

        $data = array_merge($breadcumbs, $data);
        return  $data;
    }

    /**
     * @Route("/{id}/info", name="admin_user_info")
     * @Template("admin/user/user-info.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
      */
    public function info(Request $request, User $user)
    {
        $routeName = "admin_user_list";

        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.admin_user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => '',
                    ]

                ]
            ],
            'user' => $user
        ];
    }

    /**
     * @Route("/{id}/state",
     *     name="admin_user_list_change_state",
     *       options = { "expose" = true },
     * )
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     *
     */
    public function changeState(Request $request, User $user)
    {
        if (null === $user) {
            return $this->redirectToRoute('admin_user_list');
        }
        $user->setEnabled(!$user->isEnabled());
        $saved = $this->userManager->merge($user);

        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        } else {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        }


        return new JsonResponse(array('state' => true));
    }

    /**
     * @Route("/{id}/edit", name="admin_user_edit")
     * @Template("admin/user/form/user-edit.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function edit(Request $request, User $user)
    {
        $profile = new UserModel($user);
        $session  = $request->getSession();
        $options = [
            'action' => $this->generateUrl('admin_user_edit', ['id' => $user->getUuid()]),
            'method' => 'POST',
            'validation_groups' => [
                'update_profile'
            ],
            ];

        $form = $this->createForm(UserFormType::class, $profile, $options);

        $userManager = $this->get('fos_user.user_manager');
        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_user_info', ['id' => $user->getUuid()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $profile = $form->getData();

//            $email_exist = $userManager->findUserByEmail($profile->getUser()->getEmail());
//
//            // Check if the user exists to prevent Integrity constraint violation error in the insertion
//            if($email_exist){
//
//                $request->getSession()->getFlashBag()->add('warning', 'register.user_exists');
//                return $this->redirectToRoute('admin_user_info', ['id' => $email_exist->getUuid()]);
//            }

            $profile->getUser()->setRoles($profile->getRoles());

            $saved = $this->userManager->merge($profile->getPersistUser());

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                if ($this->getUser()->getId() === $user->getId()) {
                    $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                }
                return $this->redirectToRoute('admin_user_info', ['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_user_list";

        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('admin_user_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => 'breadcumb.clinical.edit_user',
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }


    /**
     * @Route("/new", name="admin_user_new")
     * @Template("admin/user/form/user-new.html.twig")
     */
    public function newAction(Request $request)
    {
        $profile = new UserModel();

        $options = [
            'action' => $this->generateUrl('admin_user_new'),
            'method' => 'POST',
            'validation_groups' => [
                'new_patient'
            ],
        ];

        $userManager = $this->get('fos_user.user_manager');
        $form = $this->createForm(UserFormType::class, $profile, $options);


        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_user_info');
        }
        if ($form->isSubmitted() && $form->isValid()) {

            $profile = $form->getData();
            $user = $profile->getPersistUser();


            $email_exist = $userManager->findUserByEmail($user->getEmail());

            // Check if the user exists to prevent Integrity constraint violation error in the insertion
            if($email_exist){

                $request->getSession()->getFlashBag()->add('warning', 'register.user_exists');
                return $this->redirectToRoute('admin_user_info', ['id' => $email_exist->getUuid()]);
            }

            $saved = $userManager->createUser();

            $user->setEnabled(1);
            $userManager->updateUser($user);

            if ($saved) {

                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
                return $this->redirectToRoute('admin_user_info', ['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_user_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.admin_user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.new_user',

                    ],

                ]
            ],
            'form' =>  $form->createView(),
        ];
    }



    /**
     * @Route("/{id}/change-photo", name="admin_user_change_photo")
     * @Template("doctor/clinic/user/change_photo.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function changePhoto(Request $request, User $user)
    {
        $profile_upload_directory = "profiles/" . $user->getId();

        $asset = $user->getPhoto();
        if (null === $user->getPhoto()) {
            $asset = new Assets();
            $user->setPhoto($asset);
        }
        $asset->setTargetDir($profile_upload_directory);

        $form = $this->createForm(ChangePhotoType::class, $asset);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_user_info', ['id' => $user->getUuid()]);
        }


        if ($form->isSubmitted() && $form->isValid()) {

            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $assetFile = $asset->upload('BaseBundle::User');
            $storage = $this->get('base.google_storage');


            $image100x100= $profile_upload_directory . "/100x100-" . basename($assetFile);
            Image::open($assetFile)
                ->cropResize(200, 200)
                ->zoomCrop(250, 250)
                ->save($image100x100);

            $options = [
                'resumable' => true,
                'name' => $image100x100,
                'metadata' => [
                    'user_id' => $user->getId()
                ]
            ];

            if ($user->getPhoto()->getUrl() !== null) {
                $storage->deleteFile($user->getPhoto()->getUrl());
            }

            $fileName = $storage->uploadFile($image100x100, $options);

            $asset->setUrl($fileName);
            $asset->setUpdatedAt(new \DateTime());

            $user->setPhoto($asset);

            $this->userManager->merge($user);
            if ($this->getUser()->getId() === $user->getId()) {
                $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            }
            return $this->redirect($this->generateUrl('admin_user_info', ['id' => $user->getUuid()]));
        }

        $routeName = "admin_user_list";

        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.admin_user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('admin_user_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => 'breadcumb.clinical.user_change_photo',
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @Route("/{id}/change-password", name="admin_user_change_password")
     * @Template("admin/clinic/user/change_password.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function changePassword(Request $request, User $user, UserManager $userManager, UserPasswordEncoderInterface $passwordEncoder)
    {

        $form = $this->createForm(ChangePasswordFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userForm = $form->getData();


            $userForm->setPassword($passwordEncoder->encodePassword(
                $userForm,
                $userForm->getPlainPassword()
            ));
            $userForm->setPasswordRequestedAt(null);
            $userForm->setEnabled(TRUE);

            $saved = $userManager->merge($userForm);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                if ($this->getUser()->getId() === $user->getId()) {
                    $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                }
                return $this->redirect($this->generateUrl('admin_user_info', ['id' => $user->getUuid()]));
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_user_list";
        $route  = "admin_user_change_password";
        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.admin_user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('admin_user_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => 'profile.quick_actions.change_password',
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user,
            'route' => $route,
        ];
    }
}
