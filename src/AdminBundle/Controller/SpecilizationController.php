<?php

namespace AdminBundle\Controller;

use AdminBundle\Manager\ClinicManager;
use AdminBundle\Manager\SpecializationManager;
use AdminBundle\Model\ClinicModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/specilization")
 */
class SpecilizationController extends BaseController
{
    /**
     * @Route("/", name="admin_specilization_list")
     * @Template("admin/specilization/index.html.twig")
     */
    public function indexAction(Request $request, SpecializationManager $manager)
    {
        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "id"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
                [ 'id' => 'name', 'name' => 'form.name' , 'sorted' => 'true', 'style' => 'width: 600px;'],
                [ 'id' => 'updatedAt', 'name' => 'form.updatedAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'createdAt', 'name' => 'form.createdAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 100px;'],

            ]
        ];

        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

//
//        $request->getSession()->getFlashBag()->add('success', 'Cartão cadastrado com sucesso.');
//        $request->getSession()->getFlashBag()->add('error', 'Ocorreu um erro ao cadastrar o cartão.');
        $data = array_merge([ 'data' => $pagination ], $filter);


        return $data;
    }

    /**
     * @Route("/new", name="admin_specilization_new")
     * @Template("admin/specilization/new.html.twig")
     */
    public function new(Request $request, SpecializationManager $manager)
    {
        $task = new Specialization();


        $form = $this->createFormBuilder($task)
            ->setAction($this->generateUrl('admin_specilization_new'))
            ->setMethod('POST')
            ->add('name', TextType::class, array('label' => 'form.label.name'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $saved = $manager->save($task);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_specilization_list');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_specilization_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.specialization',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'specialization.new' => [
                        'name' =>  'header.specialization.new'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/new/{id}", name="admin_specilization_edit")
     * @ParamConverter(
     *     "specialization",
     *     class="BaseBundle:Specialization",
     *     options={"id": "id"}
     * )
     * @Template("admin/specilization/edit.html.twig")
     */
    public function edit(Request $request, Specialization $specialization, SpecializationManager $manager)
    {
        $form = $this->createFormBuilder($specialization)
            ->setAction($this->generateUrl('admin_specilization_edit', ['id'=> $specialization->getId() ]))
            ->setMethod('POST')
            ->add('name', TextType::class, array('label' => 'form.label.name'))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $specializationForm = $form->getData();


            $saved = $manager->save($specializationForm);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_specilization_list');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_specilization_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.specialization',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'specialization.new' => [
                        'name' =>  'header.specialization.new'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", name="admin_specilization_delete")
     * @ParamConverter(
     *     "specialization",
     *     class="BaseBundle:Specialization",
     *     options={"id": "id"}
     * )
     */
    public function delete(Request $request, Specialization $specialization, SpecializationManager $manager)
    {
        if ($specialization instanceof Specialization && $specialization->getId() > 0) {
            $saved = $manager->delete($specialization);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.removed.if.success');

                return $this->redirectToRoute('admin_specilization_list');
            }
        }

        $request->getSession()->getFlashBag()->add('danger', 'register.removed.if.notsuccess');
        return $this->redirectToRoute('admin_specilization_list');
    }
}
