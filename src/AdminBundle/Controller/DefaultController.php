<?php

namespace AdminBundle\Controller;

use BaseBundle\Controller\BaseController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class DefaultController extends BaseController
{
    /**
     * @Route("/area", name="admin_area")
     * @Template("admin/dashboard/index.html.twig")
     */
    public function indexAction()
    {
        return $this->redirectToRoute('admin_clinic');
    }
}
