<?php


namespace AdminBundle\Form;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClinicSettingsFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('domain', TextType::class, ['label' => 'clinic.label.domain'])
            ->add('port', TextType::class, ['label' => 'clinic.label.port'])
            ->add('name', TextType::class, ['label' => 'clinic.label.name'])

            ->add('locale', ChoiceType::class, [
                'label' => 'clinic.label.locale',
                'choices' => Clinic::getAvailableLocale(),
                'choice_label' => function ($choice) {
                    return Clinic::getLocaleName($choice);
                },
            ])

            ->add('logo', ClinicLogoType::class, array('label' => 'clinic.label.logo'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

                'data_class' => ClinicalSettings::class

            ]);
    }
}
