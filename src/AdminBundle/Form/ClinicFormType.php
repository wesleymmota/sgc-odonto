<?php


namespace AdminBundle\Form;

use AdminBundle\Model\ClinicModel;
use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ClinicFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fantasia', TextType::class, ['label' => 'clinic.label.fantasia'])
            ->add('razao', EmailType::class, ['label' => 'clinic.label.razao'])

            ->add('ie', TextType::class, ['label' => 'clinic.label.ie'])
            ->add('cnpj', TextType::class, ['label' => 'clinic.label.cnpj'])
            ->add('address', TextType::class, ['label' => 'clinic.label.address'])

            ->add('neighborhood', TextType::class, ['label' => 'clinic.label.neighborhood'])
            ->add('zip_code', TextType::class, ['label' => 'clinic.label.zip_code'])
            ->add('number', TextType::class, ['label' => 'clinic.label.number'])
            ->add('city', EntityType::class, [
                'label' => 'clinic.label.city',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:City',
                'choice_label' => function ($choice) {
                    return  $choice->getName() . " - ". $choice->getUf();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.uf', 'ASC') ;
                },
            ])
            ->add('settings', ClinicSettingsFormType::class)
//            ->add('domain', TextType::class, ['label' => 'clinic.label.address'])
//            ->add('owner', EntityType::class, [
//                'label' => 'clinic.label.owner',
//                'attr' => [
//                    'class' => 'm-select2 m-select2--air m-select2--pill'
//                ],
//                'class' => 'BaseBundle:User',
//                'choice_label' => function ($choice) {
//                    return  $choice->getName() . " - ". $choice->getEmail();
//                },
//                'query_builder' => function (EntityRepository $er) {
//                    $qb = $er->createQueryBuilder('u');
//                    $qb->where('u.enabled = 1');
//                    $qb->where($qb->expr()->orX(
//                        $qb->expr()->like('u.roles', ':roles1'),
//                        $qb->expr()->like('u.roles', ':roles2'),
//                        $qb->expr()->like('u.roles', ':roles3')
//                    ))
//                    ->setParameter('roles1', '%ROLE_ADMIN%')
//                    ->setParameter('roles2', '%ROLE_DOCTOR%')
//                    ->setParameter('roles3', '%ROLE_SUPER_ADMIN"%');
//
//                    return $qb->orderBy('u.name');
//                },
//            ])


            ->add('phone', TextType::class, ['label' => 'user.label.phone'])
            ->add('cell_phone', TextType::class, ['label' => 'user.label.cell_phone'])
            ->add('id', HiddenType::class)
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([

                'data_class' => ClinicModel::class

            ]);
    }
}
