<?php


namespace AdminBundle\Form;

use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'user.label.usernameCanonical'])
            ->add('email', EmailType::class, ['label' => 'user.label.email'])
            ->add('rg', TextType::class, ['label' => 'user.label.rg'])
            ->add('cpf', TextType::class, ['label' => 'user.label.cpf'])
            ->add('address', TextType::class, ['label' => 'user.label.address'])
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))

            ->add('neighborhood', TextType::class, ['label' => 'user.label.neighborhood'])
            ->add('zip_code', TextType::class, ['label' => 'user.label.zip_code'])
            ->add('number', TextType::class, ['label' => 'user.label.number'])
            ->add('city', EntityType::class, [
                'label' => 'user.label.city',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:City',
                'choice_label' => function ($choice) {
                    return  $choice->getName() . " - ". $choice->getUf();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.uf', 'ASC') ;
                },
            ])

            ->add(
                'birth_date',
                BirthdayType::class,
                [
                    'label' => 'user.label.birth_date',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy'
                    ]
                ]
            )


            ->add('phone', TextType::class, ['label' => 'user.label.phone'])
            ->add('cell_phone', TextType::class, ['label' => 'user.label.cell_phone'])
            ->add('state_civil', ChoiceType::class, [
                'label' => 'user.label.state_civil',
                'choices' => User::getAvailableStateCivil(),
                'choice_label' => function ($choice) {
                    return User::getStateCivilName($choice);
                },
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'user.label.gender',
                'choices' => User::getAvailableGender(),
                'choice_label' => function ($choice) {
                    return User::getGenderName($choice);
                },
            ])

            ->add('cell_phone', TextType::class, ['label' => 'user.label.cell_phone'])
            ->add('notes', TextareaType::class, ['label' => 'user.label.notes'])
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
            ->add('id', HiddenType::class)
        ;

        /// Profile doctor only admin
        if ($options['include_doctor'] === true) {
            $builder->add('specialization', EntityType::class, [
                'label' => 'user.label.specialization',
                'class' => 'BaseBundle:Specialization',
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
            ])
                ->add('cro', TextType::class, ['label' => 'user.label.cro'])
                ->add(
                    'admission_date',
                    DateType::class,
                    [
                        'label' => 'user.label.admission_date',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'form-control input-inline datepicker',
                            'data-provide' => 'datepicker',
                            'data-date-format' => 'dd/mm/yyyy'
                        ]
                    ]
                )

            ;
        }

        // Profile employee
        if ($options['include_employee'] === true) {
            $builder->add('specialization', EntityType::class, [
                    'label' => 'user.label.specialization',
                    'class' => 'BaseBundle:Specialization',
                    'choice_label' => 'name',
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.name', 'ASC');
                    },
                ])
                ->add('cro', TextType::class, ['label' => 'user.label.cro'])
                ->add(
                    'admission_date',
                    DateType::class,
                    [
                        'label' => 'user.label.admission_date',
                        'widget' => 'single_text',
                        'format' => 'dd/MM/yyyy',
                        'attr' => [
                            'class' => 'form-control input-inline datepicker',
                            'data-provide' => 'datepicker',
                            'data-date-format' => 'dd/mm/yyyy'
                        ]
                    ]
                )

            ;
        }

        // Profile client
        if ($options['include_client'] === true) {
            $builder->add('profession', TextType::class, ['label' => 'user.label.profession'])
                ->add('roles', ChoiceType::class, [
                    'label' => 'user.label.roles',
                    'choices' => ['ROLE_USER' => 'ROLE_USER'],
                    'choice_label' => function ($choice) {
                        return User::getStateRole($choice);
                    },
                ])
            ;
        }

        // include_is_admin
//        if ($options['include_is_clinical_admin'] === true) {
//            $builder
//                ->add('roles', ChoiceType::class, [
//                    'label' => 'user.label.roles',
//                    'choices' => User::getClinicAdminRoles(),
//                    'choice_label' => function ($choice) {
//                        return User::getStateRole($choice);
//                    },
//                ])
//
//            ;
//        }
//
//        // include_is_employee
//        if ($options['include_is_employee'] === true) {
//            $builder
//                ->add('roles', HiddenType::class,['value'=> 'ROLE_USER_AREA'])
//                ;
//        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'include_is_admin' => false,
                'include_doctor' => false,
                'include_employee' => false,  // doctor and employee
                'include_client' => false,
                'include_edit' => false,
                'include_is_clinical_admin' => false,
                'include_is_employee' => false,
                'data_class' => UserModel::class,
                'validation_groups' => [
                    'update_profile',
                    'new_doctor',
                    'new_employee',
                    'new_patient'
                ],
            ]);
    }
}
