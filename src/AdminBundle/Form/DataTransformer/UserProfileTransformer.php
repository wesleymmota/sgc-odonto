<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 27/05/18
 * Time: 10:25
 */

namespace AdminBundle\Form\DataTransformer;


use BaseBundle\Entity\UserClinical;
use Symfony\Component\Form\DataTransformerInterface;

class UserProfileTransformer implements DataTransformerInterface
{

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $issue
     * @return string
     */
    public function transform($role)
    {


         return $role[0];
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $issueNumber
     * @return Issue|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($role)
    {

        return [$role];
    }
}