<?php


namespace AdminBundle\Manager;

use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class SpecializationManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, Specialization::class);
    }
}
