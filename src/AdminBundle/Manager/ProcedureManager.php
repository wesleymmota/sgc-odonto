<?php


namespace AdminBundle\Manager;

use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class ProcedureManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, Procedure::class);
    }
}
