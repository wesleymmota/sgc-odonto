<?php


namespace AdminBundle\Manager;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\User;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class ClinicManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, Clinic::class);
    }
}
