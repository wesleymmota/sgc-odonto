<?php


namespace AdminBundle\Manager;

use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class UserManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, User::class);
    }


    public function createClientToClinic($user, $clinic)
    {
        $user->addRole(User::ROLE_CLIENT);
        $userClinical = new UserClinical($user, $clinic);
        $userClinical->setClient(true);
        $userClinical->setLastAccess(true);


        $this->getManager()->persist($userClinical);
        $this->getManager()->flush();
        return $user;
    }

    public function createOwnerToClinic($user, $clinic)
    {
        $userClinical = new UserClinical($user, $clinic);
        $userClinical->setOwner(true);
        $userClinical->setLastAccess(true);

        $this->getManager()->persist($userClinical);

        $this->getManager()->flush();

        return $user;
    }

    public function createEmployeeToClinic($user, $clinic)
    {
        $userClinical = new UserClinical($user, $clinic);
        $userClinical->setEmployee(true);
        $userClinical->setLastAccess(true);

        $this->getManager()->persist($userClinical);

        $this->getManager()->flush();

        return $user;
    }


    public function createUser($user)
    {
        $this->getManager()->persist($user);

        $this->getManager()->flush();

        return $user;
    }
}
