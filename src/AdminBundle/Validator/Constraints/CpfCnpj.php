<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 28/10/18
 * Time: 23:15
 */

namespace AdminBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
/**
 * @Annotation
 * @Target({"PROPERTY", "METHOD", "ANNOTATION"})
 *
 * @api
 */
class CpfCnpj extends Constraint
{
    public $cpf = false;
    public $cnpj = false;
    public $mask = false;
    public $messageMask = 'O {{ type }} não está em um formato válido.';
    public $message = 'O {{ type }} informado é inválido.';
}