<?php

namespace AdminBundle\Model;

use BaseBundle\BaseBundle;
use BaseBundle\Entity\Model\ModelBase;
use BaseBundle\Entity\User;
use DMS\Filter\Rules as Filter;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Zend\Hydrator\Reflection;
use Symfony\Component\Validator\Constraints as Assert;
use AdminBundle\Validator\Constraints as BFOSBrasilAssert;
/**
 * User model
 * Groups:
 *  - update_profile => make use in update profile all fields
 *  - new_doctor => Nome - CPF - RG - Data de Nascimento - Celular -
 *                  Telefone - Sexo - Endereço - Bairro - CEP - Estado
 *                  Cidade - CRO - Especialização - Data de Admissão
 *                  LOGIN/email, senha e role
 *  - new_employee => Nome - CPF - RG - Data de Nascimento - Celular
 *                  Telefone - Sexo - Estado Civil - Endereço - Bairro
 *                  CEP - Estado - Cidade - Especialização - Data de Admissão - Observação
 *                  LOGIN/email, senha e role
 *  - new_patient => Nome - CPF - RG - Foto - Data de nascimento Sexo - Telefone - Celular
 *                  Estado Civil - Profissão - Endereço - Bairro - CEP - Estado - Cidade
 *                  Tipo de Plano - como chegou a clínica = Observação/notes
 *                  LOGIN/email, senha e role
 *
 */
class UserModel extends ModelBase
{
    /**
     * @var \BaseBundle\Entity\User
     */
    protected $user;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     */
    protected $password;

    /**
     * @var integer
     * @Assert\Type(type = "numeric")
     */
    protected $id;

    /**email
     * @var string
     * @Assert\NotBlank(
     *     message = "validate_user.name.blank"
     *)
     *
     */
    protected $name;


    /**email
     * @var string
     * @Assert\NotBlank( groups={"update_profile","new_doctor","new_employee","new_patient"}
     *     message = "validate_user.username.blank"
     *)
     *
     */
    protected $username;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     *
     */
    protected $rg;

    /**
     * @var string
     *
     */
    protected $gender;

    /**
     * @var string
     *
     */
    protected $cpf;

    /**
     * @var string
     *
     */
    protected $address;

    /**
     * @var string
     *
     */
    protected $postal_code;

    /**
     * @var string Bairro
     *
     */
    protected $neighborhood;

    /**
     * @var string
     *
     */
    protected $zip_code;

    /**
     * @var string
     *
     */
    protected $number;

    /**
     * @var BaseBundle\Entity\City
     */
    protected $city;

    /**
     * @var \DateTime
     *
     */
    protected $admission_date;

    /**
     * @var \DateTime
     *
     */
    protected $birth_date;

    /**
     * @var string
     *
     */
    protected $phone;

    /**
     * @var string
     *
     */
    protected $cell_phone;

    /**
     * @var string
     */
    protected $state_civil;

    /**
     * @var string
     *
     */
    protected $cro;

    /**
     * @var string
     *
     */
    protected $profession;

    /**
     *  @var BaseBundle\Entity\Assets
     *
     */
    protected $photo;

    /**
     * @var string
     *
     */
    protected $notes;


    /**
     * @var BaseBundle\Entity\Specialization
     *
     */
    protected $specialization;


    /**
     * @var \BaseBundle\Entity\Clinic
     */
    protected $clinic;

    /**
     * @var \BaseBundle\Entity\Clinic
     */
    protected $roles;

    /**
     * @var \BaseBundle\Entity\Clinic
     */
    protected $profile;


    protected $plainPassword;

    /**
     * @var \BaseBundle\Entity\Clinic
     */
    protected $clinicalManager;

    public function __construct(User $user = null)
    {

        if (null != $user) {
            $this->user = $user;
            (new Reflection())->hydrate($user->toArray(), $this);
        } else {
            $this->user = new User();
        }
    }

    function generate_unique_username($string_name="Mike Tyson", $rand_no = 200){

        while(true){
            $username_parts = array_filter(explode(" ", strtolower($string_name))); //explode and lowercase name
            $username_parts = array_slice($username_parts, 0, 2); //return only first two arry part

            $part1 = (!empty($username_parts[0]))?substr($username_parts[0], 0,8):""; //cut first name to 8 letters
            $part2 = (!empty($username_parts[1]))?substr($username_parts[1], 0,5):""; //cut second name to 5 letters
            $part3 = ($rand_no)?rand(0, $rand_no):"";

            $username = $part1. str_shuffle($part2). $part3; //str_shuffle to randomly shuffle all characters

            if($username){
                return $username;
            }
        }
    }


    public function getPersistUser()
    {
        foreach (get_object_vars($this) as $key => $val) {
            if (! is_object($val) && ! is_null($val)) {
                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));


                if (method_exists($this->getUser(), $setMethod)) {
                    if ($setMethod == "setRoles") {
                        continue;
                    }
                    $this->getUser()->$setMethod($val);
                }
            }
        }

        $this->getUser()->setEmailCanonical($this->getUser()->getEmailCanonical());

        $this->getUser()->setAdmissionDate($this->getAdmissionDate());

        $this->getUser()->setBirthDate($this->getBirthDate());

        $this->setConfirmationToken(null);
        if ($this->getUser()->getUsername() == NULL && NULL !== $this->getUser()->getEmail()) {
            $username = strstr($this->getUser()->getEmail(), '@', true);
            $this->getUser()->setUsername($this->getUser()->getEmail());
        }else{
            $username = $this->generate_unique_username($this->getUser()->getName());
            $this->getUser()->setUsername($username);
//            $this->getUser()->setEmail("${username}@test.com");
        }

        if($this->getSpecialization() instanceof \BaseBundle\Entity\Specialization) {
            $this->getUser()->setSpecialization($this->getSpecialization());
        }
        if ($this->getCity() instanceof \BaseBundle\Entity\City) {
            $this->getUser()->setCity($this->getCity());
        }
        if (!empty($this->getRoles())) {
            $this->getUser()->setRoles($this->getRoles());
        }
//        if (is_array($this->getRoles())) {
//            $this->getUser()->setRoles($this->getRoles());
//        }
//
//        if (is_string($this->getRoles())) {
//            $this->getUser()->addRole($this->getRoles());
//        }
//        if (! empty($this->getRoles()) && $this->getUser()->getId() < 1) {
//
//            $this->getUser()->addRole($this->getRoles());
//        }
//        if($this->getClinicalManager() instanceof \BaseBundle\Entity\Clinic) {
//            $this->getUser()->setClinicalManager($this->getClinicalManager());
//        }

        return $this->getUser();
    }

    /**
     * @return \DateTime
     */
    public function getAdmissionDate()
    {
        return $this->admission_date;
    }

    /**
     * @param \DateTime $admission_date
     * @return UserModel
     */
    public function setAdmissionDate($admission_date)
    {
        $this->admission_date = $admission_date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @param \DateTime $birth_date
     * @return UserModel
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
        return $this;
    }

    /**
     * @return BaseBundle\Entity\Specialization
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * @param BaseBundle\Entity\Specialization $specialization
     * @return UserModel
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;
        return $this;
    }

    /**
     * @return \BaseBundle\Entity\Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * @param \BaseBundle\Entity\Clinic $clinic
     * @return UserModel
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }

    /**
     * @return BaseBundle\Entity\City
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param BaseBundle\Entity\City $city
     * @return UserModel
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return \BaseBundle\Entity\Clinic
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @param \BaseBundle\Entity\Clinic $roles
     * @return UserModel
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @return \BaseBundle\Entity\Clinic
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * @param \BaseBundle\Entity\Clinic $profile
     * @return UserModel
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;
        return $this;
    }
}
