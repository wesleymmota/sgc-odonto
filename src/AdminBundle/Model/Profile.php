<?php

namespace AdminBundle\Model;

use BaseBundle\Entity\Model\ModelBase;
use BaseBundle\Entity\User;
use DMS\Filter\Rules as Filter;
use Zend\Hydrator\Reflection;

/**
 * Profile
 */
class Profile extends ModelBase
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;

        (new Reflection())->hydrate($user->toArray(), $this);
    }

    public function getPersistUser()
    {
        foreach (get_object_vars($this) as $key => $val) {
            if (! is_object($val) && ! is_null($val)) {
                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));


                if (method_exists($this->getUser(), $setMethod)) {
                    $this->getUser()->$setMethod($val);
                }
            }
        }

        $this->getUser()->setEmailCanonical($this->getUser()->getEmailCanonical());

        return $this->getUser();
    }



    protected $id;
    protected $usernameCanonical;
    protected $rg;
    protected $cpf;
    protected $address;
    protected $neighborhood;
    protected $zip_code;
    protected $number;
    protected $city;
    protected $birth_date;
    protected $phone;
    protected $cell_phone;
    protected $state_civil;
    protected $cro;
    protected $profession;
    protected $notes;
    protected $email;
    protected $specialization;
}
