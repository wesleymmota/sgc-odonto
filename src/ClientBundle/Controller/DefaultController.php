<?php

namespace ClientBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
      * @Route("/client/area", name="client_area")
      * @Template("client/dashboard/index.html.twig")
      */

    public function indexAction()
    {
        return [];
    }
}
