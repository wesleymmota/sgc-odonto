<?php


namespace DoctorBundle\Form;

use AdminBundle\Form\DataTransformer\UserProfileTransformer;
use AdminBundle\Form\DataTransformer\UserRoleTransformer;
use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdminFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label' => 'user.label.usernameCanonical', 'required' => true])
            ->add('email', EmailType::class, ['label' => 'user.label.email', 'required' => true])
            ->add('rg', TextType::class, ['label' => 'user.label.rg', 'required' => false])
            ->add('cpf', TextType::class, ['label' => 'user.label.cpf', 'required' => false])
            ->add('address', TextType::class, ['label' => 'user.label.address', 'required' => false])
            ->add('neighborhood', TextType::class, ['label' => 'user.label.neighborhood', 'required' => false])
            ->add('zip_code', TextType::class, ['label' => 'user.label.zip_code', 'required' => false])
            ->add('number', TextType::class, ['label' => 'user.label.number', 'required' => false,])
            ->add('city', EntityType::class,
                [
                    'label' => 'user.label.city',
                    'required' => false,
                    'attr' => [
                        'class' => 'm-select2 m-select2--air m-select2--pill'
                    ],
                    'class' => 'BaseBundle:City',
                    'choice_label' => function ($choice) {
                        return $choice->getName() . " - " . $choice->getUf();
                    },
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.uf', 'ASC');
                    },
                ])
            ->add('birth_date', BirthdayType::class,
                [
                    'label' => 'user.label.birth_date',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy'
                    ]
                ]
            )
            ->add('phone', TextType::class, ['label' => 'user.label.phone', 'required' => false])
            ->add('cell_phone', TextType::class, ['label' => 'user.label.cell_phone', 'required' => false])
            ->add('state_civil', ChoiceType::class, [
                'label' => 'user.label.state_civil',
                'choices' => User::getAvailableStateCivil(),
                'empty_data' => User::STATE_CIVIL_SINGLE,
                'choice_label' => function ($choice) {
                    return User::getStateCivilName($choice);
                },
            ])
            ->add('gender', ChoiceType::class, [
                'label' => 'user.label.gender',
                'choices' => User::getAvailableGender(),
                'empty_data' => User::GENDER_MALE,
                'choice_label' => function ($choice) {
                    return User::getGenderName($choice);
                },
            ])
            ->add('notes', TextareaType::class, ['label' => 'user.label.notes', 'required' => false])
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
            ->add('id', HiddenType::class);
        ;


        // Profile employee
        $builder->add('specialization', EntityType::class, [
                'label' => 'user.label.specialization',
                'class' => 'BaseBundle:Specialization',
                'required' => false,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.name', 'ASC');
                },
            ])
            ->add('cro', TextType::class, ['label' => 'user.label.cro'])
            ->add('admission_date', DateType::class,
                [
                    'label' => 'user.label.admission_date',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => true,
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy'
                    ]
                ]
            ) ;
            if ($options['exclude_roles'] === TRUE) {
                $builder->add('roles', ChoiceType::class, [
                    'label' => 'user.label.roles',
                    'choices' => User::getClinicAdminRoles(),
                    'choice_label' => function ($choice) {
                        return User::getStateRole($choice);
                    },
                ])
                    ->add('profile', ChoiceType::class, [
                        'label' => 'user.label.profile',
                        'expanded' => true,
                        'required' => true,
                        'choices' => User::getProfilesRoles(),
                        'choice_label' => function ($choice) {
                            return User::getStateRole($choice);
                        },
                    ]);
                $builder->get('roles')->addModelTransformer(new UserRoleTransformer());
            }



        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $user = $event->getData();
            $form = $event->getForm();



            // checks if the User object is "new"
            // If no data is passed to the form, the data is "null".
            // This should be considered a new "User"
            if (!$user || null === $user->getId()) {

                $form->add('plainPassword', RepeatedType::class, array(
                    'type' => PasswordType::class,
                    'options' => array(
                        'translation_domain' => 'FOSUserBundle',
                        'attr' => array(
                            'autocomplete' => 'new-password',
                        ),
                    ),
                    'first_options' => array('label' => 'form.new_password'),
                    'second_options' => array('label' => 'form.new_password_confirmation'),
                    'invalid_message' => 'fos_user.password.mismatch',
                ));
            }
        });




        // include_is_admin
//        if ($options['include_is_clinical_admin'] === true) {
//            $builder
//                ->add('roles', ChoiceType::class, [
//                    'label' => 'user.label.roles',
//                    'choices' => User::getClinicAdminRoles(),
//                    'choice_label' => function ($choice) {
//                        return User::getStateRole($choice);
//                    },
//                ])
//
//            ;
//        }
//
//        // include_is_employee
//        if ($options['include_is_employee'] === true) {
//            $builder
//                ->add('roles', HiddenType::class,['value'=> 'ROLE_USER_AREA'])
//                ;
//        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'exclude_roles' => TRUE,
                'data_class' => UserModel::class,
                'validation_groups' => [
                    'update_profile',
                    'new_doctor',
                    'new_employee',
                    'new_patient'
                ],
            ]);
    }

    /**
     * Returns the prefix of the template block name for this type.
     *
     * The block prefix defaults to the underscored short class name with
     * the "Type" suffix removed (e.g. "UserProfileType" => "user_profile").
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix()
    {
        return "profile_form";
    }
}
