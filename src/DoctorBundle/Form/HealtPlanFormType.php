<?php


namespace DoctorBundle\Form;

use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HealtPlanFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fantasia', TextType::class, ['label' => 'clinic.label.health_plan.fantasia','required' => true])
            ->add('razao', TextType::class, ['label' => 'clinic.label.health_plan.razao','required' => false])
            ->add('site', TextType::class, ['label' => 'clinic.label.health_plan.site','required' => false])
            ->add('email', EmailType::class, ['label' => 'clinic.label.health_plan.email','required' => false])
            ->add('ie', TextType::class, ['label' => 'clinic.label.health_plan.ie','required' => false])
            ->add('cnpj', TextType::class, ['label' => 'clinic.label.health_plan.cnpj','required' => false])

            ->add('address', TextType::class, ['label' => 'clinic.label.health_plan.address','required' => false])
             ->add('neighborhood', TextType::class, ['label' => 'clinic.label.health_plan.neighborhood','required' => false])
            ->add('zip_code', TextType::class, ['label' => 'clinic.label.health_plan.zip_code','required' => false])
            ->add('number', TextType::class, ['label' => 'clinic.label.health_plan.number','required' => false])
            ->add('city', EntityType::class, [
                'label' => 'clinic.label.health_plan.city',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'required' => false,
                'class' => 'BaseBundle:City',
                'choice_label' => function ($choice) {
                    return  $choice->getName() . " - ". $choice->getUf();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.uf', 'ASC') ;
                },
            ])


            ->add('phone', TextType::class, ['label' => 'clinic.label.health_plan.phone','required' => false])
            ->add('cell_phone', TextType::class, ['label' => 'clinic.label.health_plan.cell_phone','required' => false])
            ->add('notes', TextareaType::class, ['label' => 'user.label.notes','required' => false])
            ->add('cancel', SubmitType::class, ['label' => 'form.cancel'])
            ->add('id', HiddenType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'data_class' => Agreement::class,
            ]);
    }

    /**
     * Returns the prefix of the template block name for this type.
     *
     * The block prefix defaults to the underscored short class name with
     * the "Type" suffix removed (e.g. "UserProfileType" => "user_profile").
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix()
    {
        return "profile_form";
    }
}
