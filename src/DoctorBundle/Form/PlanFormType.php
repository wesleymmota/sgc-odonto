<?php


namespace DoctorBundle\Form;

use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use DoctorBundle\Form\DataTransformer\ProcedureoToPlanProcedureTransformer;
use DoctorBundle\Form\DataTransformer\UserClinicalToUserTransformer;
use DoctorBundle\Model\BudgetModel;
use DoctorBundle\Model\PlanModel;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder
            //->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
           // ->add('id', HiddenType::class)
            ->add('details', TextareaType::class, ['label' => 'clinic.label.budget.plan.details','required' => false])

            ->add('professional', EntityType::class, [
                'label' => 'clinic.label.budget.profissional',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:User',
                'choice_label' => function ($choice) {
                    return  $choice->getFullName();
                },
                'query_builder' => function (EntityRepository $er) use  ($options) {
                    $clinic = $options['clinic_class'];
                    $qb = $er->createQueryBuilder('u');
                    $qb->distinct()
                        ->join('\BaseBundle\Entity\UserClinical', 'uc', \Doctrine\ORM\Query\Expr\Join::WITH, 'u = uc.user')
                        ->join('\BaseBundle\Entity\Clinic', 'c', \Doctrine\ORM\Query\Expr\Join::WITH, 'c = uc.clinic')
                    ;

                    $qb->where($qb->expr()->andX(
                        $qb->expr()->eq('u.enabled', '1'),
                        $qb->expr()->eq('c.id', ':clinic'),
                        $qb->expr()->like('u.roles', ':role')
                    ))

                    ->setParameters(
                        [
                            'clinic' => $clinic instanceof Clinic ? $clinic->getId() :  $clinic,
                            'role' => '%ROLE_DOCTOR%'
                        ]
                    );

                    return $qb;
                },
            ])

            ->add('procedures', EntityType::class, [
                'label' => 'clinic.label.budget.plan.procedures',
                'multiple' => TRUE,
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:Procedure',
                'choice_label' => function ($choice) {
                    return  $choice->getDescription();
                },
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->orderBy('u.id', 'ASC') ;
                },
            ])


            ;
        if ($options['dental_record_edit'] === true) {
            $builder
                ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
                 ->add('id', HiddenType::class)
            ->add(
                'date',
                DateType::class,
                [
                    'label' => 'clinic.label.budget.plan.procedures.data',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control input-inline datepicker',
                        'data-provide' => 'datepicker',
                        'data-date-format' => 'dd/mm/yyyy'
                    ]
                ]
            );
        }

        //$builder->get('professional')->addModelTransformer(new UserClinicalToUserTransformer());
        $builder->get('procedures')->addModelTransformer(new ProcedureoToPlanProcedureTransformer());
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'clinic_class' => false,
                'dental_record_edit' => false,
                'data_class' => Plan::class
            ]);
    }

    /**
     * Returns the prefix of the template block name for this type.
     *
     * The block prefix defaults to the underscored short class name with
     * the "Type" suffix removed (e.g. "UserProfileType" => "user_profile").
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix()
    {
        return "profile_form";
    }
}
