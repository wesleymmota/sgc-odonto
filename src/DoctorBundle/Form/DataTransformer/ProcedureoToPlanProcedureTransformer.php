<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 27/05/18
 * Time: 10:25
 */

namespace DoctorBundle\Form\DataTransformer;


use BaseBundle\Entity\Plan;
use BaseBundle\Entity\PlanProcedure;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\UserClinical;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\DataTransformerInterface;

class ProcedureoToPlanProcedureTransformer implements DataTransformerInterface
{
//    private $plan;
//
//    public function __construct(Plan $plan)
//    {
//        $this->plan = $plan;
//    }

    /**
     * Transforms an object (issue) to a string (number).
     *
     * @param  Issue|null $issue
     * @return string
     */
    public function transform($planProcedures)
    {

        $newProcedures = new ArrayCollection();
        foreach ($planProcedures as $planProcedure) {

            if ($planProcedure instanceof PlanProcedure) {
                $procedure =  $planProcedure->getProcedure();

                if (! $newProcedures->contains($procedure)) {
                    $newProcedures->add($procedure);
                }

            }
        }

        return $newProcedures;
    }

    /**
     * Transforms a string (number) to an object (issue).
     *
     * @param  string $issueNumber
     * @return Issue|null
     * @throws TransformationFailedException if object (issue) is not found.
     */
    public function reverseTransform($procedures)
    {
        $newProcedures = new ArrayCollection();
        foreach ($procedures as $procedure) {
            if ($procedure instanceof Procedure) {
                $planProcedure =  new PlanProcedure();
                $planProcedure->setProcedure($procedure);
                    if (! $newProcedures->contains($planProcedure)) {
                        $newProcedures->add($planProcedure);
                    }

            }
        }
        return $newProcedures;
    }
}