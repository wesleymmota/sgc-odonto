<?php


namespace DoctorBundle\Form;

use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\PlanFile;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use DoctorBundle\Form\DataTransformer\ProcedureoToPlanProcedureTransformer;
use DoctorBundle\Form\DataTransformer\UserClinicalToUserTransformer;
use DoctorBundle\Model\BudgetModel;
use DoctorBundle\Model\PlanModel;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PlanUploadFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {


        $builder

            ->add('file', FileType::class, array(
                    'multiple' => false,
                    'data_class' => null,
                    'required' => false,
                )
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                 'data_class' => PlanFile::class
            ]);
    }

    /**
     * Returns the prefix of the template block name for this type.
     *
     * The block prefix defaults to the underscored short class name with
     * the "Type" suffix removed (e.g. "UserProfileType" => "user_profile").
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix()
    {
        return "profile_form";
    }
}
