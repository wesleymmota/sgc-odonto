<?php


namespace DoctorBundle\Form;

use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use DoctorBundle\Form\DataTransformer\UserClinicalToUserTransformer;
use DoctorBundle\Model\BudgetModel;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BudgetFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cancel', SubmitType::class, array('label' => 'form.cancel'))
           // ->add('id', HiddenType::class)
           ->add(
               'validity',
               DateType::class,
               [
                   'data' => new \DateTime("now"),
                   'label' => 'clinic.label.budget.validity',
                   'widget' => 'single_text',
                   'format' => 'dd/MM/yyyy',
                   'attr' => [
                       'class' => 'form-control input-inline datepicker',
                       'data-provide' => 'datepicker',
                       'data-date-format' => 'dd/mm/yyyy'
                   ]
               ]
           )
            ->add('payment', ChoiceType::class, [
                'label' => 'clinic.label.budget.payment_method',
                'choices' => Budget::getPaymentMethods(),
                'choice_label' => function ($choice) {
                    return Budget::getPaymentMethod($choice);
                },
            ])
            ->add('tooth', ChoiceType::class, [
                'label' => 'clinic.label.budget.tooth',
                'choices' => Budget::getThootElements(),
                'choice_label' => function ($choice) {
                    return $choice;
                },
            ])
            ->add('value', NumberType::class, ['label' => 'clinic.label.budget.value','scale' => 2,'required' => true])
            ->add('discount', IntegerType::class, ['label' => 'clinic.label.budget.discount','required' => false, 'attr' => ['min' => 0, 'max' => 100]])


            ->add('agreement', EntityType::class, [
                'label' => 'clinic.label.budget.agreement',
                'required' => false,
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:Agreement',
                'choice_label' => function ($choice) {
                    return  $choice->getRazao() . " - ". $choice->getFantasia();
                },
                'query_builder' => function (EntityRepository $er) use  ($options) {
                    $clinic = $options['clinic_class'];
                    $qb = $er->createQueryBuilder('u');
                    $qb->distinct()
                        ->join('\BaseBundle\Entity\Clinic', 'c', \Doctrine\ORM\Query\Expr\Join::WITH, 'c = u.clinic')
                    ;

                    $qb->where($qb->expr()->andX(
                        $qb->expr()->eq('c.id', ':clinic')
                    ))

                        ->setParameters(
                            [
                                'clinic' => $clinic instanceof Clinic ? $clinic->getId() :  $clinic,
                            ]
                        )
                        ->orderBy('u.razao', 'ASC') ;

                    return $qb;
                },
            ])

            ->add('plan', PlanFormType::class, [
                'clinic_class' => $options['clinic_class']
            ])

        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                'clinic_class' => false,
                'data_class' => BudgetModel::class
            ]);
    }

    /**
     * Returns the prefix of the template block name for this type.
     *
     * The block prefix defaults to the underscored short class name with
     * the "Type" suffix removed (e.g. "UserProfileType" => "user_profile").
     *
     * @return string The prefix of the template block name
     */
    public function getBlockPrefix()
    {
        return "budget_form";
    }
}
