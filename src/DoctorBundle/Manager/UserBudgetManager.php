<?php


namespace DoctorBundle\Manager;

use BaseBundle\Entity\Budget;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class UserBudgetManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, Budget::class);
    }
}
