<?php


namespace DoctorBundle\Manager;

use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;

class HealtPlanManager extends AbstractManager
{
    public function __construct(ObjectManager $manager)
    {
        parent::__construct($manager, Agreement::class);
    }
}
