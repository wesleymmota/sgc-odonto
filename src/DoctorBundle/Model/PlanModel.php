<?php

namespace DoctorBundle\Model;

use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Model\ModelBase;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\UserClinical;
use DMS\Filter\Rules as Filter;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\Reflection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * PlanModel
 */
class PlanModel extends ModelBase
{

    public function __construct(Plan $budget = null)
    {

        if (null != $budget) {
            (new Reflection())->hydrate($budget->toArray(), $this);
        }


        $this->procedures = new ArrayCollection();
    }

    public function getPersist()
    {
//        foreach (get_object_vars($this) as $key => $val) {
//            if (! is_object($val) && ! is_null($val)) {
//                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
//
//
//                if (method_exists($this->getUser(), $setMethod)) {
//                    if ($setMethod == "setRoles") {
//                        continue;
//                    }
//                    $this->getUser()->$setMethod($val);
//                }
//            }
//        }
//
        if ( $this->getProfessional() instanceof UserClinical){
            $this->professional =  $this->getProfessional()->getUser();
        }
//        $this->plan->setProfessional( $this->getProfessional() );
//        $this->plan->addProcedures( $this->procedures );
//
//        $this->setTotal();

        return $this;
    }

    protected $procedures;


    protected $professional;

    protected $details;


}
