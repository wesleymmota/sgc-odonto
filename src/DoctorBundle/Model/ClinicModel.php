<?php

namespace DoctorBundle\Model;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Model\ModelBase;
use DMS\Filter\Rules as Filter;
use Zend\Hydrator\Reflection;
use AdminBundle\Validator\Constraints as BFOSBrasilAssert;

/**
 * Clinica
 */
class ClinicModel extends ModelBase
{

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $fantasia;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $razao;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $ie;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     *
     * @BFOSBrasilAssert\CpfCnpj(
     *     cpf=false,
     *     cnpj=true,
     *     mask=true
     *     )
     * @var string
     */
    protected $cnpj;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $address;

    /**
     * @var string Bairro
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $neighborhood;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $zip_code;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $number;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $phone;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $cell_phone;

    /**
     * @var string
     *
     * @Filter\StripTags()
     * @Filter\Trim()
     * @Filter\StripNewlines()
     */
    protected $city;


    /**
     * @var \BaseBundle\Entity\ClinicalSettings
     *
     */
    protected $settings;


    protected $clinic;

    protected $id;

    public function __construct(Clinic $clinic = null)
    {
        $this->clinic = $clinic;

        if (null != $clinic) {
            (new Reflection())->hydrate($clinic->toArray(), $this);
        }
    }

    public function getPersistClinic()
    {
        foreach (get_object_vars($this) as $key => $val) {
            if (! is_object($val) && ! is_null($val)) {
                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));


                if (method_exists($this->getClinic(), $setMethod)) {
                    $this->getClinic()->$setMethod($val);
                }

            }

            $this->getClinic()->setCity($this->getCity());
            $this->getClinic()->setSettings($this->getSettings());
        }


        return $this->getClinic();
    }
}
