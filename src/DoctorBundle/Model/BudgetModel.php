<?php

namespace DoctorBundle\Model;

use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Model\ModelBase;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\UserClinical;
use DMS\Filter\Rules as Filter;
use Doctrine\Common\Collections\ArrayCollection;
use Zend\Hydrator\Reflection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * BudgetModel
 */
class BudgetModel extends ModelBase
{

    public function __construct(Budget $budget = null, Plan $plan = null)
    {
        if (null != $plan) {
            $this->plan = $plan;


        }else{
            $this->plan = new Plan();
        }

        if (null != $budget) {
            $this->budget = $budget;

            (new Reflection())->hydrate($budget->toArray(), $this);
        }else{
            $this->budget = new Budget();
        }


        //$this->procedures = new ArrayCollection();
    }

    public function getPersist()
    {
//        foreach (get_object_vars($this) as $key => $val) {
//            if (! is_object($val) && ! is_null($val)) {
//                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));
//
//
//                if (method_exists($this->getUser(), $setMethod)) {
//                    if ($setMethod == "setRoles") {
//                        continue;
//                    }
//                    $this->getUser()->$setMethod($val);
//                }
//            }
//        }
//
//        if ( $this->getProfessional() instanceof UserClinical){
//            $this->professional =  $this->getProfessional()->getUser();
//        }
//        $this->plan->setProfessional( $this->getProfessional() );
        //$this->plan->addProcedures( $this->procedures );
        ///$this->getPersist();
        $this->setTotal();
        foreach (get_object_vars($this) as $key => $val) {

            if ( ! is_null($val)) {
                $setMethod = 'set' . str_replace(' ', '', ucwords(str_replace('_', ' ', $key)));


                if (method_exists($this->budget, $setMethod)) {
                    if ($setMethod == "setPlan") {
                        $procedures = $val->getProcedures();

                        foreach ($procedures as $procedure) {
                            $procedure->setPlan($val);
                        }

                    }
                    $this->getBudget()->$setMethod($val);
                }
            }


        }
        //dump($this->getBudget());die();

        return $this->getBudget();
    }
//    public function addProcedure(Procedure $tag)
//    {
//        $this->plan->procedures->add($tag);
//    }

    protected $plan;
    protected $budget;
//    protected $procedures;
//
    /**
     * @Assert\NotBlank()
     */
    protected $payment;

    /**
     * @Assert\GreaterThan("today",message="A validade deve ser maior que o dia de hoje!")
     * @Assert\NotBlank()
     */
    protected $validity;

    /**
     * @Assert\NotBlank()
     */
    protected $tooth;

    /**
     * @Assert\NotBlank()
     */
    protected $value;

    protected $discount;

    protected $agreement;

    protected $total;

    /**
     * @return mixed
     */
    public function getTotal()
    {
        return $this->total;
    }


    /**
     * @param string $total
     * @return Budget
     *
     */
    public function setTotal()
    {

        $percentual = $this->discount/ 100.0; // pega a porcentagem 0,15

        $this->total = $this->value - ( $percentual * $this->value);

        return $this;
    }

    /**
     * @return Budget
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param Budget $budget
     * @return BudgetModel
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
        return $this;
    }


}
