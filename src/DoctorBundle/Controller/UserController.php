<?php

namespace DoctorBundle\Controller;

use AdminBundle\Form\ChangePhotoType;
use AdminBundle\Manager\ClinicManager;
use AdminBundle\Manager\UserManager;
use AdminBundle\Model\UserModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Assets;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\User;
use BaseBundle\Manager\SystemManager;
use DoctorBundle\Form\AdminFormType;
use DoctorBundle\Form\ChangePasswordFormType;
use DoctorBundle\Form\ClientFormType;
use FOS\UserBundle\Event\FormEvent;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/clinical/user")
 */
class UserController extends BaseController
{


    /**
     * @Route("/{id}/info", name="doctor_clinic_user_profile_info")
     * @Template("doctor/clinic/user/user-info.html.twig")
     * @ParamConverter("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
      */
    public function info(Request $request, UserManager $userManager, User $user)
    {
        $oldParams = $this->getPostRoute($request);
        $routeName = "doctor_pacientes_list";
        $breadcumbName = 'breadcumb.clinical.user_list';
        if (!empty($oldParams)) {

            $routeName = $oldParams['route'];
            $breadcumbName = 'breadcumb.clinical.paciente_list';
        }

        return  [
            'breadcumbs' => [
                'name' => $breadcumbName,
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => '',
                    ]

                ]
            ],
            'user' => $user
        ];
    }

    /**
     * @Route("/{id}/user/state",
     *     name="doctor_clinic_user_profile_change_state",
     *       options = { "expose" = true },
     * )
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     *
     */
    public function changeState(Request $request, User $user, UserManager $userManager)
    {
        if (null === $user) {
            return $this->redirectToRoute('doctor_client_list');
        }
        $user->setEnabled(!$user->isEnabled());
        $saved = $userManager->merge($user);

        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        } else {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        }


        return new JsonResponse(array('state' => true));
    }

    /**
     * @Route("/{id}/edit", name="doctor_clinic_user_profile_edit")
     * @Template("doctor/clinic/user/form/user-edit.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function edit(Request $request, User $user, UserManager $userManager, ClinicManager $manager)
    {
        $profile = new UserModel($user);
        $session  = $request->getSession();
        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_profile_edit', ['id' => $user->getUuid()]),
            'method' => 'POST',
            'validation_groups' => [
                'update_profile'
            ],
            ];


        $clinicId = $session->get('app.clinic_id');

        $routeNew = "breadcumb.clinical.edit_client";
        $userClinical = $manager->getRepository()->getUserClinic($user, $clinicId);

        if($userClinical->isEmployee()){
            $profile->setProfile(User::ROLE_EMPLOYEE);
        }

        if($userClinical->isOwner()){
            $profile->setProfile(User::ROLE_ADMIN);
        }

        if ($userClinical->isEmployee() || $userClinical->isOwner()){
            $routeNew = "breadcumb.clinical.edit_user";
            $form = $this->createForm(AdminFormType::class, $profile, $options);
         }else{

            $form = $this->createForm(ClientFormType::class, $profile, $options);
        }

        if ($this->getUser()->hasRole(User::ROLE_EMPLOYEE)){
            $options['exclude_roles'] = FALSE;
            $form = $this->createForm(AdminFormType::class, $profile, $options);
        }


        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $profile = $form->getData();

            if ($profile->getProfile() == User::ROLE_ADMIN){
                $userClinical->setEmployee(FALSE);
                $userClinical->setOwner(TRUE);
                $saved = $userManager->merge($userClinical);
            }

            if ($profile->getProfile() == User::ROLE_EMPLOYEE){
                $userClinical->setEmployee(TRUE);
                $userClinical->setOwner(FALSE);
                $saved = $userManager->merge($userClinical);
            }

            $saved = $userManager->merge($profile->getPersistUser());

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                if ($this->getUser()->getId() === $user->getId()) {
                    $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                }
                return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "doctor_pacientes_list";
        $breadcumbName = 'breadcumb.clinical.user_list';
        if (!empty($oldParams)) {

            $routeName = $oldParams['route'];
            $breadcumbName = 'breadcumb.clinical.paciente_list';
        }



        return  [
            'breadcumbs' => [
                'name' => $breadcumbName,
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => $routeNew,
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user,
            'title' => $routeNew
        ];
    }


    /**
     * @Route("/add/{type}", name="doctor_clinic_user_profile_add_client")
     * @Template("doctor/clinic/user/form/user-new.html.twig")
     */
    public function newAction(Request $request, $type = "client", UserManager $userManager, ClinicManager $manager,UserPasswordEncoderInterface $passwordEncoder)
    {
        $profile = new UserModel();
        $session  = $request->getSession();
        $options = [

            'action' => $this->generateUrl('doctor_clinic_user_profile_add_client', ['type' => $type]),
            'attr' => array(
                'id' => 'user-form',
            ),
            'method' => 'POST',
            'validation_groups' => [
                'new_patient'
            ],
        ];


        $clinicId = $session->get(SystemManager::APP_CLINIC_ID);

        if ($type =="client") {
            $options['include_client'] = true;
            $form = $this->createForm(ClientFormType::class, $profile, $options);
        } else {
            if ($this->getUser()->hasRole(User::ROLE_EMPLOYEE)){
                $options['exclude_roles'] = FALSE;
            }

            $form = $this->createForm(AdminFormType::class, $profile, $options);
        }


        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_client_list');
        }


        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $profile = $form->getData();



            $user = $profile->getPersistUser();
            if(NULL === $user->getPassword()) {
                $user->setPassword($passwordEncoder->encodePassword(
                    $user,
                    User::USER_DEFAULT_PASSWORD
                ));
            }
            $saved = false;
            if($type == "client") {
                $user->addRole("ROLE_USER_AREA");
            }else{
                $user->addRole("ROLE_DOCTOR_AREA");
            }

            $clinc = $userManager->getRepository(Clinic::class)->findOneById($clinicId);
            $hasUserInClinical = $manager->getRepository()->getUserClinicByUserOrEmail($user, $clinc);

            if ($hasUserInClinical) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.alreadey_exists');
                return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $hasUserInClinical->getUser()->getUuid()]);
            }

            $hasUser = $userManager->getRepository()->getUserByEmail($user->getEmail());

            if ($hasUser) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.exists_user_in_anoter_clinic');

                $userManager->createClientToClinic($hasUser, $clinc);

                return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $hasUser->getUuid()]);
            }


            $saved = null;
            if ($profile->getProfile() == null or $profile->getProfile() == User::ROLE_CLIENT) {

                $saved = $userManager->createClientToClinic($user, $clinc);
            }

            if ($profile->getProfile() == User::ROLE_EMPLOYEE) {

                $saved = $userManager->createEmployeeToClinic($user, $clinc);
            }


            if ($profile->getProfile() == User::ROLE_ADMIN) {

                $saved = $userManager->createOwnerToClinic($user, $clinc);
            }

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                if ($this->getUser()->getId() === $user->getId()) {
                    $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                }
                return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $saved->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $oldParams = $this->getPostRoute($request);
        $routeName = "doctor_pacientes_list";
        $breadcumbName = 'breadcumb.clinical.user_list';
        $routeNew = "breadcumb.clinical.new_user";
        if (!empty($oldParams)) {

            $routeName = $oldParams['route'];
            $breadcumbName = 'breadcumb.clinical.paciente_list';
            $routeNew = "breadcumb.clinical.new_client";
        }

        return  [
            'breadcumbs' => [
                'name' => $breadcumbName ,
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => $routeNew,
                        'route' => '',
                    ]
                ]
            ],
            'title' => $routeNew,
            'form' =>  $form->createView(),
        ];
    }



    /**
     * @Route("/{id}/change-photo", name="doctor_clinic_user_profile_change_photo")
     * @Template("doctor/clinic/user/change_photo.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function changePhoto(Request $request, User $user, UserManager $userManager)
    {
        $profile_upload_directory = "profiles/" . $user->getId();

        $asset = $user->getPhoto();
        if (null === $user->getPhoto()) {
            $asset = new Assets();
            $user->setPhoto($asset);
        }
        $asset->setTargetDir($profile_upload_directory);

        $form = $this->createForm(ChangePhotoType::class, $asset);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]);
        }


        if ($form->isSubmitted() && $form->isValid()) {

            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $assetFile = $asset->upload('BaseBundle::User');
            $storage = $this->get('base.google_storage');

            $image100x100= $profile_upload_directory . "/100x100-" . basename($assetFile);
            Image::open($assetFile)
                ->cropResize(200, 200)
                ->zoomCrop(250, 250)
                ->save($image100x100);

            $options = [
                'resumable' => true,
                'name' => $image100x100,
                'metadata' => [
                    'user_id' => $user->getId()
                ]
            ];

            if ($user->getPhoto()->getUrl() !== null) {
                $storage->deleteFile($user->getPhoto()->getUrl());
            }


            $fileName = $storage->uploadFile($image100x100, $options);

            $asset->setUrl($fileName);
            $asset->setUpdatedAt(new \DateTime());

            $user->setPhoto($asset);

            $userManager->merge($user);
            if ($this->getUser()->getId() === $user->getId()) {
                $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            }
            return $this->redirect($this->generateUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]));
        }

        $routeName = "doctor_client_list";

        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => 'breadcumb.clinical.user_change_photo',
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }


    /**
     * @Route("/{id}/change-password", name="doctor_clinic_user_profile_change_password")
     * @Template("doctor/clinic/user/change_password.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function changePassword(Request $request, User $user, UserManager $userManager, UserPasswordEncoderInterface $passwordEncoder)
    {

        $form = $this->createForm(ChangePasswordFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userForm = $form->getData();


            $userForm->setPassword($passwordEncoder->encodePassword(
                $userForm,
                $userForm->getPlainPassword()
              ));
            $userForm->setPasswordRequestedAt(null);
            $userForm->setEnabled(TRUE);

            $saved = $userManager->merge($userForm);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                if ($this->getUser()->getId() === $user->getId()) {
                    $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                }
                return $this->redirectToRoute('doctor_clinic_user_profile_info', ['id' => $saved->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $oldParams = $this->getPostRoute($request);
        $routeName = "doctor_pacientes_list";
        $breadcumbName = 'breadcumb.clinical.user_list';
        if (!empty($oldParams)) {

            $routeName = $oldParams['route'];
            $breadcumbName = 'breadcumb.clinical.paciente_list';
        }

        return  [
            'breadcumbs' => [
                'name' => $breadcumbName,
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),
                    ],

                    'item2' => [
                        'name' => 'profile.quick_actions.change_password',
                        'route' => '',
                    ]
                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }

}
