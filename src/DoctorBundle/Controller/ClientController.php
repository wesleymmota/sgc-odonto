<?php

namespace DoctorBundle\Controller;

use AdminBundle\Manager\ClinicManager;
use AdminBundle\Manager\UserManager;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Clinic;
use BaseBundle\Manager\SystemManager;
use Doctrine\ORM\EntityRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/clinical")
 */
class ClientController extends BaseController
{


    /**
     * @Route("/", name="doctor_client_list")
     * @Template("doctor/client/index.html.twig")
     */
    public function clients(Request $request, ClinicManager $manager)
    {
        $session  = $request->getSession();
        $user  = $this->getUser();
        $clinicId = $session->get(SystemManager::APP_CLINIC_ID);

        $clinic = $manager->getRepository()->fetchOneById($clinicId);

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "roles"),
            'sortDirection'=> $request->query->get('sortDirection', "DESC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
             //   [ 'id' => 'uuid', 'name' => 'form.id' , 'sorted' => 'true', 'style' => 'width: 80px;'],
                [ 'id' => 'name', 'name' => 'form.name' , 'sorted' => 'true', 'style' => 'width: 250px;', 'card' => true],
                ['id'  => 'roles', 'name' => 'form.type' , 'sorted' => 'true', 'style' => 'width: 130px;',  'array' => true],
                [ 'id' => 'phone', 'name' => 'form.phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                ['id'  => 'cell_phone', 'name' => 'form.cell_phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'enabled', 'name' => 'form.status', 'sorted' => 'true' , 'bool'=> true, 'style' => 'width: 120px;'],
            ]
        ];
        $oldParams = $this->getPostRoute($request);
        if (!empty($oldParams)) {
            $filter = array_merge($filter, $oldParams['params']);
        }

        $user = $manager->getRepository()->getUserClinic($this->getUser(), $clinic);


        if (null !== $user && $user->isEmployee()) {

            $filter['employee'] = true;
        }
        $filter['paciente'] = false;
        $build = $manager->getRepository()->getClients($clinic, $filter, false);
        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);


        $data = array_merge([ 'data' => $pagination,'clinic' => $clinic ], $filter);

        $routeName = "doctor_client_list";
        $this->saveReturnPage($request, $routeName);

        $breadcumbs = [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  null,
                'items' => [
                ]
            ],
            '_route' => $routeName
        ];

        $data = array_merge($breadcumbs, $data);
        return  $data;
    }


    /**
     * @Route("/pacientes", name="doctor_pacientes_list")
     * @Template("doctor/client/pacientes.html.twig")
     */
    public function pacientes(Request $request, ClinicManager $manager)
    {
        $session  = $request->getSession();
        $user  = $this->getUser();
        $clinicId = $session->get(SystemManager::APP_CLINIC_ID);

        $clinic = $manager->getRepository()->fetchOneById($clinicId);

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "roles"),
            'sortDirection'=> $request->query->get('sortDirection', "DESC"),
            'page'=> $request->query->get('page', 1),
            'fields' => [
                //   [ 'id' => 'uuid', 'name' => 'form.id' , 'sorted' => 'true', 'style' => 'width: 80px;'],
                [ 'id' => 'name', 'name' => 'form.name' , 'sorted' => 'true', 'style' => 'width: 250px;', 'card' => true],
                [ 'id' => 'phone', 'name' => 'form.phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                ['id'  => 'cell_phone', 'name' => 'form.cell_phone' , 'sorted' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'enabled', 'name' => 'form.status', 'sorted' => 'true' , 'bool'=> true, 'style' => 'width: 120px;'],
            ]
        ];
        $oldParams = $this->getPostRoute($request);
        if (!empty($oldParams)) {
            $filter = array_merge($filter, $oldParams['params']);
        }

        $user = $manager->getRepository()->getUserClinic($this->getUser(), $clinic);


        if (null !== $user && $user->isEmployee()) {

            $filter['employee'] = true;
        }
        $filter['employee'] = true;
        $filter['paciente'] = false;
        $build = $manager->getRepository()->getClients($clinic, $filter, false);
        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);


        $data = array_merge([ 'data' => $pagination,'clinic' => $clinic ], $filter);

        $routeName = "doctor_pacientes_list";
        $this->saveReturnPage($request, $routeName);

        $breadcumbs = [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.paciente_list',
                'route' =>  null,
                'items' => [
                ]
            ],
            '_route' => $routeName
        ];

        $data = array_merge($breadcumbs, $data);
        return  $data;
    }


    /**
     * @Route("/add/{id}/user", name="admin_clinic_user_add")
     * @Template("admin/clinic/user/user-add.html.twig")
     * @ParamConverter(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function user_add(Request $request, Clinic $clinic, UserManager $userManager, ClinicManager $clinicManager)
    {
        $form = $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_clinic_user_add', ['id' => $clinic->getId()]))
            ->setMethod('POST')
            //->add('name', TextType::class, array('label' => 'form.label.name'))
            ->add('users', EntityType::class, [
                'label' => 'form.users',
                'attr' => [
                    'class' => 'm-select2 m-select2--air m-select2--pill'
                ],
                'class' => 'BaseBundle:User',
                'choice_label' => function ($choice) {
                    return  $choice->getName() . " - ". $choice->getEmail();
                },
                'query_builder' => function (EntityRepository $er) {
                    $qb = $er->createQueryBuilder('u');
                    //$qb->where('u.enabled = 1');


                    $qb->where($qb->expr()->orX(
                        //                        $qb->expr()->eq('u.enabled', 1),
                        //                        $qb->expr()->isNull('u.clinic'),
                        $qb->expr()->like('u.roles', ':roles1'),
                        $qb->expr()->like('u.roles', ':roles2'),
                        $qb->expr()->like('u.roles', ':roles3')
                    ))
                        ->setParameter('roles1', '%ROLE_EMPLOYEE%')
                        ->setParameter('roles3', '%a:0:{}%')
                        ->setParameter('roles2', '%ROLE_CLIENT%');

                    $qb->andWhere('u.clinic is null');
                    $qb->andWhere('u.enabled = 1');

                    return $qb->orderBy('u.id');
                },
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task = $form->getData();

            $user = $userManager->fetch($task['users']->getId());
            $user->setClinic($clinic);
            $saved = $clinicManager->save($user);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('admin_clinic_show', ['id' => $clinic->getId()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }


        $routeName = "admin_clinic";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'information' => [
                        'name' => 'form.information',
                        'route' => $this->getUrl('admin_clinic_show', ['id' => $clinic->getId()]),
                    ],
                    'information2' => [
                        'name' => 'form.clinic.user.edit',
                        'route' => null,
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }
}
