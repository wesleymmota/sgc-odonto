<?php

namespace DoctorBundle\Controller;

use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\User;
use BaseBundle\Manager\SystemManager;
use DoctorBundle\Form\BudgetFormType;
use DoctorBundle\Form\PayBudgetFormType;
use DoctorBundle\Manager\UserBudgetManager;
use DoctorBundle\Model\BudgetModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/clinical/user")
 */
class UserBudgetController extends BaseController
{

    /**
     * @Route("/budget", name="doctor_clinic_user_quote_list")
     * @Template("doctor/clinic/user/budget/admin/user-budget.html.twig")
     */
    public function budgetList(Request $request, UserBudgetManager $manager)
    {

        $session  = $request->getSession();

        $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);
        $clinic     = $manager->getRepository(Clinic::class)->findOneById($clinicId);

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "status"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'clinic' => $clinic,
            'fields' => [
                [ 'id' => 'id', 'name' => 'form.id' , 'sorted' => 'false', 'style' => 'width: 50px;','idView' => true],
                [ 'id' => 'client', 'name' => 'clinic.label.budget.client' , 'sorted' => 'true', 'style' => 'width: 200px;'],
                [ 'id' => 'value', 'name' => 'clinic.label.budget.value' , 'sorted' => 'true', 'style' => 'width: 100px;'],
                [ 'id' => 'discount', 'name' => 'clinic.label.budget.discount' , 'sorted' => 'true', 'style' => 'width: 100px;'],
                [ 'id' => 'total', 'name' => 'clinic.label.budget.total' , 'sorted' => 'true', 'style' => 'width: 100px;'],
                [ 'id' => 'validity', 'name' => 'clinic.label.budget.validity', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 100px;'],
                [ 'id' => 'paid', 'name' => 'Pagamento' , 'sorted' => 'false', 'style' => 'width: 120px;','paid' => true],
                [ 'id' => 'status', 'name' => 'clinic.label.budget.status' , 'sorted' => 'true', 'style' => 'width: 100px;','status' => true],

            ]
        ];


        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);

        $routeName = "doctor_clinic_user_quote_list";

        $breadCumb = [
            'name' => 'clinic.label.budgets',
            'route' =>  $this->getUrl($routeName),
            'items' => []
        ];
        $data = array_merge([ 'data' => $pagination,'breadcumbs' =>  $breadCumb], $filter);


        return $data;

    }


    /**
     * @Route("/{id}/budget", name="doctor_clinic_user_profile_quote")
     * @Template("doctor/clinic/user/budget/user-budget.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function info(Request $request, UserBudgetManager $manager, User $user)
    {
        $session    = $request->getSession();
        $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);
        $clinic     = $manager->getRepository(Clinic::class)->findOneById($clinicId);

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "validity"),
            'sortDirection'=> $request->query->get('sortDirection', "DESC"),
            'page'=> $request->query->get('page', 1),
            'user' => $user,
            'clinic' => $clinic,
            'fields' => [
                [ 'id' => 'id', 'name' => 'form.id' , 'sorted' => 'false', 'style' => 'width: 50px;','idView' => true],
                //[ 'id' => 'tooth', 'name' => 'clinic.label.budget.tooth' , 'sorted' => 'false', 'style' => 'width: 100px;'],
                [ 'id' => 'total', 'name' => 'clinic.label.budget.value' , 'sorted' => 'true', 'style' => 'width: 100px;'],
                [ 'id' => 'status', 'name' => 'clinic.label.budget.status' , 'sorted' => 'true', 'style' => 'width: 100px;','status' => true],
                [ 'id' => 'paid', 'name' => 'Pagamento' , 'sorted' => 'false', 'style' => 'width: 120px;','paid' => true],
                [ 'id' => 'validity', 'name' => 'clinic.label.budget.validity', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 100px;'],


            ]
        ];
//
//        $entityManager = $this->getDoctrine()->getManager();
//        $b = $entityManager->getRepository(Budget::class)->find(["id" => "f3af7c73-5592-11e8-8735-02420a0a011e"]);
//
//        $b->setDiscount(16);
//        $entityManager->persist($b);
//
//        $entityManager->flush();
//
//        dump($this->getRevisions(Budget::class,"f3af7c73-5592-11e8-8735-02420a0a011e")); die();

        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);
        $routeName = "doctor_client_list";
        $breadCumb = [
            'name' => 'breadcumb.clinical.user_list',
            'route' =>  $this->getUrl($routeName),
            'items' => [
                'item1' => [
                    'name' => 'breadcumb.clinical.user_info',
                    'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                ],
                'item2' => [
                    'name' => 'clinic.label.budgets',
                    'route' => '',
                ]

            ]
        ];
        $data = array_merge([ 'data' => $pagination,'user' => $user, 'breadcumbs' =>  $breadCumb], $filter);



        return $data;

    }

    /**
     * @Route("/{id}/budget/add", name="doctor_clinic_user_new_budet")
     * @Template("doctor/clinic/user/budget/form/user-budget-form-new.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function newAction(Request $request,  UserBudgetManager $manager, User $user)
    {
        $session    = $request->getSession();
        $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);
        $clinic     = $manager->getRepository(Clinic::class)->findOneById($clinicId);
        $model      = new BudgetModel();

        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_new_budet', ['id' => $user->getUuid()]),
            'method' => 'POST',
            'clinic_class' => $clinic
        ];
        $form = $this->createForm(BudgetFormType::class, $model, $options);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $budgetModel = $form->getData();
            /**
             * @var $budget Budget
             */
            $budget = $budgetModel->getPersist();
            $budget->setCreatedBy($this->getUser());
            $budget->setClient($user);
            $budget->setClinic($clinic);
            $budget->getPlan()->setClinic($clinic);
            $saved = $manager->save($budget);
            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');


        }
        $routeName = "doctor_client_list";
        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                    ],
                    'item2' => [
                        'name' => 'clinic.label.budgets',
                        'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                    ],
                    'item3' => [
                        'name' => 'clinic.label.budget.new',
                        'route' => '',
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/budget/{budget_id}/edit", name="doctor_clinic_user_edit_budet")
     * @Template("doctor/clinic/user/budget/form/user-budget-form-new.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("budget", options={"id" = "budget_id"})
     */
    public function edit(Request $request, UserBudgetManager $manager, User $user, Budget $budget)
    {
        $session    = $request->getSession();
        $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);

        $getBudget  = $manager->getRepository()->findOneBy(['id' => $budget->getId()]);
        $id         =  $budget->getPlan()->getId();
        $getPlan    = $manager->getRepository(Plan::class)->find(['id' => $id ]);
        $clinic     = $manager->getRepository(Clinic::class)->find(['id' => $clinicId ]);
        $model      = new BudgetModel($getBudget, $getPlan);
        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_edit_budet', ['id' => $user->getUuid(), 'budget_id' => $budget->getId()]),
            'method' => 'POST',
            'clinic_class' => $clinic
        ];

        $form = $this->createForm(BudgetFormType::class, $model, $options);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $budgetModel = $form->getData();

            /**
             * @var $budget Budget
             */
            $budget = $budgetModel->getPersist();
//            if(empty($budget->getPlan()->getClinic()) && $clinicId instanceof Clinic){
//                $budget->getPlan()->setClinic($clinicId);
//            }
//            $budget->setCreatedBy($this->getUser());
//            $budget->setClient($user);
            $saved = $manager->merge($budget);
            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');


        }
        $routeName = "doctor_client_list";
        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                    ],
                    'item2' => [
                        'name' => 'clinic.label.budgets',
                        'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                    ],
                    'item3' => [
                        'name' => 'clinic.label.budget.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/budget/{budget_id}/pay", name="doctor_clinic_user_pay_budget")
     * @Template("doctor/clinic/user/budget/form/user-budget-form-pay.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("budget", options={"id" = "budget_id"})
     */
    public function pay(Request $request, UserBudgetManager $manager, User $user, Budget $budget)
    {
        $session    = $request->getSession();


        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_pay_budget', ['id' => $user->getUuid(), 'budget_id' => $budget->getId()]),
            'method' => 'POST'
        ];

        $form = $this->createForm(PayBudgetFormType::class, $budget, $options);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
        }
        if ($form->isSubmitted() && $form->isValid()) {

            /**  @var Budget $budgetModel */
            $budgetModel = $form->getData();

            $budgetModel->setPaid(TRUE);
            $budgetModel->setPaidDate(new \DateTime());

            $saved = $manager->merge($budgetModel);
            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_user_profile_quote',['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');


        }
        $routeName = "doctor_client_list";
        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                    ],
                    'item2' => [
                        'name' => 'clinic.label.budgets',
                        'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                    ],
                    'item3' => [
                        'name' => 'clinic.label.budget.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' => $form->createView(),
            'budget' => $budget
        ];
    }
    /**
     * @Route("/{id}/state/{state}",
     *     name="doctor_clinic_user_profile_quote_change_state",
     *       options = { "expose" = true },
     * )
     * @Entity("budget", class="BaseBundle:Budget")
      *
     */
    public function changeState(Request $request, Budget $budget, $state = 0, UserBudgetManager $userBudgetManager)
    {
        if (null === $budget) {
           throw $this->createNotFoundException(
                'No budget found'
            );
        }

        $budget->setStatus($state);

        $saved = $userBudgetManager->merge($budget);

        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        } else {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        }


        return $this->redirectToRoute('doctor_clinic_user_profile_quote', ['id' => $budget->getClient()->getUuid()]);
    }

    /**
     * @Route("/{id}/done",
     *     name="doctor_clinic_user_profile_quote_done",
     *       options = { "expose" = true },
     * )
     * @Entity("budget", class="BaseBundle:Budget")
     *
     */
    public function setDone(Request $request, Budget $budget, UserBudgetManager $userBudgetManager)
    {
        if (null === $budget) {
            throw $this->createNotFoundException(
                'No budget found'
            );
        }

        $budget->setDone(TRUE);
        $budget->getPlan()->setDate(new \DateTime());

        $saved = $userBudgetManager->merge($budget);

        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        } else {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
        }
        return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $budget->getClient()->getUuid(), 'detal_record_id' => $budget->getPlan()->getId()]);
    }
}
