<?php

namespace DoctorBundle\Controller;

use AdminBundle\Form\ClinicFormType;
use AdminBundle\Manager\ClinicManager;
use AdminBundle\Model\ClinicModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Clinic;
use BaseBundle\Manager\SystemManager;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/clinical")
 */
class DefaultController extends BaseController
{

    /**
     * @Route("/info", name="doctor_clinic_show")
     * @Template("doctor/clinic/show.html.twig")
     */
    public function show(Request $request, ClinicManager $manager)
    {
        $session = $this->container->get('session');
        $clinicId = $session->get(SystemManager::APP_CLINIC_ID);

        $clinic = $manager->getRepository()->fetchOneById($clinicId);

        return  [
//            'breadcumbs' => [
//                'name' => 'breadcumb.clinic',
//                'route' =>  $this->getUrl($routeName),
//                'items' => [
//                    'information' => [
//                        'name' => 'form.information',
//                        'route' => null,
//                    ]
//
//                ]
//            ],
            'clinic' =>  $clinic,
        ];
    }

    /**
     * @Route("/index", name="doctor_area")
     * @Template("doctor/clinic/show.html.twig")
     */
    public function indexAction(ClinicManager $manager)
    {
        $routeName = "admin_clinic";
        $session = $this->container->get('session');
        $clinicId = $session->get('app.clinic_id');

        //  $all= $manager->getRepository()->getUserClinics($this->getUser());

        $clinic = $manager->fetch($clinicId);

        return  [
//            'breadcumbs' => [
//                'name' => 'breadcumb.clinic',
//                'route' =>  $this->getUrl($routeName),
//                'items' => [
//                    'information' => [
//                        'name' => 'form.information',
//                        'route' => null,
//                    ]
//
//                ]
//            ],
            'clinic' =>  $clinic,
        ];
    }


    /**
     * @Route("/{id}/edit", name="doctor_clinic_edit")
     * @Template("doctor/clinic/edit.html.twig")
     * @ParamConverter(
     *     "clinic",
     *     class="BaseBundle:Clinic",
     *     options={"id": "id"}
     * )
     */
    public function edit(Request $request, Clinic $clinic, ClinicManager $clinicManager)
    {
        $userLogged = $this->getUser();

        $profile_upload_directory = "clinic/" . $clinic->getId(). "/logo";
        $profile = new ClinicModel($clinic);

        //dump($profile->getUsername());die;
        $form = $this->createForm(ClinicFormType::class, $profile, [
            'action' => $this->generateUrl('doctor_clinic_edit', ["id" => $clinic->getId()]),
            'method' => 'POST'
        ]);

        $form->handleRequest($request);

        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_show');
        }

        if ($form->isSubmitted() && $form->isValid()) {

            $profile = $form->getData();
            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            if (null !== $profile->getSettings()->getLogo() && $profile->getSettings()->getLogo()->getImage() instanceof UploadedFile) {

                $asset = $profile->getSettings()->getLogo();

                if($asset->getImage() instanceof UploadedFile) {


                    $asset->setTargetDir($profile_upload_directory);

                    $assetFile = $asset->upload('BaseBundle::Clinic');
                    $storage = $this->get('base.google_storage');

                    $image100x100 = $profile_upload_directory . "/100x100-" . basename($assetFile);
                    Image::open($assetFile)
                        ->cropResize(200, 200)
                        ->zoomCrop(250, 250)
                        ->save($image100x100);

                    $options = [
                        'resumable' => true,
                        'name' => $image100x100,
                        'metadata' => [
                            'user_id' => $clinic->getId(),
                            'type' => 'logo'
                        ]
                    ];

                    $fileName = $storage->uploadFile($image100x100, $options);

                    $asset->setUrl($fileName);
                    $asset->setUpdatedAt(new \DateTime());

                    $profile->getPersistClinic()->getSettings()->setLogo($asset);
                }else{
                    $profile->getPersistClinic()->getSettings()->setLogo(null);
                }
            }

            $saved = $clinicManager->merge($profile->getPersistClinic());

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_show');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "doctor_clinic_show";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.clinic',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'form.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' =>  $form->createView(),
            'clinic' => $clinic
        ];
    }
}
