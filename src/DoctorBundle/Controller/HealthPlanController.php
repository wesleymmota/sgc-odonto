<?php

namespace DoctorBundle\Controller;

use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Clinic;
use BaseBundle\Manager\SystemManager;
use DoctorBundle\Form\HealtPlanFormType;
use DoctorBundle\Manager\HealtPlanManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/clinical/health_plan")
 */
class HealthPlanController extends BaseController
{
    /**
     * @Route("/", name="healt_plan_list")
     * @Template("doctor/health_plan/index.html.twig")
     */
    public function indexAction(Request $request, HealtPlanManager $manager, SystemManager $system)
    {
        $session    = $request->getSession();

        $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);

//        if ( null === $clinicId){
//            $system->checkAppSession();
//            $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);
//
//        }

        $clinic     = $manager->getRepository(Clinic::class)->findOneById($clinicId);
        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "id"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'clinic' => $clinic,
            'fields' => [
                [ 'id' => 'fantasia', 'name' => 'form.fantasia' , 'sorted' => 'true', 'style' => 'width: 150px;'],
                [ 'id' => 'razao', 'name' => 'form.razao' , 'sorted' => 'true', 'style' => 'width: 150px;'],
                [ 'id' => 'updatedAt', 'name' => 'form.updatedAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 130px;'],
                [ 'id' => 'createdAt', 'name' => 'form.createdAt', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 100px;'],

            ]
        ];

        $build = $manager->getRepository()->getQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);


        $breadCumb = [
            'name' => 'clinic.label.health_plan',
            'route' =>  '',
            'items' => [
            ]
        ];
        $data = array_merge([ 'data' => $pagination,'breadcumbs' =>  $breadCumb], $filter);

        return $data;
    }

    /**
     * @Route("/new", name="healt_plan_new")
     * @Template("doctor/health_plan/new.html.twig")
     */
    public function newAction(Request $request, HealtPlanManager $manager)
    {
        $model      = new Agreement();
        $session    = $request->getSession();

        $options = [
            'action' => $this->generateUrl('healt_plan_new'),
            'method' => 'POST'
        ];
        $form = $this->createForm(HealtPlanFormType::class, $model, $options);


        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('healt_plan_list');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $model = $form->getData();
            try {

                $clinicId   = $session->get(SystemManager::APP_CLINIC_ID);
                $clinic     = $manager->getRepository(Clinic::class)->findOneById($clinicId);

                $model->setClinic($clinic);
                $saved = $manager->save($model);

                if ($saved) {
                    $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                    return $this->redirectToRoute('healt_plan_list');
                }

            } catch(\Exception $e) {

                dump($e->getMessage());
                die();
                $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
            }
        }

        $routeName = "healt_plan_list";


        return  [
            'breadcumbs' => [
                'name' => 'clinic.label.health_plan',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'clinic.label.health_plan.new' => [
                        'name' =>  'clinic.label.health_plan.new'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/{id}/edit", name="healt_plan_edit")
     * @Entity(
     *     "agreement",
     *     class="BaseBundle:Agreement",
     *     options={"id": "id"}
     * )
     * @Template("doctor/health_plan/edit.html.twig")
     */
    public function edit(Request $request, Agreement $agreement, HealtPlanManager $manager)
    {

        $options = [
            'action' => $this->generateUrl('healt_plan_edit',['id' => $agreement->getId()]),
            'method' => 'POST'
        ];
        $form = $this->createForm(HealtPlanFormType::class, $agreement, $options);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('healt_plan_list');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $model = $form->getData();
            try {

                $saved = $manager->save($model);

                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
                return $this->redirectToRoute('healt_plan_list');

            } catch(\Exception $e) {

                $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
            }

        }

        $routeName = "healt_plan_list";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.specialization',
                'route' => $this->getUrl($routeName),
                'items' => [
                    'specialization.new' => [
                        'name' =>  'header.specialization.new'
                    ]

                ]
            ],
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/delete/{id}", name="healt_plan_delete")
     * @Entity(
     *     "agreement",
     *     class="BaseBundle:Agreement",
     *     options={"id": "id"}
     * )
     */
    public function delete(Request $request, Agreement $agreement, HealtPlanManager $manager)
    {

        $getBudgets = $manager->getRepository()->getBudgetByAgreement($agreement);
        if (sizeof($getBudgets) > 0) {
            $request->getSession()->getFlashBag()->add('wanning', 'register.removed.if.success');
            return $this->redirectToRoute('healt_plan_list');
        }
        if ($agreement instanceof Agreement) {
            $saved = $manager->delete($agreement);

            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.removed.if.success');
                return $this->redirectToRoute('healt_plan_list');
            }
        }


        $request->getSession()->getFlashBag()->add('danger', 'register.removed.if.notsuccess');
        return $this->redirectToRoute('healt_plan_list');
    }
}
