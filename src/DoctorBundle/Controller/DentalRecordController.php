<?php

namespace DoctorBundle\Controller;

use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\PlanFile;
use BaseBundle\Entity\PlanProcedure;
use BaseBundle\Entity\Procedure;
use BaseBundle\Entity\User;
use DoctorBundle\Form\PlanFormType;
use DoctorBundle\Form\PlanUploadFormType;
use DoctorBundle\Manager\UserBudgetManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/clinical/user")
 */
class DentalRecordController extends BaseController
{

    /**
     * @Route("/{id}/dental-record", name="doctor_clinic_user_dental_record")
     * @Template("doctor/clinic/user/dental-record/user-dental-record.html.twig")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     */
    public function info(Request $request, UserBudgetManager $manager, User $user)
    {
        $session  = $request->getSession();
        $clinicId = $session->get('app.clinic_id');

        $filter = [
            'filter' => $request->query->get('filter', ""),
            'sortField'=> $request->query->get('sortField', "id"),
            'sortDirection'=> $request->query->get('sortDirection', "ASC"),
            'page'=> $request->query->get('page', 1),
            'user' => $user,
            'clinic' => $clinicId,
            'fields' => [
                //[ 'id' => 'tooth', 'name' => 'clinic.label.budget.tooth' , 'sorted' => 'false', 'style' => 'width: 100px;'],
                [ 'id' => 'details', 'name' => 'clinic.label.budget.plan.details' , 'sorted' => 'false', 'style' => 'width: 200px;'],
                [ 'id' => 'professional', 'name' => 'clinic.label.budget.profissional' , 'sorted' => 'false', 'style' => 'width: 200px;'],
                [ 'id' => 'agreement', 'name' => 'clinic.label.budget.agreement' , 'sorted' => 'false', 'style' => 'width: 200px;'],
                [ 'id' => 'status', 'name' => 'form.status', 'sorted' => 'false' , 'bool'=> true, 'style' => 'width: 200px;'],
//                [ 'id' => 'date', 'name' => 'clinic.label.budget.plan.procedures.data', 'sorted' => 'true', 'datetime' => 'true', 'style' => 'width: 200px;'],
//
            ]
        ];

        $build = $manager->getRepository()->getDentalRecordQueryBuilder($filter, false);

        $pagination  = $this->get('base.pagination_factory')->createSimpleCollection($build, $request);
        $routeName = "doctor_client_list";


        $breadCumb = [
            'name' => 'breadcumb.clinical.user_list',
            'route' =>  $this->getUrl($routeName),
            'items' => [
                'item1' => [
                    'name' => 'breadcumb.clinical.user_info',
                    'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                ],
                'item2' => [
                    'name' => 'clinic.label.budgets',
                    'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                ],
                'item3' => [
                    'name' => 'breadcumb.dental_record',
                    'route' =>  $this->getUrl('doctor_clinic_user_dental_record',['id' => $user->getUuid()]),
                ]

            ]
         ];
        $data = array_merge([ 'data' => $pagination,'user' => $user, 'breadcumbs' =>  $breadCumb], $filter);



        return $data;

    }



    /**
     * @Route("/{id}/dental-record/{detal_record_id}/edit", name="doctor_clinic_user_dental_record_edit")
     * @Template("doctor/clinic/user/dental-record/form/user-budget-form-new.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("plan", class="BaseBundle:Plan", options={"id" = "detal_record_id"})
     */
    public function edit(Request $request, UserBudgetManager $manager, User $user, Plan $plan)
    {

        $session  = $request->getSession();
        $clinicId = $session->get('app.clinic_id');

        $clinic = $manager->getRepository(Clinic::class)->find(['id' => $clinicId ]);
        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_dental_record_edit', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]),
            'method' => 'POST',
            'clinic_class' => $clinic,
            'dental_record_edit' => true
        ];

        $form = $this->createForm(PlanFormType::class, $plan, $options);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('doctor_clinic_user_dental_record', ['id' => $user->getUuid() ]);
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $planModel = $form->getData();

            $procedures = $planModel->getProcedures();
            foreach ($procedures as $procedure) {
                $procedure->setPlan($planModel);
            }
            $saved = $manager->merge($planModel);
            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_user_dental_record',['id' => $user->getUuid()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');


        }
        $routeName = "doctor_client_list";
        return  [
            'breadcumbs' => [

                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                    ],
                    'item2' => [
                        'name' => 'clinic.label.budgets',
                        'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                    ],
                    'item3' => [
                        'name' => 'breadcumb.dental_record',
                        'route' =>  $this->getUrl('doctor_clinic_user_dental_record',['id' => $user->getUuid()]),
                    ],
                    'item4' => [
                        'name' => 'header.dental_record.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' => $form->createView()
        ];

    }

    /**
     * @Route("/{id}/dental-record/{detal_record_id}/show", name="doctor_clinic_user_dental_record_show")
     * @Template("doctor/clinic/user/dental-record/user-dental-record-view.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("plan", class="BaseBundle:Plan", options={"id" = "detal_record_id"})
     */
    public function show(Request $request, UserBudgetManager $manager, User $user, Plan $plan)
    {

        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_dental_record_upload', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]),
            'method' => 'POST'
        ];

        $form = $this->createForm(PlanUploadFormType::class, new PlanFile(), $options);

        $routeName = "doctor_client_list";
        return  [
            'breadcumbs' => [

                'name' => 'breadcumb.clinical.user_list',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'breadcumb.clinical.user_info',
                        'route' => $this->getUrl('doctor_clinic_user_profile_info', ['id' => $user->getUuid()]),

                    ],
                    'item2' => [
                        'name' => 'clinic.label.budgets',
                        'route' => $this->getUrl('doctor_clinic_user_profile_quote', ['id' => $user->getUuid()]),
                    ],
                    'item3' => [
                        'name' => 'breadcumb.dental_record',
                        'route' =>  $this->getUrl('doctor_clinic_user_dental_record',['id' => $user->getUuid()]),
                    ],
                    'item4' => [
                        'name' => 'clinical.user.tab.datail',
                        'route' => '',
                    ]

                ]

            ],
            'user' => $user,
            'plan' => $plan,
            'form'   => $form->createView(),
        ];

    }


    /**
     * @Route("/{id}/dental-record/{detal_record_id}/upload", name="doctor_clinic_user_dental_record_upload")
     * @Template("doctor/clinic/user/dental-record/form/user-budget-form-new.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("plan", class="BaseBundle:Plan", options={"id" = "detal_record_id"})
     */
    public function upload(Request $request, UserBudgetManager $manager, User $user, Plan $plan)
    {
        $options = [
            'action' => $this->generateUrl('doctor_clinic_user_dental_record_upload', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]),
            'method' => 'POST'
        ];
        $form = $this->createForm(PlanUploadFormType::class, new PlanFile(), $options);

        $form->handleRequest($request);
//        if ($form->get('cancel')->isClicked()) {
//            return $this->redirectToRoute('doctor_clinic_user_dental_record', ['id' => $user->getUuid() ]);
//        }
        if ($form->isSubmitted() && $form->isValid()) {
            $planModel = $form->getData();
            $assetFile = $planModel->upload($plan);

            $storage = $this->get('base.google_storage');

            $image100x100= $assetFile->getUploadDir() . "/" . $assetFile->getName();

            $options = [
                'resumable' => true,
                'name' => $image100x100,
                'metadata' => [
                    'user_id' => $user->getId(),
                    'dental_record_id' => $plan->getId(),
                    'name' => $assetFile->getName()
                ]
            ];

            $exists =  $plan->getFiles()->exists(function($key, $element) use ($assetFile){
                    return $element->getName() === $assetFile->getName();
                }
            );
            if(! $exists) {

                $fileNameUrl = $storage->uploadFile($assetFile->getPath(), $options);
                if (is_dir(dirname($assetFile->getPath()))) {
                    foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(dirname($assetFile->getPath()))) as $filename) {
                        if ($filename->isDir()) {
                            continue;
                        }
                        unlink($filename);
                    }
                    (new Filesystem)->remove(dirname(dirname($assetFile->getPath())));
                }

                $assetFile->setUrl($fileNameUrl);
                $assetFile->setPath($fileNameUrl);
                $plan->getFiles()->add($assetFile);

                $exists = $manager->merge($plan);
            }


            if ($exists) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

                return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');


        }

        return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);
    }

    /**
     * @Route("/{id}/dental-record/{detal_record_id}/delete-file/{file_id}", name="doctor_clinic_user_dental_record_delete_file")
     * @Template("doctor/clinic/user/dental-record/user-dental-record-view.html.twig")
     *
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("plan", class="BaseBundle:Plan", options={"id" = "detal_record_id"})
     * @Entity("file", class="BaseBundle:PlanFile", options={"id" = "file_id"})
     */
    public function delete_file(Request $request, UserBudgetManager $manager, User $user, Plan $plan, PlanFile $file)
    {

        $storage = $this->get('base.google_storage');
        $storage->deleteFile($file->getUrl());

        $plan->getFiles()->removeElement($file);

        $saved = $manager->merge($plan);



        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

            return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);
        }

        $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');

        return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);

    }


    /**
     * @Route("/{id}/dental-record/{detal_record_id}/approve_procedure/{procedure_id}", name="doctor_clinic_user_dental_record_approve_procedure")
     * @Entity("user", class="BaseBundle:User", options={
     *    "repository_method" = "findByUuid",
     *    "mapping": {"id": "uuid"},
     *    "map_method_signature" = true
     * })
     * @Entity("plan", class="BaseBundle:Plan", options={"id" = "detal_record_id"})
     * @Entity("procedure", class="BaseBundle:Procedure", options={"id" = "procedure_id"})
     */
    public function approve_procedure(Request $request, UserBudgetManager $manager, User $user, Plan $plan, Procedure $procedure)
    {

        $procedure_approve = $manager->getRepository()->getProcedureFromPlan($plan, $procedure);

        $procedure_approve->setStatus(PlanProcedure::STATUS_DONE);
        $procedure_approve->setDate(new \DateTime());


        $saved = $manager->merge($procedure_approve);



        if ($saved) {
            $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');

            return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);
        }

        $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');

        return $this->redirectToRoute('doctor_clinic_user_dental_record_show', ['id' => $user->getUuid(), 'detal_record_id' => $plan->getId()]);

    }



}
