<?php

namespace BaseBundle\DataFixtures\Processor;

use BaseBundle\Entity\User;
use Fidry\AliceDataFixtures\ProcessorInterface;

class UserProcessor implements ProcessorInterface
{
    protected $encoder;

    public function __construct($encoder)
    {
        $this->encoder = $encoder;
    }

    public function preProcess(string $id, $object): void
    {
        if (!$object instanceof User) {
            return;
        }

        $password = $this->encoder->encodePassword($object, $object->getPassword());
        $object->setPassword($password);
    }

    public function postProcess(string $id, $object): void
    {
    }
}
