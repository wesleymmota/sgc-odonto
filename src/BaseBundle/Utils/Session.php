<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 12/01/18
 * Time: 13:40
 */

namespace BaseBundle\Utils;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Entity\User;
use Psr\Container\ContainerInterface;

class Session
{

    /**
     * Load information from login screen
     * @param $container ContainerInterface
     * @param $settings ClinicalSettings
     */
    public static function loadLoginScreen(ContainerInterface $container, ClinicalSettings $settings = null)
    {

        $session = $container->get('session');
        $requestStack= $container->get('request_stack');
        $request = $requestStack->getCurrentRequest();

        $sessionSiteName = (!is_null($settings) && $settings->getName()) ? $settings->getName() : $container->getParameter('app.site_name');
        $sessionSiteSettingsID = (!is_null($settings) && $settings->getId()) ? $settings->getId() : NULL;
        $sessionSiteSClinic = (!is_null($settings) && $settings->getClinic()) ? $settings->getClinic() : NULL;
        $sessionSiteSClinicID = (!is_null($settings) && $settings->getClinic()->getId()) ? $settings->getClinic()->getId() : NULL;
        $sessionSiteLogo = (!is_null($settings) && !is_null($settings->getLogo()) && $settings->getLogo()->getUrl()) ? $settings->getLogo()->getUrl() : $container->getParameter('app.site_logo');
        $sessionSiteLogoFilename = (!is_null($settings) && !is_null($settings->getLogo()) && $settings->getLogo()->getFilename()) ? $settings->getLogo()->getFilename() : $container->getParameter('app.site_logo_filename');
        $sessionSiteLocale = (!is_null($settings) && $settings->getLocale()) ? $settings->getLocale() : $container->getParameter('app.locale');

        // Se o hostname for conhecido carrega as informacoes personalizadas
        $session->set('app.site_name', $sessionSiteName);
        $session->set('app.settings_id', $sessionSiteSettingsID);
        $session->set('app.clinic_id', $sessionSiteSClinicID);
        $session->set('app.clinic', $sessionSiteSClinic);
        $session->set('app.site_logo', $sessionSiteLogo);
        $session->set('app.site_logo_filename', $sessionSiteLogoFilename);
        $session->set('app.locale', $sessionSiteLocale);

        if ($container->hasParameter('menu.asside')) {
            $menuItems = $container->getParameter('menu.asside');

            $session->set('app.menu_asside', $menuItems['items']);
        }

        // Garante  a rota padrão
//        if ($container->get('security.authorization_checker')->isGranted('ROLE_ADMIN_AREA')) {
//            die("09");
//            $defaultRoute = "admin_area";
//            $session->set('app.default_route', $defaultRoute);
//        } elseif ($container->get('security.authorization_checker')->isGranted('ROLE_DOCTOR_AREA')) {
//            $defaultRoute = "doctor_client_list";
//            $session->set('app.default_route', $defaultRoute);
//        } elseif ($container->get('security.authorization_checker')->isGranted('ROLE_USER_AREA')) {
//            $defaultRoute = "client_area";
//            $session->set('app.default_route', $defaultRoute);
//        }


        $request->setLocale($sessionSiteLocale);
        $request->setDefaultLocale($sessionSiteLocale);
        $request->getSession()->set('_locale', $sessionSiteLocale);


    }

    /**
     * @param $container ContainerInterface
     * @param $user User
     * @param $settings ClinicalSettings
     */
    public static function loadSessionSettings(ContainerInterface $container, User $user = null, ClinicalSettings $settings = null)
    {
        $session = $container->get('session');
        $userClinic = $container->get('doctrine')->getRepository(Clinic::class)->findClinicLastAccess($user);

        $clinic = null;
        if (!is_null($userClinic) && !empty($userClinic->getClinic())) {
            $clinic = $userClinic->getClinic();
        }

        if (is_null($settings) && !empty($clinic) && !empty($clinic->getSettings())) {
            $settings = $clinic->getSettings();
        }

        if (!$session->has('app.clinic_id')) {

           self::loadLoginScreen($container, $settings);
        }

        if (! is_null($clinic) ) {

            $findClinics = $container->get('doctrine')->getRepository(Clinic::class)->getUserClinics($user);

            $clinics = [];
            $inSession = null;
            foreach ($findClinics as $userClinicLoop) {
                $clinicLoop = $userClinicLoop->getClinic();
                if ($clinicLoop->isEnabled()) {
                    $clinics[$clinicLoop->getId()] = [
                        'name' => $clinicLoop->getFantasia(),
                        'status' => $clinicLoop->isEnabled(),
                        'id' => $clinicLoop->getId()
                    ];


                    # Tente pegar a cliente com utlimo acesso e que possua alguma configuracao
                    if ($userClinicLoop->getLastAccess()) {
                        $inSession = $clinicLoop;
                        $clinic = $clinicLoop;
                        $userClinic = $userClinicLoop;
                    }
                }
            }

//            if(sizeof($clinics) > 0){
//                $session->set('app.clinics', $clinics);
//
//                // Get the first clinic
//                if (! empty($inSession)) {
//                    $firstClinic = $inSession;
//                }else {
//                    $firstClinic = current($clinics)['id'];
//                }
//
////                $defaultRoute = "doctor_client_list";
////                $session->set('app.default_route', $defaultRoute);
//
//                $userClinic = $container->get('doctrine')->getRepository(Clinic::class)->getUserClinic($user, $firstClinic);
//                $clinic = $userClinic->getClinic();
//                if (null !== $userClinic) {
//                    $session->set('app.isOwner', $userClinic->IsOwner());
//
//                    if (!$userClinic->getClinic()->isEnabled()) {
//                        return false;
//                    }
//                }
//            }
        }

        if (!is_null($userClinic)) {

            $defaultRoute = "doctor_client_list";
            $session->set('app.default_route', $defaultRoute);
            $session->set('app.clinic_id', $clinic->getId());
            $session->set('app.clinic', $clinic);
            $session->set('app.isOwner', $userClinic->IsOwner());

        }

    }

    public static function loadChangeClinic(ContainerInterface $container, $userClinic)
    {

        $session = $container->get('session');
        $defaultRoute = $session->get('app.default_route');
        $clinics = $session->get('app.clinics');

        $clinic = $userClinic->getClinic();
        self::loadSessionSettings($container, $userClinic->getUser());


        $session->getFlashBag()->add('info', "Clinica alterada para " . $clinic->getFantasia());
    }
}
