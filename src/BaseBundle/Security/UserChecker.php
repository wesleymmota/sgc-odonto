<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 11/08/18
 * Time: 22:16
 */

namespace BaseBundle\Security;


use App\Exception\AccountDeletedException;
use App\Security\User as AppUser;
use BaseBundle\Entity\User;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class UserChecker implements UserCheckerInterface
{
    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

//        if ($this->get('security.authorization_checker')->isGranted('ROLE_ADMIN_AREA')) {
//
//            echo "Post auth ROLE_ADMIN_AREA";
//        } elseif ($this->get('security.authorization_checker')->isGranted('ROLE_DOCTOR_AREA')) {
//            echo "Post auth ROLE_DOCTOR_AREA";
//        }
//        dump($user);
//        echo "Post auth";die();
    }
}