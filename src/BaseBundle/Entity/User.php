<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use BaseBundle\Entity\Traits\DateTime;
use BaseBundle\Entity\Traits\UserUUId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="emailCanonical",
 *          column=@ORM\Column(
 *              name     = "email_canonical",
 *              type     = "string",
 *              unique   = false,
 *              nullable = true
 *          )
 *      ),
 *     @ORM\AttributeOverride(name="email",
 *          column=@ORM\Column(
 *              name     = "email",
 *              type     = "string",
 *              unique   = false,
 *              nullable = true
 *          )
 *      )
 * })
 * @ORM\HasLifecycleCallbacks()
 */
class User extends BaseUser implements EquatableInterface
{
    use ArrayTrait;
    use UserUUId;

    const STATE_CIVIL_SINGLE    = "single";
    const STATE_CIVIL_MARRIED   = "merried";
    const GENDER_MALE           = "male";
    const GENDER_FEMALE         = "female";

    const TYPE_INFO    = "info";
    const TYPE_WARNING = "warning";
    const TYPE_SUCCESS = "success";
    const TYPE_DANGER  = "danger";

    const ROLE_USER         = "ROLE_USER";
    const ROLE_CLIENT       = "ROLE_CLIENT";
    const ROLE_EMPLOYEE     = "ROLE_EMPLOYEE";
    const ROLE_DOCTOR       = "ROLE_DOCTOR";
    const ROLE_ADMIN        = "ROLE_ADMIN";

    const ROLE_DOCTOR_AREA  = "ROLE_DOCTOR_AREA";
    const ROLE_ADMIN_AREA   = "ROLE_ADMIN_AREA";
    const ROLE_USER_AREA    = "ROLE_USER_AREA";

    const USER_DEFAULT_PASSWORD = "123456789";


    /** @var array user friendly named type */
    protected static $genderName = [
        self::GENDER_MALE    => 'user.label.male',
        self::GENDER_FEMALE => 'user.label.female',
    ];

    /** @var array user friendly named type */
    protected static $stateCivilName = [
        self::STATE_CIVIL_SINGLE    => 'user.label.single',
        self::STATE_CIVIL_MARRIED => 'user.label.married',
    ];

    protected static $adminRoles = [
        self::ROLE_DOCTOR    => 'user.label.role.doctor',
        self::ROLE_CLIENT => 'user.label.role.user',
        self::ROLE_ADMIN => 'user.label.role.admin',
        self::ROLE_EMPLOYEE => 'user.label.role.employee',
    ];

    protected static $adminProfile = [
        self::ROLE_CLIENT => 'user.label.role.user',
        self::ROLE_ADMIN => 'user.label.role.admin',
        self::ROLE_EMPLOYEE => 'user.label.role.employee',
    ];

    /**
     * @return array<string>
     */
    public static function getProfilesRoles()
    {
        return [

            self::ROLE_EMPLOYEE,
            self::ROLE_ADMIN,
        ];
    }

    /**
     * @return array<string>
     */
    public static function getAdminProfilesRoles()
    {
        return [

            self::ROLE_EMPLOYEE,
            self::ROLE_ADMIN,
        ];
    }
    /**
     * @return array<string>
     */
    public static function getAdminRoles()
    {
        return [
            self::ROLE_DOCTOR,
            self::ROLE_EMPLOYEE,
            self::ROLE_ADMIN,
        ];
    }

    protected static $adminClinicRoles = [
        self::ROLE_DOCTOR    => 'user.label.role.doctor',
        self::ROLE_CLIENT => 'user.label.role.user',
        self::ROLE_EMPLOYEE => 'user.label.role.employee',
    ];

    /**
     * @return array<string>
     */
    public static function getClinicAdminRoles()
    {
        return [
            self::ROLE_DOCTOR,
            self::ROLE_EMPLOYEE
        ];
    }

    protected static $employeeClinicRoles = [
        self::ROLE_CLIENT => 'user.label.role.user',

    ];

    /**
     * @return array<string>
     */
    public static function getClinicEmplyeeRoles()
    {
        return [
            self::ROLE_EMPLOYEE
        ];
    }


    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getStateRole($typeShortName)
    {
        if (!isset(static::$adminRoles[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$adminRoles[$typeShortName];
    }

    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getStateCivilName($typeShortName)
    {
        if (!isset(static::$stateCivilName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$stateCivilName[$typeShortName];
    }

    /**
     * @param  string $gender
     * @return string
     */
    public static function getGenderName($typeShortName)
    {
        if (!isset(static::$genderName[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$genderName[$typeShortName];
    }

    public function getUserGender()
    {
        return self::getGenderName($this->getGender());
    }

    /**
     * @return array<string>
     */
    public static function getAvailableStateCivil()
    {
        return [
            self::STATE_CIVIL_SINGLE,
            self::STATE_CIVIL_MARRIED
        ];
    }


    /**
     * @return owner
     */
    public function getDefaulClinic($clinic)
    {
        $testaId =
            function($element) use ($clinic) {

                if($element->getClinic()->getId() == $clinic->getId()) {
                    return true;
                }
                return false;
            }
        ;
        $ret = $this->getUsers()->filter($testaId);

        return $ret->isEmpty() ? null : $ret->first();

    }
    /**
     * @return array<string>
     */
    public static function getAvailableGender()
    {
        return [
            self::GENDER_MALE,
            self::GENDER_FEMALE
        ];
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected $id;
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @param int $id
     *
     * @return IdTrait
     */
    public function setId($id)
    {
        if ((int) $id <= 0) {
            throw new \RuntimeException(__FUNCTION__.' accept only positive integers greater than zero and');
        }
        $this->id = $id;
        return $this;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="text", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="rg", type="string", length=20, nullable=true)
     */
    protected $rg;

    /**
     * @var string
     *
     * @ORM\Column(name="cpf", type="string", length=20, nullable=true)
     */
    protected $cpf;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=20, nullable=true)
     */
    protected $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    protected $address;

    /**
     * @var string Bairro
     *
     * @ORM\Column(name="neighborhood", type="string", nullable=true)
     */
    protected $neighborhood;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    protected $zip_code;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", nullable=true)
     */
    protected $number;

    /**
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_city", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $city;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="admission_date", type="datetime", nullable=true)
     */
    protected $admission_date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    protected $birth_date;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone", type="string", nullable=true)
     */
    protected $cell_phone;

    /**
     * @var string
     *
     * @ORM\Column(name="state_civil", type="string", nullable=true)
     */
    protected $state_civil;

    /**
     * @var string
     *
     * @ORM\Column(name="cro", type="string", nullable=true)
     */
    protected $cro;

    /**
     * @var string
     *
     * @ORM\Column(name="profession", type="string", nullable=true)
     */
    protected $profession;

    /**
     * @var \Photo
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Assets", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_photo", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    protected $notes;


    /**
     * @var string
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Specialization")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_specialization", referencedColumnName="id", onDelete="SET NULL")
     * })
     *
     */
    protected $specialization;


    /**
     * @var \BaseBundle\Entity\UserClinical
     * @ORM\OneToMany(targetEntity="BaseBundle\Entity\UserClinical", mappedBy="user",fetch="EXTRA_LAZY")
     */
    protected $users;

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->locked = false;
        $this->expired = false;
        $this->roles = array();
        $this->credentialsExpired = false;
        $this->enabled = true;
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->uuid = Uuid::uuid4();
    }


    public function hasPhoto(): bool
    {
        if (is_null($this->photo)) {
            return false;
        }
        return true;
    }

    public function isEnabled()
    {
        return $this->enabled;
    }


    public static function getSexOptions($assoc = false)
    {
        $operation = array(
            self::GENDER_MALE => 'M',
            self::GENDER_FEMALE => 'F'
        );
        return $associativo === true ? $operacao : array_keys($operacao);
    }

    public function isEqualTo(UserInterface $user)
    {   

        if ($user instanceof User) {
            if ($this->password !== $user->getPassword()) {
                return false;
            }

            if ($this->salt !== $user->getSalt()) {
                return false;
            }

            if ($this->username !== $user->getUsername()) {
                return false;
            }
            
            return true;

        }
        return false;
        
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getRg()
    {
        return $this->rg;
    }

    /**
     * @param string $rg
     * @return User
     */
    public function setRg($rg)
    {
        $this->rg = $rg;
        return $this;
    }

    /**
     * @return string
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param string $cpf
     * @return User
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return User
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     * @return User
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param string $zip_code
     * @return User
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return User
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getAdmissionDate()
    {
        return $this->admission_date;
    }

    /**
     * @param \DateTime $admission_date
     * @return User
     */
    public function setAdmissionDate($admission_date)
    {
        $this->admission_date = $admission_date;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * @param \DateTime $birth_date
     * @return User
     */
    public function setBirthDate($birth_date)
    {
        $this->birth_date = $birth_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    /**
     * @return string
     */
    public function getcell_phone()
    {
        return $this->cell_phone;
    }

    /**
     * @param string $cell_phone
     * @return User
     */
    public function setCellPhone($cell_phone)
    {
        $this->cell_phone = $cell_phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getStateCivil()
    {
        return $this->state_civil;
    }

    /**
     * @param string $state_civil
     * @return User
     */
    public function setStateCivil($state_civil)
    {
        $this->state_civil = $state_civil;
        return $this;
    }

    /**
     * @return string
     */
    public function getCro()
    {
        return $this->cro;
    }

    /**
     * @param string $cro
     * @return User
     */
    public function setCro($cro)
    {
        $this->cro = $cro;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfession()
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     * @return User
     */
    public function setProfession($profession)
    {
        $this->profession = $profession;
        return $this;
    }

    /**
     * @return \Photo
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * @param \Photo $photo
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return User
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * @param string $specialization
     * @return User
     */
    public function setSpecialization($specialization)
    {
        $this->specialization = $specialization;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        if (is_null($this->salt)) {
            $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        }
        return $this->salt;
    }


    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return User
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return string
     */
    public function getGenderIcon()
    {
        if ($this->gender === self::GENDER_MALE) {
            return "user4.jpg";
        }else {
            return "user1.jpg";
        }
    }
    /**user4.jpg
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return UserClinical
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param UserClinical $users
     * @return User
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }
}
