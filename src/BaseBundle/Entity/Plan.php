<?php

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;

/**
 * Plano
 *
 * @ORM\Table(
 *     name="tratament_plan"
 * )
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 */
class Plan
{
    use BaseUUId;
    use ArrayTrait;


    public function __toString()
    {
        return $this->getDetails();
    }
    /**
     * @var string
     *
     * @ORM\Column(name="details", type="text",nullable=true)
     */
    private $details;


    /**
     * @var \DateTime
     * @ORM\Column(name="execussion_date", type="datetime", nullable=true)
     *
     */
    protected $date;

    /**
     * @var \BaseBundle\Entity\Clinic
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Clinic",cascade={"all"},fetch="EXTRA_LAZY")
     */
    protected $clinic;

    /**
     * @var \BaseBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User",fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_professional", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $professional;

    /**
     * @var \BaseBundle\Entity\Budget
     *
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\Budget",mappedBy="plan", orphanRemoval=true, cascade={"all"},fetch="EXTRA_LAZY")
     */
    protected $budget;

    /**
     * @var \BaseBundle\Entity\PlanProcedure
     *
     * @ORM\OneToMany(targetEntity="BaseBundle\Entity\PlanProcedure", mappedBy="plan",cascade={"persist"},fetch="EXTRA_LAZY", orphanRemoval=true)
     */
    protected $procedures;

    /**
     * @var BaseBundle\Entity\PlanFile
     *
     * @ORM\OneToMany(targetEntity="BaseBundle\Entity\PlanFile", mappedBy="document", cascade={"persist"},fetch="EXTRA_LAZY", orphanRemoval=true)
     *
     */
    protected $files;

    public function __construct() {
        $this->date = null;
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->procedures = new \Doctrine\Common\Collections\ArrayCollection();

    }



    public function hasProcedurePendding()
    {

        $testaId =
            function($element) {
                if($element->getStatus() != PlanProcedure::STATUS_DONE) {
                    return true;
                }
                return false;
            }
        ;

        $ret = $this->procedures->filter($testaId);

        return $ret->count();

    }

    /**
     * @return  \BaseBundle\Entity\PlanProcedure
     */
    public function getProcedures()
    {
        return $this->procedures;
    }

    /**
     * @return \BaseBundle\Entity\Procedure
     */
    public function setProcedures($procedures = null)
    {
        $this->procedures = $procedures;
       return $this;
    }

    public function addProcedures($procedures){

        $this->procedures =  $procedures;
        return $this;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Plan
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return Plan
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * @param Clinic $clinic
     * @return Plan
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }


    /**
     * @return string
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param string $details
     * @return Plan
     */
    public function setDetails($details)
    {
        $this->details = $details;
        return $this;
    }

    /**
     * @return User
     */
    public function getProfessional()
    {
        return $this->professional;
    }

    /**
     * @param User $professional
     * @return Plan
     */
    public function setProfessional(User $professional)
    {
        $this->professional = $professional;
        return $this;
    }

    /**
     * @return User
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param User $budget
     * @return Plan
     */
    public function setBudget(Budget $budget)
    {
        $this->budget = $budget;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    /**
     * @param ArrayCollection $uploadedFiles
     * @return Plan
     */
    public function setUploadedFiles($uploadedFiles)
    {
        $this->uploadedFiles = $uploadedFiles;
        return $this;
    }

    /**
     * @return BaseBundle\Entity\PlanFile
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param BaseBundle\Entity\PlanFile $files
     * @return Plan
     */
    public function setFiles($files)
    {
        $this->files = $files;
        return $this;
    }

    /**
     * @return Clinic
     */
    public function getAgreement()
    {
        if ($this->getBudget()->getAgreement() instanceof Agreement) {
            if($this->getBudget()->getAgreement() != null) {
                return $this->getBudget()->getAgreement();
            }
        }
        return null;
    }
}
