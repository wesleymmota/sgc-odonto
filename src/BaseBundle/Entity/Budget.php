<?php

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Finder\Finder;

/**
 * Budget
 *
 * @ORM\Table(
 *     name="clinical_budget"
 * )
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\BudgetRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Budget
{
    use BaseUUId;
    use ArrayTrait;
    const PAYMENT_CREDIT_CARD    = "credit_card";
    const PAYMENT_MONEY          = "money";
    const PAYMENT_TICKET         = "ticker";

    const STATUS_PENDIND           = NULL;
    const STATUS_APPROVED          = 1;
    const STATUS_DISAPPROVED       = 0;
    const STATUS_FINISHED          = 2 ;

    const PAYMENT_STATE_PAID       = 1;
    const PAYMENT_STATE_PENDING    = 0;

    /** @var array user friendly named type */
    protected static $paymentMethod = [
        self::PAYMENT_CREDIT_CARD    => 'clinical.user.quote.payment_credit_card',
        self::PAYMENT_MONEY => 'clinical.user.quote.payment_money',
        self::PAYMENT_TICKET => 'clinical.user.quote.payment_ticket'
    ];

    /** @var array user friendly named type */
    protected static $statusMethod = [
        self::STATUS_PENDIND    => 'clinical.user.quote.status.pending',
        self::STATUS_APPROVED => 'clinical.user.quote.status.approved',
        self::STATUS_DISAPPROVED => 'clinical.user.quote.status.disaproved',
        self::STATUS_FINISHED => 'clinical.user.quote.status.finished'
    ];


    /** @var array user friendly named type */
    protected static $paymentStateMethod = [
        self::PAYMENT_STATE_PENDING    => 'clinical.user.quote.status.pending',
        self::PAYMENT_STATE_PAID => 'clinical.user.quote.status.finished'
    ];
    /**
     * @return array<string>
     */
    public static function getPaymentMethods()
    {
        return [

            self::PAYMENT_CREDIT_CARD,
            self::PAYMENT_MONEY,
            self::PAYMENT_TICKET
        ];
    }

    /**
     * @return array<string>
     */
    public static function getPaymentStateMethods()
    {
        return [

            self::PAYMENT_STATE_PENDING,
            self::PAYMENT_STATE_PAID
        ];
    }
    /**
     * @return array<string>
     */
    public static function getStateMethods()
    {
        return [

            self::STATUS_PENDIND,
            self::STATUS_APPROVED,
            self::STATUS_DISAPPROVED,
            self::STATUS_FINISHED
        ];
    }

    /**
     * @return array<string>
     */
    public static function getThootElements()
    {
        $el = [];
        for ($i = 1; $i <= 32; $i++) {
           array_push($el,$i);
        }
        return $el;
    }

    /**
     * @param  string $paymentMethod
     * @return string
     */
    public static function getPaymentMethod($typeShortName)
    {
        if (!isset(static::$paymentMethod[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$paymentMethod[$typeShortName];
    }


    /**
     * @param  string $paymentMethod
     * @return string
     */
    public static function getStateMethod($typeShortName)
    {
        if (!isset(static::$statusMethod[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$statusMethod[$typeShortName];
    }


    /**
     * @var string pagamento
     * @ORM\Column(name="payment", type="string", nullable=true)
     */
    protected $payment;

    /**
     * @var \DateTime
     * @ORM\Column(name="validity", type="datetime", nullable=true)
     */
    protected $validity;

    /**
     * @var string  status(1=Aprovado, 0=reprovado, null = pendente de aprovacao
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    protected $status;

    /**
     * @var string  status(1= done,)
     *
     * @ORM\Column(name="done", type="boolean", nullable=true)
     */
    protected $done = 0;

    /**
     * @var string  status(1= done,)
     *
     * @ORM\Column(name="paid", type="boolean", nullable=true)
     */
    protected $paid = 0;


    /**
     * @var \DateTime
     * @ORM\Column(name="paid_date", type="datetime", nullable=true)
     *
     */
    protected $paid_date;

    /**
     * @var string - Dente
     *
     * @ORM\Column(name="tooth", type="integer", nullable=true)
     */
    protected $tooth;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="decimal", precision=7, scale=2)
     */
    protected $value = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="total", type="decimal", precision=7, scale=2)
     */
    protected $total = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    protected $discount;

    /**
     * Convenio
     * @var \BaseBundle\Entity\Clinic
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Agreement", fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_agreement", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $agreement;

    /**
     * Plano de tratamento
     * @var \BaseBundle\Entity\Clinic
     *
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\Plan",inversedBy="budget" ,fetch="EXTRA_LAZY", cascade={"all"})
     */
    protected $plan;

    /**
     * @var \BaseBundle\Entity\Clinic
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Clinic",cascade={"all"},fetch="EXTRA_LAZY")
     */
    protected $clinic;

//    /**
//     * @var \BaseBundle\Entity\User
//     *
//     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User",fetch="EXTRA_LAZY")
//     * @ORM\JoinColumns({
//     *   @ORM\JoinColumn(name="fk_professional", referencedColumnName="id", onDelete="SET NULL")
//     * })
//     */
//    protected $professional;

    /**
     * @var \BaseBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User",fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_created_by", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $createdBy;

    /**
     * @var \BaseBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User",fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_client", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $client;

    public function __construct()
    {
        $this->done = 0;
        $this->paid = 0;
        //$this->plan = new Plan();
    }

//    /**
//     * @param \BaseBundle\Entity\Procedure procedure
//     */
//    public function addProcedure(Procedure $procedure)
//    {
//        $this->getPlan()->addProcedure($procedure);
//    }
//
//    /**
//     * @param \BaseBundle\Entity\Procedure procedure
//     */
//    public function removeProcedure(Procedure $procedure)
//    {
//        $this->getPlan()->removeProcedure($procedure);
//    }

    /**
     * @return \BaseBundle\Entity\Procedure
     */
    public function getProcedures()
    {
        return $this->getPlan()->getProcedures();
    }

    /**
     * @return string
     */
    public function getTotal()
    {

        return $this->total;
    }


    /**
     * @param string $total
     * @return Budget
     *
     */
    public function setTotal()
    {

        $percentual = $this->getDiscount() / 100.0; // pega a porcentagem 0,15

        $this->total = $this->getValue() - ( $percentual * $this->getValue());

        return $this;
    }



    /**
     * @return string
     */
    public function getTooth()
    {
        return $this->tooth;
    }

    /**
     * @param string $tooth
     * @return Budget
     */
    public function setTooth($tooth)
    {
        $this->tooth = $tooth;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return Budget
     */
    public function setValue($value)
    {
        $this->value = $value;
        $this->setTotal();

        return $this;
    }

    /**
     * @return string
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * @param string $discount
     * @return Budget
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return Clinic
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * @param Clinic $agreement
     * @return Budget
     */
    public function setAgreement($agreement)
    {
        $this->agreement = $agreement;
        return $this;
    }

    /**
     * @return Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param Clinic $plan
     * @return Budget
     */
    public function setPlan($plan)
    {
        $this->plan = $plan;
        return $this;
    }



    /**
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param string $payment
     * @return Budget
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * @param \DateTime $validity
     * @return Budget
     */
    public function setValidity(\DateTime $validity): Budget
    {
        $this->validity = $validity;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Budget
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Budget
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return User
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param User $client
     * @return Budget
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * @return Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * @param Clinic $clinic
     * @return Budget
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }

    /**
     * @return string
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * @param string $done
     * @return Budget
     */
    public function setDone($done = 0)
    {
        $this->done = $done;
        return $this;
    }

    /**
     * @return string
     */
    public function getPaid()
    {
        return $this->paid;
    }

    /**
     * @param string $paid
     * @return Budget
     */
    public function setPaid($paid)
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPaidDate()
    {
        return $this->paid_date;
    }

    /**
     * @param \DateTime $paid_date
     * @return Budget
     */
    public function setPaidDate( $paid_date)
    {
        $this->paid_date = $paid_date;
        return $this;
    }


    public function isValid(): bool
    {
        $now    = new \DateTime('now');

        if ($this->getValidity() > $now) {

            return true;
        }else{
            return false;
        }
    }


    public function getToothImage()
    {
        $finder = new Finder();

        $finder->in(APPLICATION_PATH . "/public/img/dentes")->name($this->getTooth() . ".png");

        if ($finder->count() <= 0) {

            return null;
        }
        if ($finder->hasResults()){
            $iterator = $finder->getIterator();
            $iterator->rewind();
            $file = $iterator->current();
            return "/img/dentes/".$file->getRelativePathname();
        }
        return null;
    }
}
