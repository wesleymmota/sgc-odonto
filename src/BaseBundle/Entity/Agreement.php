<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use BaseBundle\Entity\Traits\DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use FOS\UserBundle\Model\User as BaseUser;
use AdminBundle\Validator\Constraints as BFOSBrasilAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Convenio medico
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\AgreementRepository")
 * @ORM\Table(name="clinical_agreement")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Agreement
{
    use BaseUUId;
    use ArrayTrait;

    public function __toString()
    {
        if (null !== $this->fantasia)
            return $this->fantasia;

         if (null !== $this->razao)
             return $this->razao;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="fantasia", type="string", length=100, nullable=true)
     */
    protected $fantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="razao", type="string", nullable=true)
     */
    protected $razao;


    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", nullable=true)
     */
    protected $site;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     * @Assert\Email(
     *     message = "O email '{{ value }}' não é valido.",
     *     checkMX = true
     * )
     */
    protected $email;

    /**
     * @var string
     *
     * @ORM\Column(name="ie", type="string", length=20, nullable=true)
     */
    protected $ie;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="cnpj", type="string", length=20, nullable=true)
     *  @BFOSBrasilAssert\CpfCnpj(
     *     cnpj=true,
     *     mask=true
     *     )
     */
    protected $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    protected $address;

    /**
     * @var string Bairro
     *
     * @ORM\Column(name="neighborhood", type="string", nullable=true)
     */
    protected $neighborhood;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    protected $zip_code;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", nullable=true)
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone", type="string", nullable=true)
     */
    protected $cell_phone;

    /**
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_city", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $city;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    protected $notes;

    /**
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Clinic")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_clinic", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $clinic;



    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getFantasia()
    {
        return $this->fantasia;
    }

    /**
     * @param string $fantasia
     * @return Agreement
     */
    public function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
        return $this;
    }

    /**
     * @return string
     */
    public function getRazao()
    {
        return $this->razao;
    }

    /**
     * @param string $razao
     * @return Agreement
     */
    public function setRazao($razao)
    {
        $this->razao = $razao;
        return $this;
    }

    /**
     * @return string
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param string $site
     * @return Agreement
     */
    public function setSite($site)
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Agreement
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getIe()
    {
        return $this->ie;
    }

    /**
     * @param string $ie
     * @return Agreement
     */
    public function setIe($ie)
    {
        $this->ie = $ie;
        return $this;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param string $cnpj
     * @return Agreement
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Agreement
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     * @return Agreement
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param string $zip_code
     * @return Agreement
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Agreement
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Agreement
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    /**
     * @param string $cell_phone
     * @return Agreement
     */
    public function setCellPhone($cell_phone)
    {
        $this->cell_phone = $cell_phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Agreement
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     * @return Agreement
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * @param mixed $clinic
     * @return Agreement
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }

}
