<?php

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Procedure
 *
 * @ORM\Table(
 *     name="clinical_procedure"
 * )
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\ProcedureRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Procedure
{
    public function __toString()
    {
        return (string) $this->getDescription();
    }
    use BaseUUId;
    use ArrayTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="description",type="text", nullable=true)
     * @Assert\NotBlank()
     */
    protected $description;

    /**
     * @var string
     *
     * @ORM\Column(name="color",type="string", nullable=true)
     */
    protected $color;

    /**
     * @var string
     *
     * @ORM\Column(name="average_time",type="integer", nullable=true)
     */
    protected $average_time;

    /**
     * @var PlanProcedure $procedures
     *
     * @ORM\OneToMany(targetEntity="BaseBundle\Entity\PlanProcedure", mappedBy="procedure",fetch="EXTRA_LAZY")
     */
    protected $procedures;

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Procedure
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getAverageTime()
    {
        return $this->average_time;
    }

    /**
     * @param string $average_time
     * @return Procedure
     */
    public function setAverageTime($average_time)
    {
        $this->average_time = $average_time;
        return $this;
    }

    /**
     * @return PlanProcedure
     */
    public function getProcedures(): PlanProcedure
    {
        return $this->procedures;
    }

    /**
     * @param PlanProcedure $procedures
     * @return Procedure
     */
    public function setProcedures(PlanProcedure $procedures): Procedure
    {
        $this->procedures = $procedures;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return Procedure
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }




}
