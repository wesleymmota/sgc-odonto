<?php

namespace BaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;

/**
 * Class City
 * @package BaseBundle\Entity
 *
 * @ORM\Table(
 *     name="city"
 * )
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class City
{
    use Base;
    use ArrayTrait;


    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true, unique=false)
     */
    protected $name;


    /**
     * @var string
     *
     * @ORM\Column(name="code", type="integer", unique=false)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=200, nullable=false, unique=false)
     */
    protected $uf;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return City
     */
    public function setName(string $name): City
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return City
     */
    public function setCode(string $code): City
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return Uf
     */
    public function getUf(): string
    {
        return $this->uf;
    }

    /**
     * @param Uf $uf
     * @return City
     */
    public function setUf($uf): City
    {
        $this->uf = $uf;
        return $this;
    }
}
