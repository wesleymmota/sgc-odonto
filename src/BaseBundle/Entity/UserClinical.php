<?php
namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

//@ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
/**
 * @ORM\Entity()
 * @ORM\Table(name="user_clinical")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class UserClinical
{
    use BaseUUId;
    use ArrayTrait;

    /**
     * @var \BaseBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\User",inversedBy="users",cascade={"persist"},fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_user", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $user;

    /**
     * @var \BaseBundle\Entity\Clinic
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Clinic", inversedBy="users", cascade={"persist"},fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_clinic", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $clinic;

    /**
     * @var owner
     *
     * @ORM\Column(name="owner", type="boolean", nullable=true)
     */
    protected $owner;

    /**
     * @var employee
     *
     * @ORM\Column(name="employee", type="boolean", nullable=true)
     */
    protected $employee;

    /**
     * @var client
     *
     * @ORM\Column(name="client", type="boolean", nullable=true)
     */
    protected $client;

    /**
     * @var last_access
     *
     * @ORM\Column(name="last_access", type="boolean", nullable=true)
     */
    protected $lastAccess;


    public function __construct(User $user = null, Clinic $clinic = null)
    {
        $this->user = $user;
        $this->clinic = $clinic;
        $this->client = false;
        $this->owner = false;
        $this->employee = false;
        $this->lastAccess = false;
    }


    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }


    /**
     * @return User
     */
    public function isUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserClinical
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Clinic
     */
    public function getClinic()
    {
        return $this->clinic;
    }


    /**
     * @param Clinic $clinc
     * @return UserClinical
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }

    /**
     * @return owner
     */
    public function isOwner()
    {
        return $this->owner;
    }

    /**
     * @param owner $owner
     * @return UserClinical
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return employee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * @return employee
     */
    public function isEmployee()
    {
        return $this->employee;
    }

    /**
     * @param employee $employee
     * @return UserClinical
     */
    public function setEmployee($employee)
    {
        $this->employee = $employee;
        return $this;
    }

    /**
     * @return client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @return client
     */
    public function isClient()
    {
        return $this->client;
    }

    /**
     * @param client $client
     * @return UserClinical
     */
    public function setClient($client)
    {
        $this->client = $client;
        return $this;
    }

    /**
     * Getter of $lastAccess
     *
     * @return last_access
     */
    public function getLastAccess()
    {
        return $this->lastAccess;
    }

    /**
     * @param last_access $lastAccess
     * @return UserClinical
     */
    public function setLastAccess($lastAccess)
    {
        $this->lastAccess = $lastAccess;
        return $this;
    }
}
