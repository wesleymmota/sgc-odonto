<?php


namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Clinica
 *
 * @ORM\Table(
 *     name="clinic"
 * )
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\ClinicRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Clinic
{
    use BaseUUId;
    use ArrayTrait;

    public function __toString()
    {
        return $this->getFantasia();
    }

    const LOCALE_PT_BR    = "pt_BR";
    const LOCALE_EN   = "en";
    /**
     * @return array<string>
     */
    public static function getAvailableLocale()
    {
        return [
            self::LOCALE_PT_BR,
            self::LOCALE_EN
        ];
    }

    protected static $getLocales = [
        self::LOCALE_PT_BR => 'PT BR',
        self::LOCALE_EN => 'EN',
    ];
    /**
     * @param  string $typeShortName
     * @return string
     */
    public static function getLocaleName($typeShortName)
    {
        if (!isset(static::$getLocales[$typeShortName])) {
            return "Unknown type ($typeShortName)";
        }

        return static::$getLocales[$typeShortName];
    }
    /**
     * @var string
     *
     * @ORM\Column(name="fantasia", type="string", length=100, nullable=true)
     */
    protected $fantasia;

    /**
     * @var string
     *
     * @ORM\Column(name="razao", type="text", nullable=true)
     */
    protected $razao;

    /**
     * @var string
     *
     * @ORM\Column(name="ie", type="string", length=20, nullable=true)
     */
    protected $ie;

    /**
     * @var string
     *
     *
     * @ORM\Column(name="cnpj", type="string", length=20, nullable=true)
     */
    protected $cnpj;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    protected $address;

    /**
     * @var string Bairro
     *
     * @ORM\Column(name="neighborhood", type="string", nullable=true)
     */
    protected $neighborhood;

    /**
     * @var string
     *
     * @ORM\Column(name="zip_code", type="string", nullable=true)
     */
    protected $zip_code;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", nullable=true)
     */
    protected $number;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", nullable=true)
     */
    protected $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="cell_phone", type="string", nullable=true)
     */
    protected $cell_phone;

    /**
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\City")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_city", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $city;

    /**
     * Usuários da clinica.
     *
     * @var \BaseBundle\Entity\UserClinical
     *
     * @ORM\OneToMany(targetEntity="BaseBundle\Entity\UserClinical", mappedBy="clinic", orphanRemoval=true, cascade={"all"},fetch="EXTRA_LAZY")
     */
    protected $users;


    /**
     * @var \BaseBundle\Entity\ClinicalSettings
     *
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\ClinicalSettings", inversedBy="clinic", cascade={"all"},fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_settings", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $settings;


    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=true)
     */
    protected $enabled;

    public function __construct()
    {
        $this->enabled =  true;
        $this->settings = new ClinicalSettings();
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getOwners()
    {
        $users = $this->getUsers();
        $ArrayCollectionOfActiveUsers = $users->filter(function ($user) {
            return $user->isOwner() === true;
        });


        return $ArrayCollectionOfActiveUsers;
    }

//    public function getOwner()
//    {
//        $users = $this->getUsers();
//        $ArrayCollectionOfActiveUsers = $users->filter(function ($user) {
//            return $user->isOwner() === true;
//        });
//
//
//        return $ArrayCollectionOfActiveUsers->first();
//    }
    /**
     * @return string
     */
    public function getName()
    {
        return $this->fantasia;
    }

    /**
     * @return string
     */
    public function getFantasia()
    {
        return $this->fantasia;
    }

    /**
     * @param string $fantasia
     * @return Clinic
     */
    public function setFantasia($fantasia)
    {
        $this->fantasia = $fantasia;
        return $this;
    }

    /**
     * @return string
     */
    public function getRazao()
    {
        return $this->razao;
    }

    /**
     * @param string $razao
     * @return Clinic
     */
    public function setRazao($razao)
    {
        $this->razao = $razao;
        return $this;
    }

    /**
     * @return string
     */
    public function getIe()
    {
        return $this->ie;
    }

    /**
     * @param string $ie
     * @return Clinic
     */
    public function setIe($ie)
    {
        $this->ie = $ie;
        return $this;
    }

    /**
     * @return string
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param string $cnpj
     * @return Clinic
     */
    public function setCnpj($cnpj)
    {
        $this->cnpj = $cnpj;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     * @return Clinic
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return string
     */
    public function getNeighborhood()
    {
        return $this->neighborhood;
    }

    /**
     * @param string $neighborhood
     * @return Clinic
     */
    public function setNeighborhood($neighborhood)
    {
        $this->neighborhood = $neighborhood;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param string $zip_code
     * @return Clinic
     */
    public function setZipCode($zip_code)
    {
        $this->zip_code = $zip_code;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param string $number
     * @return Clinic
     */
    public function setNumber($number)
    {
        $this->number = $number;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return Clinic
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getCellPhone()
    {
        return $this->cell_phone;
    }

    /**
     * @param string $cell_phone
     * @return Clinic
     */
    public function setCellPhone($cell_phone)
    {
        $this->cell_phone = $cell_phone;
        return $this;
    }


    /**
     * @param \Photo $photo
     * @return Clinic
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     * @return Clinic
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }



    /**
     * @return ClinicalSettings
     */
    public function getSettings()
    {
        return $this->settings;
    }

    /**
     * @param ClinicalSettings $settings
     * @return Clinic
     */
    public function setSettings(ClinicalSettings $settings)
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * @return User
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param User $users
     * @return Clinic
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return string
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param string $enabled
     * @return Clinic
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }
}
