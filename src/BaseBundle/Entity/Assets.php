<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 08/12/17
 * Time: 00:28
 */

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Photo
 *
 * @ORM\Table(
 *     name="assets"
 * )
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Assets
{
    use BaseUUId;
    use ArrayTrait;
    const ASSETS_FILE_PATH='uploads/';

    public function upload($class)
    {
        if (!$this->getImage() instanceof UploadedFile) {
            throw new \Exception("No such file or directory.");
        }

        $file = $this->getImage();
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $this->setFilename($fileName);
        $this->setExtension($file->guessExtension());
        $this->setClass($class);

        $file->move($this->getTargetDir(), $fileName);

        return $this->getTargetDir() . "/". $fileName;
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }
    protected $targetDir;

    /**
     * @Assert\NotBlank(message="Please, upload the product brochure as a JPEG file.")
     * @Assert\File( maxSize="10M", mimeTypes={"image/jpeg", "image/png"} )
     */
    protected $image;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", nullable=false)
     */
    protected $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="class", type="string", nullable=true)
     */
    protected $class;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", nullable=true)
     */
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    protected $url;

    /**
     * @return string
     */
    public function getFilename()
    {
        if (is_null($this->filename)) {
            return "{$this->name}.{$this->extension}";
        }

        return $this->filename;
    }

    /**
     * @param string $filename
     * @return Asset
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
        return $this;
    }


    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     * @return Asset
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Asset
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return Asset
     */
    public function setClass(string $class)
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTargetDir()
    {
        return $this->targetDir;
    }

    /**
     * @param mixed $targetDir
     * @return Assets
     */
    public function setTargetDir($targetDir)
    {
        $this->targetDir = self::ASSETS_FILE_PATH . $targetDir;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }/**
     * @param mixed $image
     * @return Assets
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }
}
