<?php
namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

//@ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
/**
 * @ORM\Entity()
 * @ORM\Table(name="plan_procedure")
 * @ORM\HasLifecycleCallbacks()
 *
 */
class PlanProcedure
{
    use BaseUUId;
    use ArrayTrait;

    const STATUS_PENDIND           = 0;
    const STATUS_DONE              = 1;

    /** @var array user friendly named type */
    protected static $statusMethod = [
        self::STATUS_PENDIND    => 'clinical.user.quote.status.pending',
        self::STATUS_DONE => 'clinical.user.quote.status.aproved',
    ];

    /**
     * @return array<string>
     */
    public static function getStateMethods()
    {
        return [

            self::STATUS_PENDIND,
            self::STATUS_APPROVED,
            self::STATUS_DISAPPROVED
        ];
    }

    /**
     * @var \BaseBundle\Entity\Plan
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Plan",inversedBy="procedures",cascade={"persist"},fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_plan", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $plan;

    /**
     * @var \BaseBundle\Entity\Procedure
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Procedure", inversedBy="procedures", cascade={"persist"},fetch="EXTRA_LAZY")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_procedure", referencedColumnName="id", onDelete="CASCADE")
     * })
     */
    protected $procedure;


    /**
     * @var status
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    protected $status = self::STATUS_PENDIND;

    /**
     * @var \DateTime
     * @ORM\Column(name="execussion_date", type="datetime", nullable=true)
     *
     */
    protected $date;



    public function __construct(Plan $plan = null, Procedure $procedure = null)
    {
        $this->plan = $plan;
        $this->procedure = $procedure;
        $this->status = self::STATUS_PENDIND;

    }


    /**
     * @return Plan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * @param Plan $plan
     * @return PlanProcedure
     */
    public function setPlan(Plan $plan)
    {
        $this->plan = $plan;
        return $this;
    }

    /**
     * @return Procedure
     */
    public function getProcedure()
    {
        return $this->procedure;
    }

    /**
     * @param Procedure $procedure
     * @return PlanProcedure
     */
    public function setProcedure(Procedure $procedure)
    {
        $this->procedure = $procedure;
        return $this;
    }

    /**
     * @return status
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param status $status
     * @return PlanProcedure
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     * @return PlanProcedure
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }


}
