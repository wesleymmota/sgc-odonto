<?php

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use Doctrine\ORM\Mapping as ORM;

/**
 * Uf
 *
 * @ORM\Table(
 *     name="uf"
 * )
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class Uf
{
    use Base;
    use ArrayTrait;

    public function __toString()
    {
        return $this->uf;
    }

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="integer", unique=false)
     */
    protected $code;


    /**
     * @var string
     *
     * @ORM\Column(name="uf", type="string", length=20, nullable=false, unique=true)
     */
    protected $uf;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=false, unique=false)
     */
    protected $name;

    /**
     * @return string
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param string $uf
     * @return Uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Uf
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Uf
     */
    public function setCode(string $code): Uf
    {
        $this->code = $code;
        return $this;
    }
}
