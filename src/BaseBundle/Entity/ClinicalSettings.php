<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 07/01/18
 * Time: 23:37
 */

namespace BaseBundle\Entity;

use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Clinica
 *
 * @ORM\Table(
 *     name="clinical_settings"
 * )
 * @ORM\Entity(repositoryClass="BaseBundle\Repository\ClinicalSettingsRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ClinicalSettings
{
    use BaseUUId;
    use ArrayTrait;


    public function __construct()
    {
        $this->port =  "80";
        $this->name =  "My Clinic";
        $this->domain =  "localhost";
        $this->locale = Clinic::LOCALE_PT_BR;
        $this->logo = null;
    }
    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", nullable=true)
     */
    protected $domain;

    /**
     * @var string
     *
     * @ORM\Column(name="port", type="string", nullable=true)
     */
    protected $port;

    /**
     * @var \BaseBundle\Entity\Assets
     *
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Assets", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fk_logo", referencedColumnName="id", onDelete="SET NULL")
     * })
     */
    protected $logo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", nullable=true)
     */
    protected $locale = 'pt_BR';

    /**
     * @ORM\OneToOne(targetEntity="BaseBundle\Entity\Clinic", mappedBy="settings",orphanRemoval=true))
     *
    **/
    protected $clinic;

    /**
     * @return mixed
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param mixed $domain
     * @return ClinicalSettings
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
        return $this;
    }

    /**
     * @return Assets
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param Assets $logo
     * @return ClinicalSettings
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return ClinicalSettings
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return ClinicalSettings
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getClinic()
    {
        return $this->clinic;
    }

    /**
     * @param mixed $clinic
     * @return ClinicalSettings
     */
    public function setClinic($clinic)
    {
        $this->clinic = $clinic;
        return $this;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     * @return ClinicalSettings
     */
    public function setPort($port)
    {
        $this->port = $port;
        return $this;
    }
}
