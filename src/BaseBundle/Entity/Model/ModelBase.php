<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 12/01/18
 * Time: 12:04
 */

namespace BaseBundle\Entity\Model;

class ModelBase
{
    public function __get($property)
    {
        if (property_exists($this, $property)) {
            return $this->$property;
        }

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $property .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE
        );
        return null;
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }

        return $this;
    }

    public function __call($propertyName, $arguments)
    {
        $prefix  = substr($propertyName, 0, 4);
        $varName = substr($propertyName, 4);

        if (isset($this->{$varName})) {
            $value = (isset($arguments[0])) ? $arguments[0] : null;
            return $this->processCall($prefix, $varName, $value);
        } else {
            $prefix  = substr($propertyName, 0, 3);
            $varName = lcfirst(substr($propertyName, 3));
            $value = (isset($arguments[0])) ? $arguments[0] : null;

            return $this->processCall($prefix, $varName, $value);
        }
    }

    private function processCall($prefix, $property, $value = null)
    {
        switch ($prefix) {
            case 'get':
                return $this->__get($property);
                break;

            case 'set':
//                if (count($property) != 1) {
//                    throw new \Exception("Setter for $property requires exactly one parameter.");
//                }

                $this->__set($property, $value);
                return $this;

            default:
                throw new \Exception("Property $property doesn't exist.");
                break;
        }
    }
}
