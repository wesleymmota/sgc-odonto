<?php

namespace BaseBundle\Entity;


use BaseBundle\Entity\Traits\ArrayTrait;
use BaseBundle\Entity\Traits\Base;
use BaseBundle\Entity\Traits\BaseUUId;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Plano
 *
 * @ORM\Table(
 *     name="tratament_plan_file"
 * )
 * @ORM\Entity()
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE", region="clinical_region")
 */
class PlanFile
{
    use BaseUUId;
    use ArrayTrait;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer")
     */
    private $size;

    /**
     * @var string
     *
     * @ORM\Column(name="extension", type="string", nullable=true)
     */
    protected $extension;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    protected $url;

    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="BaseBundle\Entity\Plan", inversedBy="files",cascade={"all"})
     * @ORM\JoinColumn(name="tratament_plan_files", referencedColumnName="id",onDelete="CASCADE")
     **/
    private $document;

    public function upload($class)
    {
        if (!$this->getFile() instanceof UploadedFile) {
            throw new \Exception("No such file or directory.");
        }

        $file = $this->getFile();
        $this->setSize($file->getClientSize());
        $this->setName($file->getClientOriginalName());
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->setExtension($file->guessExtension());
        $this->setDocument($class);
        $this->setPath($this->getUploadRootDir() . "/". $file->getClientOriginalName());
        $file->move($this->getUploadRootDir(), $file->getClientOriginalName());

        return $this;
    }

    /**
     * @param UploadedFile $uploadedFile
     */
    function __construct(UploadedFile $uploadedFile = null)
    {
        if(!is_null($uploadedFile)) {

            $this->setSize($uploadedFile->getClientSize());
            $this->setName($uploadedFile->getClientOriginalName());
            $this->createdAt = new \DateTime();
            $this->updatedAt = new \DateTime();
            $uploadedFile->move($this->getUploadRootDir(), $path);
        }
    }

    public function getIcon()
    {
        $icon = "la-file-text-o";
        switch ($this->getExtension()) {
            case "pdf":
                $icon = "la la-file-pdf-o";
                break;
            case "png":
                $icon = "la la-file-image-o";
                break;
            case "jpg":
                $icon = "la la-file-image-o";
                break;
            case "jpeg":
                $icon = "la la-file-image-o";
                break;
        }

        return $icon;
    }

    /**
     * Set path
     *
     * @param string $path
     * @return File
     */
    public function setPath($path)
    {
        $this->path = $path;
        return $this;
    }
    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    /**
     * Set name
     *
     * @param string $name
     * @return File
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Set size
     *
     * @param integer $size
     * @return File
     */
    public function setSize($size)
    {
        $this->size = $size;
        return $this;
    }
    /**
     * Get size
     *
     * @return integer
     */
    public function getSize()
    {
        return $this->size;
    }
    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }
    /**
     * @param mixed $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }
    /**
     * @return mixed
     */
    public function getDocument()
    {
        return $this->document;
    }
    /**
     * @param mixed $document
     */
    public function setDocument($document)
    {
        $this->document = $document;
    }
    /**
     * @return string
     */
    protected function getUploadRootDir()
    {
        return __DIR__.'/../../../public/uploads/'.$this->getUploadDir();
    }
    /**
     * @return string
     */
    public function getUploadDir()
    {
        return "tratament_files/" . $this->getDocument()->getId();
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return Asset
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension;
    }

    /**
     * @param string $extension
     * @return Asset
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
        return $this;
    }
}
