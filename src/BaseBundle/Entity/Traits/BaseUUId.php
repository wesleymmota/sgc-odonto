<?php


namespace BaseBundle\Entity\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use PascalDeVink\ShortUuid\ShortUuid;

/**
 * ORM\@MappedSuperclass
 */
trait BaseUUId
{
    /**
     * @ORM\Column(type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;
    /**
     * @return int
     */
    public function getId()
    {
//        $uuid = Uuid::fromString($this->id);
//        $shortUuid = new ShortUuid();
//
//        return $shortUuid->encode($uuid);
        return $this->id;
    }

    /**
     * @return int
     */
    public function getShortId()
    {
        $uuid = Uuid::fromString($this->id);
        $shortUuid = new ShortUuid();

        return $shortUuid->encode($uuid);
    }

    /**
     * @param int $id
     *
     * @return IdTrait
     */
    public function setShortId($uuid)
    {

        $shortUuid = new ShortUuid();
        $this->id = $shortUuid->decode($uuid);
        return $this;
    }
    /**
     * @param int $id
     *
     * @return IdTrait
     */
    public function setId($id, $short = true)
    {
        $this->id = $id;
//
//        if ($short) {
//            $shortUuid = new ShortUuid();
//            $this->id = $shortUuid->decode($id);
//        }
        return $this;
    }
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     *
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    protected $updatedAt;


    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * Sets createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Sets updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
