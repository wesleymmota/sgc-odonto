<?php


namespace BaseBundle\Entity\Traits;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use PascalDeVink\ShortUuid\ShortUuid;

/**
 * ORM\@MappedSuperclass
 */
trait UserUUId
{
    /**
     * @ORM\Column(type="guid",name="uuid")
     */
    protected $uuid;
    /**
     * @return int
     */
    public function getUuid()
    {
        $uuid = Uuid::fromString($this->uuid);
        $shortUuid = new ShortUuid();

        return $shortUuid->encode($uuid);
    }
    /**
     * @param int $id
     *
     * @return IdTrait
     */
    public function setUuid($uuid, $short = true)
    {
        $this->uuid = $uuid;

        if ($short) {
            $shortUuid = new ShortUuid();
            $this->id = $shortUuid->decode($uuid);
        }
        return $this;
    }
    public function __clone()
    {
        $this->id = null;
    }

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     *
     */
    protected $createdAt;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(type="datetime", options={"default"="CURRENT_TIMESTAMP"}, nullable=true)
     */
    protected $updatedAt;


    /**
     * Returns createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
    /**
     * Sets createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return $this
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }
    /**
     * Returns updatedAt.
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
    /**
     * Sets updatedAt.
     *
     * @param \DateTime $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }
}
