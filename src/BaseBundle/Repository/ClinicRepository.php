<?php


namespace BaseBundle\Repository;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use Doctrine\ORM\Query\Expr\OrderBy;

class ClinicRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $cacheId = "filter_";
        foreach (array_keys($parameters) as $parameter) {
            $cacheId .= serialize($parameter);
        }

        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(Clinic::class, 'e')
        ;
        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {


            $qb->andWhere(('e.id LIKE :id'));
            $qb->orWhere(('e.razao LIKE :razao'));
            $qb->orWhere(('e.fantasia LIKE :fantasia'));

            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('razao', "%" .$parameters['filter']."%");
            $qb->setParameter('fantasia', "%" .$parameters['filter']."%");
        }

        if (!empty($parameters['sortField']) and array_key_exists('sortField', $parameters)) {
            switch ($parameters['sortField']) {
                case 'createdAt':
                    $qb->addOrderBy('e.createdAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'updatedAt':
                    $qb->addOrderBy('e.updatedAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'razao':
                    $qb->addOrderBy('e.razao', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'fantasia':
                    $qb->addOrderBy('e.fantasia', $parameters['sortDirection']);
                    break;
                case 'enabled':
                    $qb->addOrderBy('e.enabled', $parameters['sortDirection']);
                    break;

                case 'id':
                    $qb->addOrderBy('e.id', $parameters['sortDirection']);
                    break;
            }
        }
        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getResult()
            : $qb;
    }

    public function getDoctorFromClinic($clinic, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
            ->innerJoin('e.user', 'u')
        ;
        $qb->andWhere('e.clinic = :clinic')
            ->andWhere('u.enabled = 1')
            ->setParameters(
                array(
                    'clinic' => $clinic instanceof Clinic ? $clinic->getId() : $clinic
                )
            );
        $qb->where($qb->expr()->orX(
            $qb->expr()->like('u.roles', ':roles1'),
            $qb->expr()->like('u.roles', ':roles2'),
            $qb->expr()->like('u.roles', ':roles3')
        ))
            ->setParameter('roles1', '%ROLE_ADMIN%')
            ->setParameter('roles2', '%ROLE_DOCTOR%')
            ->setParameter('roles3', '%ROLE_SUPER_ADMIN"%');

        $cacheId = sprintf(
            "getDoctorFromClinic__%s",
            $clinic instanceof Clinic ? $clinic->getId() : $clinic
        );

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getResult()
            : $qb;
    }

    public function getUserClinic($user, $clinic, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
        ;
        $qb->andWhere('e.user = :user')
        ->andWhere('e.clinic = :clinic')
        // ->andWhere('r.ativo = 1')
        ->setParameters(
            array(
                'user' => $user instanceof User ? $user->getId() : $user,
                'clinic' => $clinic instanceof Clinic ? $clinic->getId() : $clinic,
            )
        );


        $cacheId = sprintf(
            "getAllClinics_%s_%s",
            $user instanceof User ? $user->getId() : $user,
            $clinic instanceof Clinic ? $clinic->getId() : $clinic
        );

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getOneOrNullResult()
            : $qb;
    }


    public function getUserClinicByEmail($email, $clinic, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
            ->innerJoin('e.user', 'user')
        ;
        $qb->andWhere('user.email = :email')
            ->andWhere('e.clinic = :clinic')
            // ->andWhere('r.ativo = 1')
            ->setParameters(
                array(
                    'email' =>$email,
                    'clinic' => $clinic instanceof Clinic ? $clinic->getId() : $clinic,
                )
            )
            ->setMaxResults(1);
        ;

        return $execute === true ? $qb->getQuery()
            ->getOneOrNullResult()
            : $qb;
    }

    public function getUserClinicByUserOrEmail(User $user, $clinic, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
            ->innerJoin('e.user', 'user')
        ;
        $qb->where('user.username = :username OR user.email = :email')
        ->andWhere('e.clinic = :clinic')
        // ->andWhere('r.ativo = 1')
        ->setParameters(
            array(
                'email' => $user->getEmail(),
                'username' =>$user->getUsername(),
                'clinic' => $clinic instanceof Clinic ? $clinic->getId() : $clinic,
            )
        )
        ->setMaxResults(1);
        ;

        return $execute === true ? $qb->getQuery()
            ->getOneOrNullResult()
            : $qb;
    }
    public function getUserClinics($user, $execute = true)
    {

        $cacheId = sprintf(
            "getUserClinics_%s_%s_search_",
            strtolower(str_replace("\\", "_slash_", $this->getClassName())),
            $execute
        );
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
            ->innerJoin('e.clinic', 'clinic')
            ->innerJoin('e.user', 'user')
        ;
        $qb->andWhere('user.id = :user')
            ->setParameters(
                array(
                    'user' => $user instanceof User ? $user->getId() : $user,
                )
            );

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getResult()
            : $qb;
    }


    public function getClients($clinic, $parameters = null, $execute = true)
    {
        $cacheId = sprintf(
            "getClients_%s_%s",
            strtolower(str_replace("\\", "_slash_", $this->getClassName())),
            $clinic instanceof Clinic ? $clinic->getId() : $clinic
        );

        foreach (array_keys($parameters) as $parameter) {
            $cacheId .= serialize($parameter);
        }
        $qb =  $this->getEntityManager()->createQueryBuilder()
            ->select('user')
            ->from('\BaseBundle\Entity\User', 'user');
        $qb
            ->join('\BaseBundle\Entity\UserClinical', 'userClinical', \Doctrine\ORM\Query\Expr\Join::WITH, 'user = userClinical.user')
            ->join('\BaseBundle\Entity\Clinic', 'clinic', \Doctrine\ORM\Query\Expr\Join::WITH, 'clinic = userClinical.clinic')
        ;

        if (!empty($parameters['paciente']) and $parameters['paciente'] == true) {

            $qb->andWhere('userClinical.client = TRUE');

        }
        $qb->andWhere('clinic.id = :clinic')
            ->setParameter('clinic', $clinic instanceof Clinic ? $clinic->getId() : $clinic);



        if ((!empty($parameters['paciente']) and $parameters['paciente'] == true) or (!empty($parameters['employee']) and $parameters['employee'] == true)) {

           $qb->andWhere('userClinical.client = TRUE');
        }else{
            $qb->andWhere('userClinical.client = FALSE');
        }


        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {

            $qb->andWhere(('user.id LIKE :id'));
            $qb->orWhere(('user.name LIKE :name'));
            $qb->orWhere(('user.username LIKE :username'));

            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('name', "%" .$parameters['filter']."%");
            $qb->setParameter('username', "%" .$parameters['filter']."%");
        }

        if (array_key_exists('sortField', $parameters) and !empty($parameters['sortField'])) {

            if($parameters['sortField'] ==  "roles"){
                $qb
                    ->addOrderBy("userClinical.client", "${parameters['sortDirection']}")

                ;
            }
            if ($this->getEntityManager()->getRepository(User::class)->getClassMetadata()->hasField($parameters['sortField'])) {

                $qb
                    ->addOrderBy("user.${parameters['sortField']}", "${parameters['sortDirection']}")

                    ;
            }
//            else{
//
//                $qb->addOrderBy("userClinical.client", "DESC")
//                    ->addOrderBy("userClinical.owner", "DESC")
//                    ->addOrderBy("userClinical.employee", "DESC");
//            }
        }
//        $qb->groupBy('e.user');

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getResult()
            : $qb;
    }

    public function findClinicLastAccess($user, $execute = true)
    {
        $cacheId = "findLastClinicInSession_";
        $cacheId = md5($cacheId);
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(UserClinical::class, 'e')
            ->andWhere('e.user = :user')
            ->andWhere('e.lastAccess = :lastAccess')
            ->setParameters(
                [
                    'user' => $user instanceof User ? $user->getId() : $user,
                    'lastAccess' => TRUE
                ]
            )
            ->setMaxResults(1)
        ;

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getOneOrNullResult()
            : $qb;
    }
}
