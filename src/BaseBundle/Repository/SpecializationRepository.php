<?php


namespace BaseBundle\Repository;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Specialization;
use Doctrine\ORM\Query\Expr\OrderBy;

class SpecializationRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(Specialization::class, 'e')
        ;
        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {


            $qb->andWhere(('e.id LIKE :id'));
            $qb->orWhere(('e.name LIKE :name'));
            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('name', "%" .$parameters['filter']."%");
            // }
            //$qb->setParameter(1, 1);
        }

        if (!empty($parameters['sortField']) and array_key_exists('sortField', $parameters)) {
            switch ($parameters['sortField']) {
                case 'createdAt':
                    $qb->addOrderBy('e.createdAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'updatedAt':
                    $qb->addOrderBy('e.updatedAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'name':
                    $qb->addOrderBy('e.name', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'id':
                    $qb->addOrderBy('e.id', $parameters['sortDirection']);
                    break;
            }
        }
        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }
}
