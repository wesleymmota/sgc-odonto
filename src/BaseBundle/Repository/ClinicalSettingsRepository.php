<?php


namespace BaseBundle\Repository;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use Doctrine\ORM\Query\Expr\OrderBy;

class ClinicalSettingsRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(ClinicalSettings::class, 'e')
        ;
        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {


            $qb->andWhere(('e.id LIKE :id'));
            $qb->orWhere(('e.razao LIKE :razao'));
            $qb->orWhere(('e.fantasia LIKE :fantasia'));

            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('razao', "%" .$parameters['filter']."%");
            $qb->setParameter('fantasia', "%" .$parameters['filter']."%");
            // }
            //$qb->setParameter(1, 1);
        }

        if (!empty($parameters['sortField']) and array_key_exists('sortField', $parameters)) {
            switch ($parameters['sortField']) {
                case 'razao':
                    $qb->addOrderBy('e.razao', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'fantasia':
                    $qb->addOrderBy('e.fantasia', $parameters['sortDirection']);
                    break;
                case 'id':
                    $qb->addOrderBy('e.id', $parameters['sortDirection']);
                    break;
            }
        }
        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }

    public function getSettings($domain, $port = null)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')->distinct()
            ->from(ClinicalSettings::class, 'e')
        ;

        $qb->andWhere(('e.domain LIKE :domain'));
        $qb->setParameter('domain', "%" .$domain."%");

        if (!is_null($port) && $port <> 80) {
            $qb->andWhere('e.port = :port');
            $qb->setParameter('port', $port);
        }
        $qb->setMaxResults(1);


        return $qb->getQuery()->getOneOrNullResult();
    }

    public function getSettginsByClinic($clinic)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')->distinct()
            ->from(ClinicalSettings::class, 'e')
            ->innerJoin('e.clinic', 'clinic')
        ;
        $qb->andWhere('clinic.id = :clinic')
            ->setParameters(
                array(
                    'clinic' => $clinic instanceof Clinic ? $clinic->getId() : $clinic,
                )
            );

        $qb->setMaxResults(1);


        return $qb->getQuery()->getOneOrNullResult();
    }
}
