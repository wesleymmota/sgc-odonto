<?php


namespace BaseBundle\Repository;

use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use Doctrine\ORM\Query\Expr\OrderBy;

class AgreementRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(Agreement::class, 'e')
            ->innerJoin('e.clinic', 'clinic')
        ;
        if(!empty($parameters['clinic'])) {
            $qb->andWhere('clinic.id = :clinic')
                ->setParameters(
                    [
                        'clinic' => $parameters['clinic'] instanceof Clinic ? $parameters['clinic']->getId() : $parameters['clinic'],
                    ]
                );
        }

        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {


            $qb->andWhere(('e.id LIKE :id'));
            $qb->orWhere(('e.fantasia LIKE :fantasia'));
            $qb->orWhere(('e.razao LIKE :razao'));
            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('fantasia', "%" .$parameters['filter']."%");
            $qb->setParameter('razao', "%" .$parameters['filter']."%");
            // }
            //$qb->setParameter(1, 1);
        }

        if (array_key_exists('sortField', $parameters) and !empty($parameters['sortField'])) {
           // $arraySort = $this->sanitizeDirectionFields($parameters['sortField']);

            //foreach ($arraySort as $sortConfig) {
            if ($this->getClassMetadata()->hasField($parameters['sortField'])) {
                $qb->addOrderBy("e.${parameters['sortField']}", "${parameters['sortDirection']}");
            }
            //}
        }
        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }


    public function getBudgetByAgreement($agrement, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("b");
        $qb->select('b')
            ->from(Budget::class, 'b')
            ->innerJoin('b.agreement', 'agreement')
        ;
        $qb->andWhere('agreement.id = :agreement')
            ->setParameters(
                array(

                    'agreement' => $agrement instanceof Agreement ? $agrement->getId() : $agrement,
                )
            )
            ->setMaxResults(1);
        ;

        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }
}
