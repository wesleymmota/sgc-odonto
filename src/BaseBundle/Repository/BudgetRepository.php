<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 30/04/18
 * Time: 15:24
 */

namespace BaseBundle\Repository;

use BaseBundle\Entity\Budget;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\Plan;
use BaseBundle\Entity\PlanProcedure;
use BaseBundle\Entity\Procedure;
use Doctrine\ORM\Query\Expr\OrderBy;

class BudgetRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(Budget::class, 'e')
            ->innerJoin('e.plan', 'plan')
            ->innerJoin('e.createdBy', 'createdBy')
        ;

        if(!empty($parameters['user'])) {
            $qb->andWhere('e.client = :client')
                ->setParameter( 'client', $parameters['user'] instanceof User ? $parameters['user']->getId() : $parameters['user']);
        }

        if(!empty($parameters['clinic'])) {
            $qb->andWhere('e.clinic = :clinic')
                ->setParameter( 'clinic', $parameters['clinic'] instanceof Clinic ? $parameters['clinic']->getId() : $parameters['clinic']);
        }

        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {
            $field = $parameters['filter'];
            $qb->andWhere(('e.id LIKE :filter'));
            $qb->orWhere(('e.name LIKE :filter'));

            $qb->setParameter('filter', "%" .$parameters['filter']."%");
            // }
            //$qb->setParameter(1, 1);
        }

        if (array_key_exists('sortField', $parameters) and !empty($parameters['sortField'])) {
            if ($parameters['sortField'] == 'client') {

                $qb->addOrderBy("e.client", "${parameters['sortDirection']}");
            }
            if ($this->getClassMetadata()->hasField($parameters['sortField'])) {
                $qb->addOrderBy("e.${parameters['sortField']}", "${parameters['sortDirection']}");
            }
        }
        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }

    public function getDentalRecordQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("p");
        $qb->select('p')
            ->from(Plan::class, 'p')
            ->innerJoin('p.clinic', 'c')
            ->innerJoin('p.budget', 'b')

        ;
        $qb->andWhere("b.status = :status")
            ->andWhere('c.id = :clinic')
            ->setParameters(
                [
                    'clinic' => $parameters['clinic'] instanceof Clinic ? $parameters['clinic']->getId() : $parameters['clinic'],
                    'status' => Budget::STATUS_APPROVED
                ]
            )
           ;
        if(!empty($parameters['user'])) {
            $qb->andWhere('b.client = :client')
                ->setParameter( 'client', $parameters['user'] instanceof User ? $parameters['user']->getId() : $parameters['user']);
        }
        if (array_key_exists('sortField', $parameters) and !empty($parameters['sortField'])) {


            if ($this->getEntityManager()->getRepository(Budget::class)->getClassMetadata()->hasField($parameters['sortField'])) {

                $qb->addOrderBy("b.${parameters['sortField']}", "${parameters['sortDirection']}");
            }

            if ($this->getEntityManager()->getRepository(Plan::class)->getClassMetadata()->hasField($parameters['sortField'])) {

                $qb->addOrderBy("p.${parameters['sortField']}", "${parameters['sortDirection']}");
            }
        }

        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }


    public function getProcedureFromPlan($plan, $procedure, $execute = true)
    {

        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(PlanProcedure::class, 'e')
            ->andWhere('e.plan = :plan')
            ->andWhere('e.procedure = :procedure')
            ->setParameters(
                [
                    'plan' => $plan instanceof Plan ? $plan->getId() : $plan,
                    'procedure' => $procedure instanceof Procedure ? $procedure->getId() : $procedure,
                ]
            )
            ->setMaxResults(1)
        ;

        return $execute === true ? $qb->getQuery()
            ->getOneOrNullResult()
            : $qb;
    }
}
