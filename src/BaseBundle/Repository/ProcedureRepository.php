<?php


namespace BaseBundle\Repository;

use BaseBundle\Entity\Procedure;
use Doctrine\ORM\Query\Expr\OrderBy;

class ProcedureRepository extends AbstractRepository
{
    public function getQueryBuilder($parameters = null, $execute = true)
    {
        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(Procedure::class, 'e')
        ;
        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            //foreach ($parameters['filter'] as $key => $value) {


            $qb->andWhere(('e.id LIKE :id'));
            $qb->orWhere(('e.description LIKE :description'));
            $qb->setParameter('id', "%" .$parameters['filter']."%");
            $qb->setParameter('description', "%" .$parameters['filter']."%");
            // }
            //$qb->setParameter(1, 1);
        }

        if (!empty($parameters['sortField']) and array_key_exists('sortField', $parameters)) {
            switch ($parameters['sortField']) {
                case 'createdAt':
                    $qb->addOrderBy('e.createdAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'updatedAt':
                    $qb->addOrderBy('e.updatedAt', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'description':
                    $qb->addOrderBy('e.description', $parameters['sortDirection']);
                    //$qb->add('orderBy', new OrderBy('e.razao', $parameters['sortDirection']));
                    break;
                case 'id':
                    $qb->addOrderBy('e.id', $parameters['sortDirection']);
                    break;
            }
        }
        return $execute === true ? $qb->getQuery()
            ->getResult()
            : $qb;
    }
}
