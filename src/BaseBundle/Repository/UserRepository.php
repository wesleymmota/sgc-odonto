<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace BaseBundle\Repository;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;

/**
 * This custom Doctrine repository is empty because so far we don't need any custom
 * method to query for application user information. But it's always a good practice
 * to define a custom repository that will be used when the application grows.
 *
 * See https://symfony.com/doc/current/book/doctrine.html#custom-repository-classes
 *
 * @author Ryan Weaver <weaverryan@gmail.com>
 * @author Javier Eguiluz <javier.eguiluz@gmail.com>
 */
class UserRepository extends AbstractRepository
{

    public function getQueryBuilder($parameters = null, $execute = true)
    {

        $cacheId = sprintf(
            "getQueryBuilder_%s_%s_search_",
            strtolower(str_replace("\\", "_slash_", $this->getClassName())),
            $execute
        );

        $qb = $this->getEntityManager()->createQueryBuilder("e");
        $qb->select('e')
            ->from(User::class, 'e')
        ;


        if (!empty($parameters['filter']) and array_key_exists('filter', $parameters)) {
            foreach ($parameters['filter'] as $key => $value) {

                $cacheId .= sprintf(
                    "%s_%s",
                    strtolower($key),
                    strtolower($value)
                );
            }

            $field = $parameters['filter'];
            $qb->andWhere(('e.id LIKE :filter'));
            $qb->orWhere(('e.name LIKE :filter'));

            $qb->setParameter('filter', "%" .$parameters['filter']."%");

        }

        if (array_key_exists('sortField', $parameters) and !empty($parameters['sortField'])) {

                $cacheId .= sprintf(
                    "%s_%s",
                    strtolower($parameters['sortField']),
                    strtolower($parameters['sortDirection'])
                );
            if ($this->getClassMetadata()->hasField($parameters['sortField'])) {
                $qb->addOrderBy("e.${parameters['sortField']}", "${parameters['sortDirection']}");
            }
        }

        $qb
            ->andWhere($qb->expr()->orX(
                $qb->expr()->like('e.roles', ':roles1'),
                $qb->expr()->like('e.roles', ':roles2'),
                $qb->expr()->like('e.roles', ':roles3'),
                $qb->expr()->like('e.roles', ':roles4')
            ))
            ->setParameters(
                [
                    'roles1' => '%ROLE_DOCTOR%',
                    'roles2' => '%ROLE_EMPLOYEE%',
                    'roles3' => '%ROLE_ADMIN%',
                    'roles4' => '%ROLE_CLIENT%',

                ]
            );

        return $execute === true ? $qb->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getResult()
            : $qb;
    }


    public function loadUserByUsername($username)
    {
        $cacheId = "active_user_".$username;
        return $this->createQueryBuilder('u')
            ->where('u.username = :username OR u.email = :email')
            ->setParameter('username', $username)
            ->setParameter('email', $username)
            ->getQuery()
            ->useQueryCache(true)
            ->useResultCache(true, 3600, $cacheId)
            ->getOneOrNullResult() ;
    }

    public function getUserByEmail($email, $execute = true)
    {
        $qb = $this->createQueryBuilder('u')
            ->where('u.email = :email')
            ->setParameter('email', $email)
            ->setMaxResults(1)
        ;

        return $execute === true ? $qb->getQuery()
            ->getOneOrNullResult()
            : $qb;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf(
                'Instances of "%s" are not supported.',
                $class
            ));
        }

        if (!$refreshedUser = $this->find($user->getId())) {
            throw new UsernameNotFoundException(sprintf('User with id %s not found', json_encode($user->getId())));
        }

        return $refreshedUser;
    }

    public function supportsClass($class)
    {
        return $this->getEntityName() === $class
            || is_subclass_of($class, $this->getEntityName());
    }


    public function findUsersWithAddressesIn(Clinic $clinic)
    {
        $subQuery =  $this->getEntityManager()->createQuery(
            "SELECT user
            FROM BaseBundle:User user
            INNER JOIN user.users userClinical
            INNER JOIN userClinical.clinic clinic
            WHERE clinic.id = :clinic"
        );



        $queryBuilder = $this->createQueryBuilder('u');
        $queryBuilder
            ->where($queryBuilder->expr()->notIn('u.id', $subQuery->getDQL()))
            ->andWhere($queryBuilder->expr()->orX(
//                $queryBuilder->expr()->eq('u.enabled', 1),
                $queryBuilder->expr()->like('u.roles', ':roles1'),
                $queryBuilder->expr()->like('u.roles', ':roles2')
            ))
            ->andWhere('u.enabled = :state')
            ->setParameters(
            [
                'clinic' => $clinic instanceof Clinic ? $clinic->getId() :  $clinic,
                'roles1' => '%ROLE_DOCTOR%',
                'roles2' => '%ROLE_EMPLOYEE%',
                    'state' => TRUE
            ]
        );
//        dump( $queryBuilder->getQuery()->getDQL());
//        dump( $queryBuilder->getQuery()->getParameters());
//        dump( $queryBuilder->getQuery()->getResult());die();
        return $queryBuilder;
    }
}


