<?php

namespace BaseBundle\Google\Storage;

use Google\Cloud\Storage\StorageClient;
use Google\Cloud\Storage\StorageObject;
use Symfony\Component\Filesystem\Filesystem;

class StorageManager
{
    private $storage;
    private $bucket;

    public function __construct($bucket = null )
    {
        if (! is_null($bucket)) {
            $this->storage = new StorageClient();

            $this->bucket = $this->storage->bucket($bucket);
        }
    }

    public function uploadFile($file, array $options = null, $permission = 'publicRead')
    {
        if (! file_exists($file)) {
            throw new \Exception("No such file or directory.");
        }

        $filename = basename($file);
        $filename = substr($filename, 0, 50);
        $options = array_merge($options, ['predefinedAcl' => $permission]);
        $object = $this->bucket->upload(
            fopen($file, 'r'),
            $options
        );

        if (is_dir(dirname($file))) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator(dirname($file))) as $filename) {
                if ($filename->isDir()) {
                    continue;
                }
                unlink($filename);
            }
            (new Filesystem)->remove(dirname($file));
        }

        $link = $object->info()['mediaLink'];

        return $link;
    }

    public function downlodFile($fileName, $fileDestination, $bucket = null)
    {
        if (! is_null($bucket)) {
            $this->bucket = $this->storage->bucket($bucket);
        }

        $object = $this->bucket->object($fileName);
        $object->downloadToFile($fileDestination);
    }


    /**
     * Deletes a file.
     *
     * @param string $url A URL returned by a call to StorageFile.
     */
    public function deleteFile($url)
    {
        $path_components = explode('/', parse_url($url, PHP_URL_PATH));
        $name = urldecode($path_components[count($path_components) - 1]);

        $object = $this->bucket->object($name);
        if ($object->exists()) {
            $object->delete();
        }
    }
}
