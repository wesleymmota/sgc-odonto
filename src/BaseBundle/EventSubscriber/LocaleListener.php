<?php
namespace BaseBundle\EventSubscriber;

use BaseBundle\Manager\SystemManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class LocaleListener implements EventSubscriberInterface
{
    private $defaultLocale;
    public function __construct($defaultLocale = 'en')
    {
        $this->defaultLocale    = $defaultLocale;
    }


    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $session  = $request->getSession();


        if (!$request->hasPreviousSession()) {
            return;
        }


        if ($locale = $request->attributes->get('clinicId')) {

            $session->set('clinicId', $request->attributes->get('clinicId'));
        }


        if ($locale = $request->attributes->get('_locale')) {
            $session->set('_locale', $locale);
            $session->set('app.locale', $locale);
        } else {
            // if no explicit locale has been set on this request, use one from the session
            $request->setLocale($request->getSession()->get('_locale', $this->defaultLocale));
        }
    }
    public static function getSubscribedEvents()
    {
        return array(
            // must be registered before the default Locale listener
            KernelEvents::REQUEST => array(array('onKernelRequest', 17)),
        );
    }
}
