<?php

namespace BaseBundle\Provider;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Entity\User;
use BaseBundle\Manager\SystemManager;
use BaseBundle\Utils\Session;
use BaseBundle\Utils\Utils;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use League\Uri\Schemes\Http as HttpUri;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class UserProvider implements UserProviderInterface
{
    /**
     * @var $repository \BaseBundle\Repository\UserRepository
     */
    private $repository;
    private $entityManager;
    private $encoderFactory;
    private $systemSettings;

    /**
     * @var $container Container
     */
    private $container;


    public function __construct($entityManager, $encoderFactory, $container, SystemManager $systemSettings)
    {
        $this->encoderFactory = $encoderFactory;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(User::class);
        $this->container = $container;
        $this->systemSettings = $systemSettings;
    }

    public function loadUserByUsername($username)
    {
        if (!$user = $this->repository->loadUserByUsername($username)) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }

        $session = $this->container->get('session');

        $this->systemSettings->loadDefaultConfiguration();

        if ($session->has(SystemManager::APP_SETTINGS)) {

            $settings = $session->get(SystemManager::APP_SETTINGS);
            $this->systemSettings->persistSettingsInSession($settings);

        } else {

            if ($user->hasRole(User::ROLE_DOCTOR) or $user->hasRole(User::ROLE_EMPLOYEE)) {
                $this->systemSettings->checkUserSession($user);
            }
        }

        return $user;
    }

    public function refreshUser(UserInterface $user)
    {
        $class = get_class($user);

        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf(
                'Instances of "%s" are not supported.',
                $class
            ));
        }

        if (!$refreshedUser = $this->loadUserByUsername($user->getUsername())) {
            throw new UsernameNotFoundException(sprintf('User %s not found', $user->getUsername()));
        }

        return $refreshedUser;
    }

    public function supportsClass($class)
    {
        return User::class === $class
            || is_subclass_of($class, User::class);
    }
}
