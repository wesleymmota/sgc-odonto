<?php

namespace BaseBundle\Controller;

use AdminBundle\Form\ChangePhotoType;
use AdminBundle\Form\ProfileFormType;
use AdminBundle\Manager\UserManager;
use AdminBundle\Model\Profile;
use AdminBundle\Model\UserModel;
use BaseBundle\Controller\BaseController;
use BaseBundle\Entity\Assets;
use BaseBundle\Entity\User;
use FOS\UserBundle\Event\FormEvent;
use Gregwar\Image\Image;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGenerator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\RouterInterface;

/**
 * Shared controller to change use informations
 * - Admin
 * - Doctors
 * - Employee
 * - Clients
 * @Route("/profile")
 */
class ProfileController extends BaseController
{
    /**
    * @Route("/", name="admin_area_profile")
    * @Template("admin/user/profile.html.twig")
    */
    public function profile(UserManager $userManager)
    {

        if (!$this->getUser()) {
            $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            return $this->redirectToRoute("security_logout");
        }

        $userLogged = $this->getUser();
        $user = $userManager->fetch($userLogged->getId());

        $routeName = "admin_area_profile";


        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.profile',
                'route' => $this->getUrl($routeName),
                'items' => []
            ],
            'user' => $user
        ];
    }


    /**
     * @Route("/edit", name="user_profile_edit")
     * @Template("admin/user/edit-profile.html.twig")
     */
    public function edit(Request $request, UserManager $userManager)
    {
        if (!$this->getUser()) {
            $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            return $this->redirectToRoute("security_logout");
        }

        $userLogged = $this->getUser();
        $user = $userManager->fetch($userLogged->getId());

        $profile = new UserModel($user);


        $form = $this->createForm(ProfileFormType::class, $profile, [
            'action' => $this->generateUrl('user_profile_edit'),
            'method' => 'POST',


            'validation_groups' => [
                'update_profile'
                ],
        ]);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_area_profile');
        }
        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $profile = $form->getData();

//            dump($profile->getPersistUser()); die;

            $saved = $userManager->merge($profile->getPersistUser());


            if ($saved) {
                $request->getSession()->getFlashBag()->add('success', 'register.save.if.success');
                $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
                return $this->redirectToRoute('admin_area_profile');
            }

            $request->getSession()->getFlashBag()->add('danger', 'register.save.if.notsuccess');
        }

        $routeName = "admin_area_profile";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.profile',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'form.edit',
                        'route' => '',
                    ]

                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }

    /**
     * @Route("/change_photo", name="user_profile_change_photo")
     * @Template("admin/user/change_photo.html.twig")
     */
    public function changePhoto(Request $request, UserManager $userManager)
    {
        if (!$this->getUser()) {
            $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            return $this->redirectToRoute("security_logout");
        }

        $userLogged = $this->getUser();
        $user = $userManager->fetch($userLogged->getId());

        $profile_upload_directory = "profiles/" . $user->getId();

        $asset = $user->getPhoto();
        if (null === $user->getPhoto()) {
            $asset = new Assets();
            $user->setPhoto($asset);
        }
        $asset->setTargetDir($profile_upload_directory);

        $form = $this->createForm(ChangePhotoType::class, $asset);

        $form->handleRequest($request);
        if ($form->get('cancel')->isClicked()) {
            return $this->redirectToRoute('admin_area_profile');
        }


        if ($form->isSubmitted() && $form->isValid()) {

            /** @var /Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $assetFile = $asset->upload('BaseBundle::User');
            $storage = $this->get('base.google_storage');

            // Upload an object in a resumable fashion while setting a new name for
            // the object and including the content language.
//            $options = [
//                 'resumable' => true,
//                 'name' => $profile_upload_directory . "/" . basename($assetFile),
//                 'metadata' => [
//                    'user_id' => $user->getId()
//                ]
//             ];

            //$fileName = $storage->uploadFile($assetFile, $options);
            $image100x100= $profile_upload_directory . "/100x100-" . basename($assetFile);
            Image::open($assetFile)
                ->cropResize(200, 200)
                ->zoomCrop(250, 250)
                ->save($image100x100);

            $options = [
                'resumable' => true,
                'name' => $image100x100,
                'metadata' => [
                    'user_id' => $user->getId()
                ]
            ];

            if ($user->getPhoto()->getUrl() !== null) {
                $storage->deleteFile($user->getPhoto()->getUrl());
            }

            $fileName = $storage->uploadFile($image100x100, $options);

            $asset->setUrl($fileName);
            $asset->setUpdatedAt(new \DateTime());

            $user->setPhoto($asset);

            $userManager->merge($user);

            if ($this->getUser()->getId() === $user->getId()) {
                $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
            }

            return $this->redirect($this->generateUrl('admin_area_profile'));
        }

        $routeName = "admin_area_profile";



        return  [
            'breadcumbs' => [
                'name' => 'breadcumb.profile',
                'route' =>  $this->getUrl($routeName),
                'items' => [
                    'item1' => [
                        'name' => 'form.change_photo',
                        'route' => '',
                    ]

                ]
            ],
            'form' =>  $form->createView(),
            'user' => $user
        ];
    }
}
