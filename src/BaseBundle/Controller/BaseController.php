<?php
/**
 * Created by PhpStorm.
 * User: diego
 * Date: 21/12/17
 * Time: 19:57
 */

namespace BaseBundle\Controller;

use Psr\Http\Message\ServerRequestInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\ControllerTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller implements ContainerAwareInterface
{

    const RETURN_TO_ROUTE='return_to_route';
    /**
     * Gets a container configuration parameter by its name.
     *
     * @return mixed
     *
     * @final
     */
    protected function getParameter(string $name)
    {
        return $this->container->getParameter($name);
    }
    /**
     * @return \SimpleThings\EntityAudit\AuditReader
     */
    protected function getAuditReader()
    {
        return $this->get('simplethings_entityaudit.reader');
    }

    protected function saveReturnPage(Request $request, $routeName)
    {

        $response = new Response();
        $routeInfo = [
            'route' => $routeName,
            'params' => $request->query->all()
        ];
        $cookie = new Cookie(self::RETURN_TO_ROUTE, json_encode($routeInfo), time() + (365 * 24 * 60 * 60));  // Expires 1 years
        $response->headers->setCookie($cookie);
        $response->sendHeaders();
    }

    protected function getPostRoute(Request $request){

        $cookieRoute = $request->cookies->get(self::RETURN_TO_ROUTE);

        if (!empty($cookieRoute)) {
             return json_decode($cookieRoute,true);
        }

    }

    protected function getRevisions($entity, $id)
    {

        $auditReader = $this->getAuditReader();

        $revisions = $auditReader->findRevisions(
            $entity, $id
        );

        return $revisions;
    }

    protected function getRevision($entity, $id)
    {

        $auditReader = $this->getAuditReader();

        $revisions = $auditReader->find(
            $entity, $id, $this->getCurrentRevision($entity, $id)
        );

        return $revisions;
    }

    protected function getCurrentRevision($entity, $id)
    {

        $auditReader = $this->getAuditReader();

        $revisions = $auditReader->getCurrentRevision(
            $entity, $id
        );

        return $revisions;
    }

    public function getUrl($routeName, $params = [])
    {
        $gerador = $this->get('router')->generate(
            $routeName,
            $params,
            RouterInterface::ABSOLUTE_PATH // ABSOLUTE_URL, ABSOLUTE_PATH, RELATIVE_PATH, NETWORK_PATH
        );

        return $gerador;
    }

    private function getFilter()
    {
        //Get Doctrine Reader
        $reader = new Annotations\AnnotationReader();
        $reader->setEnableParsePhpImports(true);
        //Load AnnotationLoader
        $loader = new Mapping\Loader\AnnotationLoader($reader);
        $this->loader = $loader;
        //Get a MetadataFactory
        $metadataFactory = new Mapping\ClassMetadataFactory($loader);
        //Get a Filter
        $filter = new DMS\Filter\Filter($metadataFactory);

        return $filter;
    }



    // PSR-7 funcions

    /**
     * Returns a parameter by name.
     *
     * @param string $key     The key
     * @param mixed  $default The default value if the parameter key does not exist
     *
     * @return mixed
     */
    public function getParam(ServerRequestInterface $request, $key, $default = null)
    {
        return array_key_exists($key, $request->getQueryParams()) ? $request->getQueryParams()[$key] : $default;
    }

    /**
     * Returns an iterator for parameters.
     *
     * @return \ArrayIterator An \ArrayIterator instance
     */
    public function getIterator(ServerRequestInterface $request)
    {
        return new \ArrayIterator($request->getQueryParams());
    }

    /**
     * Returns the number of parameters.
     *
     * @return int The number of parameters
     */
    public function count(ServerRequestInterface $request)
    {
        return count($request->getQueryParams());
    }
}
