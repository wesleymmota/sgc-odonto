<?php

namespace BaseBundle\Controller;

use AdminBundle\Manager\ClinicManager;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Manager\SystemManager;
use BaseBundle\Utils\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        return $this->redirectToRoute("security_login");
    }

    /**
     * @Route("/switchlanguage/{_locale}", name="security_switchlanguage", requirements={"_locale": "en|pt_BR"})
     */
    public function switchlanguageAction(Request $request, $_locale = 'pt_BR', ClinicManager $manager, SystemManager $systemSettings)
    {
        $request->setLocale($_locale);
        $session  = $request->getSession();

        $userClinic = $manager->getRepository(Clinic::class)->findClinicLastAccess($this->getUser());
        $clinic = $userClinic->getClinic();
        if (!empty($clinic) && !empty($clinic->getSettings())) {

            /**
             * @var $settings \BaseBundle\Entity\ClinicalSettings
             */
            $settings = $clinic->getSettings();
            //Update settings
            $settings->setLocale($_locale);

            $this->getDoctrine()->getManager()->merge($settings);
            $this->getDoctrine()->getManager()->flush();
            $systemSettings->persistSettingsInSession($settings);

        }
        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * @Route("/change_clinics/{clinicId}", name="security_change_clinic")
     */
    public function changeclinicsAction(Request $request, $clinicId, SystemManager $systemSettings)
    {
        $session  = $this->get('session');
        $id = $session->get(SystemManager::APP_CLINIC_ID);

        $user = $this->getUser();
        $entityManager = $this->getDoctrine()->getManager();

        if(!empty($id)) {
            $userClinicOld = $this->getDoctrine()->getRepository(Clinic::class)->getUserClinic($user, $id);

            $userClinicOld->setLastAccess(FALSE);
            $entityManager->merge($userClinicOld);
            $entityManager->flush();

            unset($userClinicOld);

        }

        $userClinic = $this->getDoctrine()->getRepository(Clinic::class)->getUserClinic($user, $clinicId);
        $isOwner = false;
        if(!empty($userClinic)) {
            // Set with last access
            $userClinic->setLastAccess(TRUE);
            $entityManager->merge($userClinic);
            $entityManager->flush();

        }

        $systemSettings->checkAppSession();

        return $this->redirect($request->headers->get('referer'));
    }
}
