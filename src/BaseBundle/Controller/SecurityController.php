<?php

namespace BaseBundle\Controller;

use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Manager\SystemManager;
use BaseBundle\Utils\Session;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends Controller
{
    /**
     * @Route("/", name="security_index")
     */
    public function index(Request $request, SystemManager $systemSettings)
    {
        $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();
        if (null === $this->getUser()) {
            return $this->redirectToRoute("security_login");
        }

        return $this->check($request);

    }

    /**
     * @Route("/login", name="security_login")
     */
    public function login(Request $request, SystemManager $systemSettings)
    {

        $authUtils = $this->get('security.authentication_utils');
        if (null !== $this->getUser()) {
            return $this->check($request, $systemSettings);
        }
        $this->initialize($request, $systemSettings);
        // get the login error if there is one
        $error = $authUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authUtils->getLastUsername();
        return $this->render('base/security/login.html.twig', [
                // last username entered by the user (if any)
                'last_username' => $lastUsername,
                // last authentication error (if any)
                'error' => $error,
            ]);
    }

    /**
     * @Route("/login/redirect", name="security_redirect")
     */
    public function check(Request $request, SystemManager $systemSettings)
    {
        try {
            $session  = $request->getSession();

            $systemSettings->checkAppSession();

            $defaultRoute =  $session->get(SystemManager::APP_DEFAULT_ROUTE);

            return $this->redirectToRoute($defaultRoute);
        }catch(\Exception $e) {

            $request->getSession()->getFlashBag()->add('danger', $e->getMessage());

            $session->set(Security::AUTHENTICATION_ERROR, $e->getMessage());
            return $this->redirectToRoute("security_logout_clear");
        }
    }


    public function initialize(Request $request, SystemManager $systemSettings)
    {
        $session  = $request->getSession();

        $domain         = $request->getHost();
        $port           = $request->getPort();
        $defaultRoute   = null;
        $clinicId       = $session->get(SystemManager::APP_CLINIC_ID);
        $clinic         = $systemSettings->getRepository(Clinic::class)->fetchOneById($clinicId);

        $getSettings    = $this->getDoctrine()->getRepository(ClinicalSettings::class)->getSettings($domain, $port);

        if(!empty($clinic)){
            $systemSettings->persistSettingsInSession($clinic->getSettings());
        }


        $systemSettings->persistSettingsInSession($getSettings);
    }


    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     * @Route("/logout", name="security_logout")
     */
    public function logout(Request $request)
    {

        // Logging user out.
        $this->get('security.token_storage')->setToken(null);
        $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();


        // Invalidating the session.
        $session = $request->getSession();
        $session->invalidate();
        $session->clear();

        // Redirecting user to login page in the end.
        $response = $this->redirectToRoute('security_login');

        // Clearing the cookies.
        $cookieNames = [
            $this->container->getParameter('session.name'),
            $this->container->getParameter('session.remember_me.name'),
        ];
        foreach ($cookieNames as $cookieName) {
            $response->headers->clearCookie($cookieName);
        }


        return $response;
    }


    /**
     * This is the route the user can use to logout.
     *
     * But, this will never be executed. Symfony will intercept this first
     * and handle the logout automatically. See logout in app/config/security.yml
     *
     * @Route("/logout_clear", name="security_logout_clear")
     */
    public function logoutClear(Request $request)
    {
        // Logging user out.
        $this->get('security.token_storage')->setToken(null);
        $this->getDoctrine()->getManager()->getConfiguration()->getResultCacheImpl()->deleteAll();

        // Invalidating the session.
        $session = $request->getSession();
        $session->invalidate();
        $session->clear();
        // Redirecting user to login page in the end.
        $response = $this->redirectToRoute('security_login');

        // Clearing the cookies.
        $cookieNames = [
            $this->container->getParameter('session.name'),
            $this->container->getParameter('session.remember_me.name'),
        ];
        foreach ($cookieNames as $cookieName) {
            $response->headers->clearCookie($cookieName);
        }


        return $response;
    }

    /**
     * Request reset user password: show form.
     * @Route("/resetting/request", name="security_reset_password")
     */
    public function requestAction(Request $request, SystemManager $systemSettings)
    {

        return $this->render('base/security/reset.html.twig');
    }
}
