<?php

namespace BaseBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{

    /**
     * @Route("/user/area", name="user")
     * @Template("BaseBundle:Default:index.html.twig")
     */
    public function indexAction()
    {
        $roles = $this->getUser();
        return  ["roles" => $roles->getRoles()];
    }
}
