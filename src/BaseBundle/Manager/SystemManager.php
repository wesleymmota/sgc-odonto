<?php


namespace BaseBundle\Manager;

use BaseBundle\Entity\Agreement;
use BaseBundle\Entity\Clinic;
use BaseBundle\Entity\ClinicalSettings;
use BaseBundle\Entity\Specialization;
use BaseBundle\Entity\User;
use BaseBundle\Entity\UserClinical;
use BaseBundle\Manager\AbstractManager;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class SystemManager extends AbstractManager
{
    public function __construct(ObjectManager $manager, Security $security, SessionInterface $session,ParameterBagInterface $params)
    {
        parent::__construct($manager, Clinic::class);

        $this->security = $security;
        $this->session = $session;
        $this->params = $params;
    }

    private $security;
    private $session;
    private $params;

    const APP_SITE_NAME          = "app.site_name";
    const APP_SITE_LOGO          = "app.site_logo";
    const APP_SITE_LOGO_FILENAME = "app.site_logo_filename";
    const APP_LOCALE             = "app.locale";

    const MENU_ASSIDE            = "menu.asside";

    const APP_CLINIC_ID          = "app.clinic_id";
    const APP_CLINIC             = "app.clinic";
    const APP_CLINICS            = "app.clinics";
    const APP_DEFAULT_ROUTE      = "app.default_route";
    const APP_IS_OWNER           = "app.isOwner";

    const APP_SETTINGS           = "app.settings";

    const APP_MULTIDOMAIN        = "app.multidomain_support";


    /**
     * @return array<string>
     */
    public static function getAppSettings()
    {
        return [

            self::APP_SITE_NAME,
            self::APP_SITE_LOGO,
            self::APP_SITE_LOGO_FILENAME,
            self::APP_LOCALE
        ];
    }

    public static function getAppSession()
    {
        return [

            self::APP_SITE_NAME,
            self::APP_SITE_LOGO,
            self::APP_SITE_LOGO_FILENAME,
            self::APP_LOCALE,
        ];
    }

    public function checkAppSession()
    {

        $this->loadDefaultConfiguration();

        if ($this->security->isGranted(User::ROLE_ADMIN_AREA)) {
            $defaultRoute = "admin_area";

        }elseif ($this->security->isGranted(User::ROLE_DOCTOR_AREA)) {
            $defaultRoute = "doctor_pacientes_list";

            $findClinics = $this->prepareClinics($this->security->getUser());

            if (sizeof($findClinics) === 0) {
                throw new \Exception("Usuário sem clinica!");
            }

            $this->persistClinicsInSession($findClinics);
            $settgins = $this->getSettings();
            $this->persistSettingsInSession($settgins);


        }elseif ($this->security->isGranted(User::ROLE_USER_AREA)) {
            $defaultRoute = "client_area";
            $findClinics = $this->prepareClinics($this->security->getUser());

            if (sizeof($findClinics) === 0) {
                throw new \Exception("Usuário sem clinica!");
            }

            $this->persistClinicsInSession($findClinics);
            $settgins = $this->getSettings();
            $this->persistSettingsInSession($settgins);

        }
        $this->session->set(self::APP_DEFAULT_ROUTE, $defaultRoute);
    }


    public function checkUserSession($user)
    {
        $defaultRoute = "admin_area";
        if ($user->hasRole(User::ROLE_DOCTOR) or $user->hasRole(User::ROLE_EMPLOYEE)){
            $defaultRoute = "doctor_pacientes_list";
        }


        if ($user->hasRole(User::ROLE_CLIENT)){
            $defaultRoute = "client_area";
        }

        $findClinics = $this->prepareClinics($user);
        $this->persistClinicsInSession($findClinics);
        $settgins = $this->getSettings();
        $this->persistSettingsInSession($settgins);
        $this->session->set(self::APP_DEFAULT_ROUTE, $defaultRoute);
    }

    public function getSettings(){

        $clinic         = $this->session->get(self::APP_CLINIC_ID);
        $getSettings    = $this->getRepository(ClinicalSettings::class)->getSettginsByClinic($clinic);
        $this->session->set(self::APP_SETTINGS, $getSettings);

        return $getSettings;
    }


    public function loadDefaultConfiguration()
    {

        foreach (self::getAppSettings() as $setting) {
            if ($this->params->has($setting))
                $this->session->set($setting, $this->params->get($setting));

        }

        if ($this->params->has(self::MENU_ASSIDE)) {
            $menuItems = $this->params->get('menu.asside');
            $this->session->set('app.menu_asside', $menuItems['items']);
        }

    }

    public function prepareClinics(User $user)
    {
        # Pega todas as clinicas que o usuario tem e armazena em sessao(somente as habilitadas
        $findClinics = $this->getRepository()->getUserClinics($user);
        $clinics = [];
        $this->getLastAccess($findClinics);

        foreach ($findClinics as $userClinic) {
            $clinic = $userClinic->getClinic();

            if ($clinic->isEnabled()) {
                $clinics[$clinic->getId()] = [
                    'name' => $clinic->getFantasia(),
                    'status' => $clinic->isEnabled(),
                    'id' => $clinic->getId(),
                    'lastAccess' => $userClinic->getLastAccess(),
                    'isOwner' => (bool)$userClinic->IsOwner(),
                ];

            }
            unset($userClinic);
            unset($clinic);
        }

        return $clinics;

    }

    public function getLastAccess($findClinics)
    {
        $lastSession = null;

        foreach ($findClinics as $userClinic) {

            if ($userClinic->getLastAccess() === TRUE)
                $lastSession = $userClinic;
        }

        // Se nunca ouve acesso pela a primeira clinica e seta um como padrao, e salva
        if (empty($lastSession)) {

            $userClinic = current($findClinics);
            if($userClinic instanceof UserClinical) {
                $userClinic->setLastAccess(TRUE);
                $this->merge($userClinic);
            }

        }
    }


    public function persistClinicsInSession($findClinics)
    {
        $this->session->set(self::APP_CLINICS, $findClinics);

        foreach ($findClinics as $clinic) {

            if ($clinic['lastAccess'] === TRUE) {
                $this->session->set(self::APP_CLINIC, $clinic);
                $this->session->set(self::APP_CLINIC_ID, $clinic['id']);
            }

            if ($clinic['isOwner'] === TRUE)
                $this->session->set(self::APP_IS_OWNER, TRUE);


        }

    }

    public function switchClinicInSession()
    {

        $this->checkAppSession();


    }

    public function persistSettingsInSession(ClinicalSettings $settings = null)
    {
        if($settings !== null and $this->params->get(self::APP_MULTIDOMAIN) == "true") {

            $sessionSiteName = $settings->getName() ?? $this->params->get(self::APP_SITE_NAME);
            $sessionSiteSClinic = $settings->getClinic() ?? $this->params->get(self::APP_CLINIC);
            $sessionSiteSClinicID = $settings->getClinic()->getId() ?? $this->params->get(self::APP_CLINIC_ID);

            if (!is_null($settings->getLogo())) {
                $sessionSiteLogo = $settings->getLogo()->getUrl() ?? $this->params->get(self::APP_SITE_LOGO);
                $sessionSiteLogoFilename = $settings->getLogo()->getFilename() ?? $this->params->get(self::APP_SITE_LOGO_FILENAME);
            } else {
                $sessionSiteLogo = $this->params->get(self::APP_SITE_LOGO);
                $sessionSiteLogoFilename = $this->params->get(self::APP_SITE_LOGO_FILENAME);
            }
            $sessionSiteLocale = $settings->getLocale() ?? $this->params->get(self::APP_LOCALE);

            $this->session->set(self::APP_SITE_NAME, $sessionSiteName);
            $this->session->set(self::APP_CLINIC, $sessionSiteSClinic);
            $this->session->set(self::APP_CLINIC_ID, $sessionSiteSClinicID);
            $this->session->set(self::APP_SITE_LOGO, $sessionSiteLogo);
            $this->session->set(self::APP_SITE_LOGO_FILENAME, $sessionSiteLogoFilename);
            $this->session->set(self::APP_LOCALE, $sessionSiteLocale);

        }else{
            $this->loadDefaultConfiguration();
        }
    }



}
