<?php declare(strict_types = 1);

namespace DoctrineMigrations;

//use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171219020952 extends AbstractMigration
{
    public function up(Schema $schema):void
    {
        #$this->addSql("SET FOREIGN_KEY_CHECKS=0;");
        //$this->connection->setFetchMode(\PDO::FETCH_ASSOC);
        //$this->connection->beginTransaction();
//        foreach (explode(';', file_get_contents(__DIR__ . '/SQL/uf.sql')) as $sql) {
//            //$this->connection->exec($sql);
//            $this->addSql($sql);
//
//        }
        //$this->connection->commit();

        //$this->connection->beginTransaction();
//        foreach (explode(';', file_get_contents(__DIR__ . '/SQL/city.sql')) as $sql) {
//           // $this->connection->exec($sql);
//            $this->addSql($sql);
//
//        }
        //$this->connection->commit();
        $this->write("Datasource: " . $this->connection->getDatabasePlatform()->getName());

        $files = ["uf.sql", "city.sql"];
        if ($this->connection->getDatabasePlatform()->getName() == "mysql") {
            $this->addSql("SET FOREIGN_KEY_CHECKS=0;");
            $this->addSql("TRUNCATE uf;");
            $this->addSql("TRUNCATE city;");
            $this->addSql("SET FOREIGN_KEY_CHECKS=1;");
        } else {
            $this->connection->exec("DELETE from uf;");
            $this->connection->exec("DELETE from city;");
        }
        foreach ($files as $file) {
            $fileDir = __DIR__ . "/SQL/$file";

            if (file_exists($fileDir)) {
                if ($this->connection->getDatabasePlatform()->getName() == "mysql") {
                    $this->addSql(file_get_contents($fileDir));
                } else {
                    //$this->connection->beginTransaction();
                    foreach (explode(';', file_get_contents($fileDir)) as $sql) {
                        $this->connection->exec($sql);
                        $this->write($sql);
                    }
                    //$this->connection->close();
                }
            } else {
                echo "File $fileDir not found";
            }
        }
    }

    public function down(Schema $schema):void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
