#  SGC Odonto - Comandos úteis

## Clear cache

```bash

php bin/console cache:clear --env=prod
php bin/console cache:warmup --env=prod
php bin/console doctrine:cache:clear-metadata         #Clears all metadata cache for an entity manager
php bin/console doctrine:cache:clear-query            #Clears all query cache for an entity manager
php bin/console doctrine:cache:clear-result
php bin/console redis:flushall -n

```

## Restartar os serviços web

```bash

sudo systemctl restart php7.2-fpm
sudo systemctl restart nginx
sudo nginx -s reload

```