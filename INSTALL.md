#  SGC Odonto - Instalação

## Ubuntu Server 18.04 LTS 

```bash

sudo add-apt-repository ppa:ondrej/php
sudo apt-get update

```

## PHP

```bash

sudo apt-get install php7.2-cli php7.2-fpm php7.2-mysql php7.2-curl php7.2-dev php7.2-sqlite3 php7.2-mbstring php7.2-gd php7.2-json php7.2-xmlrpc php7.2-xml php7.2-zip php7.2-apcu php7.2-bcmath php7.2-intl composer -y

```

## Webserver Nginx

```bash

sudo apt-get install nginx -y

```

## Databases

```bash

sudo apt-get install mariadb-server redis-server -y

```

## Outros recursos

```bash

sudo apt-get install git-core wkhtmltopdf xvfb -y

```

## Configuração do WebServer Nginx

Criar o arquivo "/etc/nginx/sites-enabled/sgc-odonto.conf".

Atenção para as confugurações do banco de dados e mailler.

```bash

server {
   listen 80;

   server_tokens off;
   # Redirect HTTP to HTTPS
   if ($scheme = https) {
      return 301 http://$server_name$request_uri;
   }

   server_name odonto.local ; # Editar o nome do servico correramente
   root /var/www/html/sgc-odonto/public;

   index index.php; 
   location / {
      try_files $uri /index.php?$query_string; 
   }

   location @rewrite {
      rewrite ^/(.*)$ /index.php?q=$1 last;
   }

   # PROD
   location ~ \.php$ {
      try_files $uri =404;
      fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
      fastcgi_split_path_info ^(.+\.php)(/.*)$;
      include fastcgi_params;
      aio threads;
      fastcgi_index index.php;
      fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
      fastcgi_param DOCUMENT_ROOT $realpath_root;
      fastcgi_param SCRIPT_NAME $fastcgi_script_name;
      fastcgi_param QUERY_STRING $query_string;
      fastcgi_param REQUEST_METHOD $request_method;
      fastcgi_param CONTENT_TYPE $content_type;
      fastcgi_param CONTENT_LENGTH $content_length;
      fastcgi_intercept_errors on;
      fastcgi_ignore_client_abort off;
      fastcgi_connect_timeout 60;
      fastcgi_send_timeout 180;
      fastcgi_read_timeout 180;
      fastcgi_buffer_size 128k;
      fastcgi_buffers 4 256k;
      fastcgi_busy_buffers_size 256k;
      fastcgi_temp_file_write_size 256k; 

      # configurações da aplicacao
      fastcgi_param APP_NAME Odontologia;
      #fastcgi_param APP_ENV prod; # Producao
      fastcgi_param APP_ENV dev; # Desenvolvimento
      fastcgi_param APP_DEBUG 0;
      fastcgi_param APP_SECRET 7asd12d384dc6c213071e49bb7d106df5;
      fastcgi_param DATABASE_URL "mysql://sgc:sgc@localhost:3306/sgc";
      fastcgi_param MAILER_URL smtp://127.0.0.1:1025;
      fastcgi_param WKHTMLTOPDF_PATH /usr/bin/wkhtmltopdf;
      fastcgi_param WKHTMLTOIMAGE_PATH /usr/bin/wkhtmltoimage;
   } 

   location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
      expires max;
      log_not_found off;
   }

   error_log /var/log/nginx/sgc-odonto-error.log;
   access_log /var/log/nginx/sgc-odonto-access.log combined;

}

```

Desativar a configuração padrão

```bash

sudo rm /etc/nginx/sites-enabled/default

```

## Ajuste timezone do PHP

```bash

sudo sed -r -i "s,;?date.timezone =.*,date.timezone = America/Sao_Paulo," /etc/php/7.2/cli/php.ini
sudo sed -r -i "s,;?date.timezone =.*,date.timezone = America/Sao_Paulo," /etc/php/7.2/fpm/php.ini

```

## Baixar o prpjeto

Clonar o projeto do Bitbucket

```bash

cd /var/www/html/
sudo git clone https://wesleymmota@bitbucket.org/wesleymmota/sgc-odonto.git
cd sgc-odonto
composer update
composer install
cp .env.dist .env
sudo chown www-data:www-data /var/www/html/ -R

```

Editar o arquivo ".env" e colocar as informações reais de banco de dados.

```bash

DATABASE_URL="mysql://sgc:sgc@localhost:3306/sgc"

```

Adicionar as informações de email(sparkpostmail) no arquivo ".env" e no MAILER_URL no nginx

```bash

MAILER_URL=smtp://smtp.sparkpostmail.com:587?encryption=tls&auth_mode=login&username=SMTP_Injection&password=1231231212312313131231

```

## Setup do database e seu usuário

Logando no MySQL com o root:

```bash

sudo mysql -u root

```

Criação do database, usuário e permissões:

```bash

create database sgc;
create user 'sgc' identified by 'sgc';
grant all privileges on `sgc`.* to 'sgc'@localhost;
flush privileges;

```

Opcional, mas por segurança, executar o script de instalação segura do MySQL:

```bash

sudo mysql_secure_installation
   
   Enter current password for root (enter for none): [ENTER]
   Set root password? [Y/n] [ENTER]
   New password: root 
   Re-enter new password: root
   Remove anonymous users? [Y/n] [ENTER]
   Disallow root login remotely? [Y/n] [ENTER]
   Remove test database and access to it? [Y/n] [ENTER]
   Reload privilege tables now? [Y/n] [ENTER]

```

## Preparação do database para a aplicação

```bash

php bin/console doctrine:database:drop --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force 
php bin/console doctrine:migrations:migrate
php bin/console cache:pool:prune

php bin/console doctrine:cache:clear-metadata         #Clears all metadata cache for an entity manager
php bin/console doctrine:cache:clear-query            #Clears all query cache for an entity manager
php bin/console doctrine:cache:clear-result
php bin/console redis:flushall -n

php bin/console hautelook:fixtures:load --env=prod -n
 
# ou para desenvolvimento
#php bin/console hautelook:fixtures:load --env=dev -n

```

## Restartar o serviço web

```bash

sudo systemctl restart php7.2-fpm
sudo systemctl restart nginx
sudo nginx -s reload

```